WhatstrendingWebApp::Application.routes.draw do

  SSL_URL_OPTIONS ||= {protocol: 'https://'}
  HTTP_URL_OPTIONS ||= {protocol: 'http://'}


  # --- Require HTTPS ---
  constraints(SSL_URL_OPTIONS) do

    # ADMIN AREA
    namespace :admin do
      # A/B Testing dashboard.
      mount Split::Dashboard, at: 'ab-tests'

      get 'authors_search', to: 'users#search'

      resources :posts do
        collection do
          get 'by/:filter', to: 'posts#index', as: 'filter'
        end
        member do
          get 'publish'
          get 'unpublish'
          get 'fetch_authors'
          get 'stats/:filter', to: 'posts#stats', as: 'stats'
        end
      end
      resources :post_items, only: [:destroy] do
        get 'oembed', as: :oembed, on: :collection
      end
      resources :post_queues, only: [:create, :update]

      resources :trend_lists, path: '/trends', as: 'trends', only: [:index] do
        member do
          post 'publish'
        end
        resources :trend_list_items, path: '/list', as: 'lists', only: [] do
          resources :trend_items, path: '/item', as: 'items', only: [:edit, :update] do
            collection do
              post 'publish'
              post 'unpublish'
            end
          end
        end
      end
      resources :categories
      resources :users do
        get 'by/:filter', on: :collection, to: 'users#index', as: 'filter'
      end
      resources :badges
      resources :dashboard, only: [:index]
      resources :caches, only: [:index,:update]
      resources :layouts, only: [:index, :create]
      resources :settings, only: [:index, :create, :destroy]
      namespace :queues do
        resources :scheduled, only: [:index,:create,:destroy]
      end

      resources :search_terms, only: [:index, :show]

      root to: 'dashboard#index'
    end

    # USER ACCOUNT, SIGNUP, LOGIN, PASSWORD RESET
    resource :user, only: [:edit, :update], path: '/account', path_names: {edit: '/'}
    resource :user_session, only: [:create]
    resources :password_resets, except: [:index, :show, :destroy]
    get '/login' => 'user_sessions#new', as: :login, path: '/access/login'
    get '/logout' => 'user_sessions#destroy', as: :logout, path: '/access/logout'
  end

  # --- Require HTTP ---
  constraints(HTTP_URL_OPTIONS) do

    # ADMIN AREA
    namespace :admin do
      resources :posts, only: [] do
        member do
          match "/preview", as: :preview, action: :preview, via: [:get,:put,:post]
        end
      end
    end

    resources :email_subscribers, path: '/newsletter', only: [:new, :create], path_names: {new: 'subscribe', create: 'subscribe'}

    resources :categories, only: [:index, :show]
    resources :authors, only: [:show], path: '/contributors'
    resources :posts, only: [], path: '/post' do
      resources :post_item, only: [], as: :item, path: '/' do
        member do
          put 'share'
        end
      end
      member do 
        put 'comment'
        put 'share'
      end
    end

    resources :sharing_mails, path: '/share/email', only: [:new, :create], path_names: {new: 'send', create: 'send'} do
      get :preview, on: :collection
    end

    resources :support_messages, path: '/contact', only: [:index,:new,:create], path_names: {new: '', create: 'submit'}

    # Search path
    get '/search' => 'categories#index', as: :search, defaults: {search: true}
    get '/random' => 'posts#random', as: :random_post

    put '/_ab/:id(/:goal)' => 'ab_tests#finish', as: :finish_ab_test


    # --- OAUTH ---
    # match 'auth/:provider', to: lambda{|env| [404, {}, ["Not Found"]]}, as: 'auth', via: [:get,:post]
    # get 'auth/:provider/callback', to: 'contest/escape/authentications#create'


    if Rails.env.development?
      require 'sidekiq/web'
      mount Sidekiq::Web, at: '/sidekiq'
    end
  end

  # Protocol constrain agnostic
  root to: 'categories#index'

  # Old URL from previous WordPress Site
  get '/:year/:month/:slug' => 'posts#old', as: :old_post, constraints: {year: /\d+{4}/, month: /\d+{2}/}

  # Static pages routing, use StaticPage to check if exists as constraint
  match '/*page' => 'static_pages#show', as: :static_page, constraints: StaticPage.new, via: [:get]


  # --- OLDER CUSTOM CAMPAIGN ROUTES ---
  # Escape2Bonnaroo Contest redirect route (May 2014) - we collapsed this into a single static page after the contest ended
  get '/escapetobonnaroo/*etc', to: redirect('/escapetobonnaroo')

  # Nice SEO route for Google News
  get '/:prefix/:post_id-:slug/:id' => 'post_items#show', as: :seo_post_item, constraints: {post_id: /\d+/}
  get '/:prefix/:id-:slug' => 'posts#show', as: :seo_post, constraints: {id: /\d+/}


end


# Now this is much better!
WhatstrendingWebApp::Application.routes.named_routes.module.module_eval do
  def post_path(post, opts={})
    post_url(post, opts.merge(only_path: true) )
  end

  def post_url(post, opts={})
    seo_post_url(opts.merge(prefix: (post.category.slug rescue 'unknown'), id: ('%03d' % post.id), slug: post.slug, protocol: 'http'))
  end

  def post_item_path(post, item, opts={})
    post_item_url(post, item, opts.merge(only_path: true))
  end

  def post_item_url(post, item, opts={})
    seo_post_item_url(opts.merge(prefix: (post.category.slug rescue 'unknown'), post_id: ('%03d' % post.id), slug: post.slug, id: item.slug, protocol: 'http'))
  end

  # Social Media URLs
  def facebook_url;   'https://facebook.com/whatstrending'; end
  def twitter_url;    'https://twitter.com/whatstrending'; end
  def youtube_url;    'https://www.youtube.com/user/Whatstrending'; end
  def googleplus_url; 'https://plus.google.com/+WhatsTrending'; end
  def podcast_url;    'http://www.iheart.com/talk/show/Whats-Trending/'; end
  def instagram_url;  'http://instagram.com/whatstrending'; end
  def vine_url;       '#'; end
  def xolator_url;    'http://xolator.com?ref=wt_footer'; end
end