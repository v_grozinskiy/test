every '12 */1 * * *', roles: [:db] do
  # Generate site map
  rake 'sitemap:refresh', output: 'log/sitemap.log'
end

every '*/15 * * * *', roles: [:db] do
  # Generate news site map
  rake 'sitemap:refresh CONFIG_FILE=config/sitemap_news.rb', output: 'log/sitemap.log'
end

every :day, at: '03:00am', roles: [:db] do
  # Update WT followers #s from various social sites
  rake 'followers_count:refresh', output: 'log/followers_count.log'
end

every :day, at: '03:33am', roles: [:db] do
  # Rollup and delete old impression data from database. (backup, rollup, delete)
  rake 'stats:impressions_rollups', output: 'log/impression_rollups.log'
end