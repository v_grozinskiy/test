role :app, %w{whatstrending.com}
role :web, %w{whatstrending.com}
role :db,  %w{whatstrending.com}

set :whenever_roles, -> { [:db] }
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

server 'whatstrending.com', user: 'ubuntu', roles: %w{web app db}

set :deploy_to, '/vol/whatstrending.com'

set :rails_env, :production
set :sidekiq_processes, 2


set :ssh_options, { auth_methods: ["publickey"], keys: [File.join(File.expand_path('../../../.ssh', File.dirname(__FILE__)), "wt-app-web.pem")] }