role :app, %w{app.testing.whatstrending.com}
role :web, %w{app.testing.whatstrending.com}
role :stage_db,  %w{app.testing.whatstrending.com}

set :whenever_roles, -> { [:stage_db] }
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

server 'app.testing.whatstrending.com', user: 'ubuntu', roles: %w{web app stage_db}

set :deploy_to, '/vol/app.testing.whatstrending.com'

set :rails_env, :staging
set :sidekiq_processes, 1
set :assets_prefix, 'testing/assets'

set :ssh_options, { auth_methods: ["publickey"], keys: [File.join(File.expand_path('../../../.ssh', File.dirname(__FILE__)), "wt-app-web.pem")] }