# Default Options
SitemapGenerator::Sitemap.default_host = ['production'].include?(Rails.env) ? "http://whatstrending.com" : "http://wt.dev"
SitemapGenerator::Sitemap.sitemaps_path = "sitemaps/"
now = Time.now


# --- Regular Site Map --------------------------------------------------------
SitemapGenerator::Sitemap.filename = 'sitemap'
SitemapGenerator::Sitemap.create do
  # Top Trend List (TODO)
  # add trends_path, lastmod: TrendList.published.latest.updated_at, priority: 1.0

  # Category Pages (TODO)
  Category.active.each do |category|
    add category_path(category), priority: 0.7, changefreq: 'hourly'
  end

  # All published posts. Sets higher priority for posts recently edited (1.0 to 0.5)
  Post.includes(:category).published.find_each do |post|
    days = [5,((now-post.updated_at).round / 1.day)].min
    priority = 0.5 + (0.5 * Math.exp(-0.12 * days * days)).round(1)
    add seo_post_path(post.category.slug, post.id, post.slug), lastmod: post.updated_at, priority: priority, images: [{loc: post.promo_image.url(:large), title: post.title}]
  end

  # Authors (TODO)
  User.authors.find_each do |user|
    if user.posts.published.count > 0
      add author_path(user), lastmod: user.posts.published.latest.updated_at, priority: 0.5 rescue nil
    end
  end

  # Static Pages (TODO)
  # add articles_path, priority: 0.5, changefreq: 'weekly'
end


# --- Video Site Map ----------------------------------------------------------
# SitemapGenerator::Sitemap.filename = 'video-sitemap'
# SitemapGenerator::Sitemap.create do
#   # All published posts. Sets higher priority for posts recently edited (1.0 to 0.5)
#   Post.published.find_each do |post|
#     videos, images = [], []
#     days = [5,((now-post.updated_at).round / 1.day)].min
#     priority = 0.5 + (0.5 * Math.exp(-0.12 * days * days)).round(1)
# 
#     images << {loc: post.promo_image.url(:large), title: post.title} # Promo Image
#     unless post.post_items.blank?
#       post.post_items.images.each{|p| images << p.to_sitemap }
#       post.post_items.videos.each{|p| videos << p.to_sitemap }
#     end
# 
#     add post_path(post), lastmod: post.updated_at, priority: priority, images: images, videos: videos
#   end
# end
