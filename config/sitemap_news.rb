# Default Options
SitemapGenerator::Sitemap.default_host = ['production'].include?(Rails.env) ? "http://whatstrending.com" : "http://wt.dev"
SitemapGenerator::Sitemap.sitemaps_path = "sitemaps/"
now = Time.now

# --- News Site Map -----------------------------------------------------------
SitemapGenerator::Sitemap.filename = 'news-sitemap'
SitemapGenerator::Sitemap.create do
  Post.includes(:category).includes(:tags).published.google_news.sfw.where('published_at > ?', now-2.days).find_each do |post|
    add seo_post_path(post.category.slug, post.id, post.slug), lastmod: nil, changefreq: nil, priority: nil, news: {
      title: post.title, keywords: post.news_keywords.join(', '), genres: 'Blog', publication_date: post.published_at, publication_name: "What's Trending", publication_language: 'en'
    }
  end
end