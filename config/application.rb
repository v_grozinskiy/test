require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)


module WhatstrendingWebApp
  class Application < Rails::Application
    redis_options = YAML.load_file(File.join(Rails.root, 'config', 'redis.yml'))[::Rails.env] rescue nil
    $redis = Redis.new(url: redis_options['url']) if redis_options.present?

    if defined?(PhusionPassenger)
      PhusionPassenger.on_event(:starting_worker_process) do |forked|
        if forked
          $redis.client.reconnect if $redis
          Rails.cache.reconnect
          Rails.logger.info "Reconnecting to cache store..."
        end
      end
    end


    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Pacific Time (US & Canada)'
    config.active_record.default_timezone = :local

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :en

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += %W(#{config.root}/lib)
    config.autoload_paths += %W(#{config.root}/lib/**/)

    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')
    config.assets.paths << Rails.root.join('vendor', 'assets', 'fonts')

    # Allow Airbrake capture errors in rake tasks
    # config.rescue_rake_exceptions = true

    config.action_mailer.preview_path = "#{Rails.root}/lib/mailer_previews"

    require "#{config.root}/lib/media_adapters/media_adapters.rb"
  end
end

I18n.enforce_available_locales = false
