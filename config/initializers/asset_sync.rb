Rails.application.assets.register_mime_type 'image/png', '.png'
 
Rails.application.assets.register_postprocessor 'image/png', :png_compressor do |context, data|
  IO.popen("pngquant -", "rb+") do |process|
    process.write(data)
    process.close_write
    process.read
  end
end


if defined?(AssetSync)
  AssetSync.configure do |config|
    config.custom_headers = {
      '.*\.(woff|ttf|svg|otf|eot)' => {
        'Access-Control-Allow-Origin' => '*'
      }
    }
  end

  # S3 Config
  s3_options = YAML.load_file(File.join(Rails.root, 'config', 's3.yml'))[Rails.env].symbolize_keys rescue nil

  if s3_options.present?
    AssetSync.configure do |config|
      config.fog_provider = 'AWS'
      config.aws_access_key_id = s3_options[:access_key_id]
      config.aws_secret_access_key = s3_options[:secret_access_key]
      config.fog_directory = s3_options[:bucket]
      # config.prefix = s3_options[:asset_prefix] unless s3_options[:asset_prefix].blank?

      Fog.credentials = { path_style: true }

      # To use AWS reduced redundancy storage.
      # config.aws_reduced_redundancy = true

      # Invalidate a file on a cdn after uploading files
      # config.cdn_distribution_id = "12345"
      # config.invalidate = ['file1.js']

      # Increase upload performance by configuring your region
      # config.fog_region = 'eu-west-1'

      # Don't delete files from the store
      # config.existing_remote_files = "keep"

      # Automatically replace files with their equivalent gzip compressed version
      config.gzip_compression = true

      # Use the Rails generated 'manifest.yml' file to produce the list of files to
      # upload instead of searching the assets directory.
      # config.manifest = true

      # Fail silently.  Useful for environments such as Heroku
      # config.fail_silently = true
    end
  end
end