begin

  # Include defaults
  Setting.save_default(:wt_youtube_live_url, '')
  Setting.save_default(:recommended_post_ids, [])
  Setting.save_default(:min_post_count_to_view, 100)
  Setting.save_default(:follow_count_facebook, 59000)
  Setting.save_default(:follow_count_twitter, 48000)
  Setting.save_default(:follow_count_youtube, 128500)
  Setting.save_default(:follow_count_google_plus, 483800)

  # Explicity define
  Setting.admin_facebook_ids = [
    7002294,    # greg leuch
    34605207,   # jiashan wu
    500115741,  # shira lazar
    1036937674, # damon berger
    # 13005458,   # gaby dunn
    # 1165443834, # chris menning
    528188335,  # jonathan harris
    523801777,  # Mai Linh Nguyen
    
  ]

  Setting.facebook_app_id = (Rails.env.production? ? '1543512445874879' : '1397878103808635')


rescue => err
  puts "Settings Initializer Error: #{err}"
  nil
end