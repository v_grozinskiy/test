Split.configure do |config|
  config.allow_multiple_experiments = true
  config.enabled = true
  config.include_rails_helper = true
end


# Define where dashboard is (password protected)
Split::Dashboard.use Rack::Auth::Basic do |username, password|
  username == 'xolator' && password == 'xoxo'
end


# Defined experiments
Split.configure do |config|
  config.bots['flipboard'] = "Flipboard"

  config.experiments = {

    'ab_similar_posts' => {
      alternatives: [
        {name: 'whatstrending', percent: 33},
        {name: 'zergnet',       percent: 33},
        {name: 'outbrain',      percent: 33}
      ]
    },

    # Social gates used
    'social_gates' => {
      alternatives: [
        {name: 'none',        percent: 90},
        {name: 'twitter',     percent: 6.67},
        {name: 'facebook',    percent: 6.67},
        {name: 'newsletter',  percent: 6.67}
      ],
      goals:  ['follow', 'cancel'],
      resettable:   false
    }

  }
end