require "spec_helper"

describe "The post" do

  before do
    create_categories

    @user = FactoryGirl.create :user
    @post = FactoryGirl.create :post, category: @categories.sample, creator: @user
    @post.authors << @user
    ActionController::Base.perform_caching = true
    ActionController::Base.cache_store = :file_store, "tmp/cache"
    FileUtils.rm_rf(Dir['tmp/cache'])
  end

  describe "cache fragment" do

    before do
      get seo_post_path(prefix: @post.category.name, id: @post.id, slug: @post.slug)
    end

    it "should cache post" do
      response.should be_fragment_cached("#{@post.cache_key}/show")
    end

    it "should cache post items" do
      response.should be_fragment_cached("#{@post.cache_key}/post_items")
    end

  end
  

  describe "expire fragment" do

    before do
      @post_items = FactoryGirl.create_list(:post_item, 2, post: @post)
      @user.roles = [:admin, :super]
      @user.save
      create_user_session @user
    end

    before :each do
      get seo_post_path(prefix: @post.category.name, id: @post.id, slug: @post.slug)
    end

    describe "reorder" do
      it "should work" do
        put admin_post_path(post: {
          post_items_attributes: {
            "0" => { position: 1, 
                     id: @post_items[0].id},
            "1" => { position: 2, 
                     id: @post_items[1].id}
          }},
          commit: "Arrange",
          id: @post.id)
        response.should_not be_fragment_cached("#{@post.cache_key}/post_items")
      end
    end

  end

  after do
    ActionController::Base.perform_caching = false
    FileUtils.rm_rf(Dir['tmp/cache'])
  end  

end