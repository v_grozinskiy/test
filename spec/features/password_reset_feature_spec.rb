require "spec_helper"

describe "Reset Password" do

  before :each do
    @user = FactoryGirl.create(:user, password: "password", password_confirmation: "password")
  end

  it "should not work for users not existing" do 
    visit login_path
    click_link "Forgot password?"
    current_path.should == new_password_reset_path
    fill_in :email, with: "invalid_email"
    click_button "Reset Password"
    page.should have_content "No user was found with that email address."
  end

  it "should work for users having valid email" do
    visit login_path
    click_link "Forgot password?"
    current_path.should == new_password_reset_path
    fill_in :email, with: @user.email
    click_button "Reset Password"
    current_path.should == root_path
    page.should have_content "Instructions to reset your password have been emailed to you. Please check your email."

    @user.reload
    ActionMailer::Base.deliveries.last.body.to_s.should include 
      edit_password_reset_url(@user.perishable_token)

    visit edit_password_reset_url(@user.perishable_token)
    fill_in :user_password, with: "new_password"
    fill_in :user_password_confirmation, with: "new_password"
    click_button "Update password"
    current_path.should == root_path
    page.should have_content "Password successfully updated."
  end

  it "should not work for invalid perishable token" do
    visit edit_password_reset_url("invalid_perishable_token")
    page.should have_content "We're sorry, but we could not locate your account."
  end

  it "should not reset for invalid password" do
    visit edit_password_reset_url(@user.perishable_token)
    fill_in :user_password, with: "new_password"
    fill_in :user_password_confirmation, with: "invalid_password"
    click_button "Update password"
    page.should have_content "doesn't match"
  end

end