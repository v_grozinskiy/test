require "spec_helper"
require 'rake'
WhatstrendingWebApp::Application.load_tasks


describe "trends:fetch" do

  before :all do
    create_categories

    @user = FactoryGirl.create :user, roles: [:admin]
    FactoryGirl.create_list :trend_list, 5, list_type: 'all', state: 'draft'
    TrendWorker.new.fetch
  end

  it "should delete unpublished and create new one" do
    TrendList.draft.where(list_type: 'all').count.should == 1
    TrendList.published.count.should == 0
  end

  it "should create trend list items" do
    TrendList.first.list_items.count.should == 25
  end

  it "should create trend items" do
    TrendItem.all.count.should == 25
  end 

  it "should create posts" do
    Post.all.count.should == 25
  end
end