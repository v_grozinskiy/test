require "spec_helper"

describe "Admin users" do

  describe "for unauthorized users" do

    before :each do
      @user = FactoryGirl.create :user
    end

    it "should request user authentication" do
      [admin_users_path, new_admin_user_path, admin_user_path(@user), edit_admin_user_path(@user)].each do |page_name|
        visit page_name
        current_path.should == login_path
        page.should have_content "You should log in to access this page."
      end
    end

    it "should not allow no admin users" do
      login @user
      [admin_users_path, new_admin_user_path, admin_user_path(@user), edit_admin_user_path(@user)].each do |page_name|
        visit page_name
        current_path.should == root_path
        page.should have_content "You are not authorized to access this page."
      end
    end
  end

  describe "for authorized admin users" do

    before :each do
      @users = FactoryGirl.create_list :user, 5
      @users.first.roles = [:admin, :manager]
      @users.first.save
      login @users.first
    end

    it "should allow to browse users" do 
      visit admin_users_path
      current_path.should == admin_users_path
      page.should_not have_content "You are not authorized to access this page."
      page.should have_content @users[4].name
    end

    it "should allow to see user profile" do 
      visit admin_users_path
      click_link @users[1].name
      current_path.should == admin_user_path(@users[1])
      page.should_not have_content "You are not authorized to access this page."
      page.should have_content @users[1].email
      page.should have_content @users[1].login
    end

    it "should allow to create user" do 
      visit new_admin_user_path
      current_path.should == new_admin_user_path
      page.should_not have_content "You are not authorized to access this page."
      fill_in :user_email, with: "test@example.com"
      fill_in :user_login, with: "test_user"
      fill_in :user_first_name, with: "Test"
      fill_in :user_last_name, with: "User"
      choose :user_access_roles_user
      choose :user_admin_roles_editor
      click_button "Create User"
      page.should have_content "User was successfully created!"
      page.should have_content "test@example.com"
      page.should have_content "test_user"
      page.should have_content "Test User"
      User.all.count.should == 6
    end

    it "should not allow to create user with invalid profile" do 
      visit new_admin_user_path
      fill_in :user_email, with: "invalid_email"
      choose :user_access_roles_user
      choose :user_admin_roles_editor
      click_button "Create User"
      page.should have_content "Please review the problems below:"
      page.should have_content "should look like an email address."
      User.all.count.should == 5
    end

    it "should allow to update user" do 
      visit edit_admin_user_path(@users[1])
      current_path.should == edit_admin_user_path(@users[1])
      page.should_not have_content "You are not authorized to access this page."
      fill_in :user_email, with: "test@example.com"
      choose :user_access_roles_user
      choose :user_admin_roles_editor
      click_button "Update User"
      page.should have_content "User was successfully updated!"
      page.should have_content "test@example.com"
    end

    it "should not allow to update user with invalid profile" do 
      visit edit_admin_user_path(@users[1])
      fill_in :user_email, with: "invalid_email"
      choose :user_access_roles_user
      choose :user_admin_roles_editor
      click_button "Update User"
      page.should have_content "Please review the problems below:"
      page.should have_content "should look like an email address."
    end

    it "should allow destroy user" do 
      visit admin_user_path(@users[1])
      click_link "Delete"
      current_path.should == admin_users_path
      page.should have_content "User was successfully deleted!"
      User.all.count.should == 4
    end
  end

  describe "for user search" do
    before :each do
      @user = FactoryGirl.create :user, roles: [:admin, :manager]
      create_user_session @user
    end

    it "responds with JSON" do
      get "/admin/authors_search", q: @user.name[0..1], format: :json
      parsed_body = JSON.parse(response.body)
      parsed_body.first['name'] == @user.name
    end
  end
end