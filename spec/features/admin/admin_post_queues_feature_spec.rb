require "spec_helper"

describe "The post queue" do

  before do
    create_categories

    @user = FactoryGirl.create :user, roles: [:admin, :super]
    @post = FactoryGirl.create :post, category: @categories.sample, creator: @user
    @post.authors << @user
    @post_items = FactoryGirl.create_list :post_item, 2, post: @post, image: File.open(Rails.root.join('spec', 'fixtures', 'files', 'test.png'), 'r')
    @post.unpublish
    login @user
  end

  describe "publish later", sidekiq: :fake do

    it "should queue publishment", js: true do

      visit edit_admin_post_path(@post)
      
      find('#publish-buttons .dropdown-toggle').click
      page.should have_content 'Publish Later'
      click_link 'Publish Later'

      fill_in :post_queue_publish_at, with: 3.day.from_now.strftime('%Y/%m/%d %I:%M %p')
      click_button 'Publish Later'

      page.should have_content 'Post was queued successfully.'
      @post.reload
      @post.queued?.should == true
      PostQueueWorker.jobs.size.should == 1
    end

  end

  describe "publish", sidekiq: :fake do

    before :each do
      @post_queue = @post.build_post_queue(publish_at: 3.days.from_now)
      @post_queue.save
    end

    it "should delete queue" do
      PostQueueWorker.jobs.size.should == 1
      visit edit_admin_post_path(@post)
      click_button "Publish"
      PostQueueWorker.jobs.size.should == 0
    end

  end

  describe "queue", sidekiq: :fake do

    before :each do
      @post_queue = @post.build_post_queue(publish_at: 1.days.from_now)
      @post_queue.save
    end

    it "should publish post" do
      @post.queued?.should be_true
      @post.publish
      @post.queued?.should be_false
      PostQueueWorker.jobs.size.should == 0
    end

  end

end