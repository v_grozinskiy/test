require "spec_helper"

describe "Admin search terms" do

  describe "for unauthorized users" do

    before :each do
      @user = FactoryGirl.create :user
      @search_term = FactoryGirl.create :search_term
    end

    it "should request users authentication" do
      [admin_search_terms_path, admin_search_term_path(@search_term)].each do |page_name|
        visit page_name
        current_path.should == login_path
        page.should have_content "You should log in to access this page."
      end
    end

    it "should not allow no admin users" do
      login @user
      [admin_search_terms_path, admin_search_term_path(@search_term)].each do |page_name|
        visit page_name
        current_path.should == root_path
        page.should have_content "You are not authorized to access this page."
      end
    end
  end

  describe "for authorized admin users" do

    before do
      @user = FactoryGirl.create :user, roles: [:admin, :super]
      login @user
      @search_terms = FactoryGirl.create_list :search_term, 2
    end

    it "should allow to browse search terms" do 
      visit admin_search_terms_path
      current_path.should == admin_search_terms_path
      page.should_not have_content "You are not authorized to access this page."
      page.should have_content @search_terms[0].term
      page.should have_content @search_terms[1].term
    end

    it "should allow to see search term detail" do 
      visit admin_search_terms_path
      click_link @search_terms[0].term
      current_path.should == admin_search_term_path(@search_terms[0])
      page.should_not have_content "You are not authorized to access this page."
      page.should have_content @search_terms[0].impressions.first.referrer
      page.should have_content @search_terms[0].impressions.first.ip_address
    end
  end

end