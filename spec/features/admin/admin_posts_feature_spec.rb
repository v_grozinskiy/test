require "spec_helper"

describe "Admin posts page" do

  describe "for unauthorized users" do

    before :each do
      @user = FactoryGirl.create :user
      @category = FactoryGirl.create :category
      @post = FactoryGirl.create :post, category: @category
    end

    it "should request users authentication" do
      [admin_posts_path, new_admin_post_path, admin_post_path(@post), edit_admin_post_path(@post)].each do |page_title|
        visit page_title
        current_path.should == login_path
        page.should have_content "You should log in to access this page."
      end
    end

    it "should not allow no admin users" do
      login @user
      [admin_posts_path, new_admin_post_path, admin_post_path(@post), edit_admin_post_path(@post)].each do |page_title|
        visit page_title
        current_path.should == root_path
        page.should have_content "You are not authorized to access this page."
      end
    end
  end

  describe "for authorized admin users" do

    before do
      create_categories
      @user = FactoryGirl.create :user, roles: [:admin, :manager]
      @post = FactoryGirl.create :post, category: @categories.sample, creator: @user
      login @user
    end

    describe "index" do
      it "should allow to browse posts" do 
        visit admin_posts_path
        current_path.should == admin_posts_path
        page.should_not have_content "You are not authorized to access this page."
        page.should have_content @post.title
      end
    end

    describe "show" do
      it "should allow to see post detail" do 
        visit admin_post_path(@post)
        current_path.should == admin_post_path(@post)
        page.should_not have_content "You are not authorized to access this page."
        page.should have_content @post.title
        page.should have_content @post.description
      end
    end

    describe "create" do
      it "should allow to create post" do 
        visit new_admin_post_path
        current_path.should == new_admin_post_path
        page.should_not have_content "You are not authorized to access this page."
        select @categories.first.name, from: :post_category_id
        fill_in :post_title, with: "Test Post"
        fill_in :post_description, with: "this is a test"
        click_button "Save"
        page.should have_content "Post was successfully created!"
      end

      it "should not allow to create post with invalid information" do 
        visit new_admin_post_path
        fill_in :post_title, with: ""
        fill_in :post_description, with: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
        page.should have_content "Sed ut perspiciatis unde omnis iste natus"
        click_button "Save"
        page.should_not have_content "Post was successfully created!"
        page.should have_content "Title is too short (minimum is 2 characters)"
        page.should have_content "Description is too long (maximum is 500 characters)"
      end
    end

    describe "update" do
      it "should allow to update post" do 
        visit edit_admin_post_path(@post)
        current_path.should == edit_admin_post_path(@post)
        page.should_not have_content "You are not authorized to access this page."
        fill_in :post_title, with: "Test Post"
        click_button "Save"
        page.should have_content "Post was successfully saved!"
      end

      it "should not allow to update post with invalid information" do 
        visit edit_admin_post_path(@post)
        fill_in :post_title, with: ""
        click_button "Save"
        page.should have_content "Title can't be blank"
      end
    end

    describe "publish" do
      before do
        @post.unpublish
      end

      it "should allow to publish post" do 
        visit edit_admin_post_path(@post)
        click_button "Publish"
        page.should have_content "Post was successfully published!"
      end
    end

    describe "unpublish" do
      it "should allow to unpublish post" do 
        visit edit_admin_post_path(@post)
        click_link "Unpublish"
        page.should have_content "Post was successfully unpublished!"
      end
    end

    describe "recommended" do
      before do
        @new_posts = FactoryGirl.create_list(:post, 2, category: @categories.sample, creator: @user)
      end

      it "should allow to add recommended post", js: true do
        visit edit_admin_post_path(@post)
        click_link "Recommended"
        fill_field = "$('#post_recommended_post_ids_').val('#{@new_posts[0].id}');"
        page.driver.execute_script(fill_field)
        click_button "Save"
        @post.recommended_post_ids.should == [@new_posts[0].id]
      end

      it "should allow to delete recommended post", js: true do
        visit edit_admin_post_path(@post)
        click_link "Recommended"
        remove = "$('.select2-search-choice-close').click();"
        page.driver.execute_script(remove)
        click_button "Save"
        @post.recommended_post_ids.should == []
      end
    end

  end

end