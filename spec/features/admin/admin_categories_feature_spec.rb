require "spec_helper"

describe "Admin categories page" do

  describe "for unauthorized users" do

    before :each do
      @user = FactoryGirl.create :user
      @category = FactoryGirl.create :category
    end

    it "should request users authentication" do
      [admin_categories_path, new_admin_category_path, admin_category_path(@category), edit_admin_category_path(@category)].each do |page_name|
        visit page_name
        current_path.should == login_path
        page.should have_content "You should log in to access this page."
      end
    end

    it "should not allow no admin users" do
      login @user
      [admin_categories_path, new_admin_category_path, admin_category_path(@category), edit_admin_category_path(@category)].each do |page_name|
        visit page_name
        current_path.should == root_path
        page.should have_content "You are not authorized to access this page."
      end
    end
  end

  describe "for authorized admin users" do

    before :each do
      Category.destroy_all
      create_categories
      @categories_ct = Category.count
      @user = FactoryGirl.create :user, roles: [:admin, :super]
      login @user
    end

    it "should allow to browse categories" do 
      visit admin_categories_path
      current_path.should == admin_categories_path
      page.should_not have_content "You are not authorized to access this page."
      page.should have_content @categories[4].name
    end

    # it "should allow to see category detail" do 
    #   visit admin_categories_path
    #   first(:link, 'View').click
    #   current_path.should == category_path(@categories[1])
    #   page.should_not have_content "You are not authorized to access this page."
    #   page.should have_content @categories[1].name
    #   page.should have_content @categories[1].description
    # end

    it "should allow to create category" do 
      word = Faker::Lorem.word
      visit new_admin_category_path
      current_path.should == new_admin_category_path
      page.should_not have_content "You are not authorized to access this page."
      fill_in :category_name, with: word
      fill_in :category_description, with: "this is a test"
      # attach_file :category_banner_image, File.join(Rails.root, "spec/fixtures/files/test.png")
      click_button "Create Category"
      page.should have_content "Category was successfully created!"
      page.should have_content word
      page.should have_content "this is a test"
      Category.all.count.should == (@categories_ct + 1)
    end

    it "should not allow to create category with invalid information" do 
      visit new_admin_category_path
      fill_in :category_name, with: ""
      click_button "Create Category"
      page.should have_content "Please review the problems below:"
      page.should have_content "can't be blank"
      Category.all.count.should == @categories_ct
    end

    it "should allow to update category" do 
      word = Faker::Lorem.word
      visit edit_admin_category_path(@categories[1])
      current_path.should == edit_admin_category_path(@categories[1])
      page.should_not have_content "You are not authorized to access this page."
      fill_in :category_name, with: word
      click_button "Update Category"
      page.should have_content "Category was successfully updated!"
      page.should have_content word
    end

    it "should not allow to update category with invalid information" do 
      visit edit_admin_category_path(@categories[1])
      fill_in :category_name, with: ""
      click_button "Update Category"
      page.should have_content "Please review the problems below:"
      page.should have_content "can't be blank"
    end

    it "should allow destroy category" do 
      visit admin_category_path(@categories.first)
      click_link "Delete"
      current_path.should == admin_categories_path
      page.should have_content "Category was successfully deleted!"
      Category.all.count.should == (@categories_ct - 1)
    end
  end

end