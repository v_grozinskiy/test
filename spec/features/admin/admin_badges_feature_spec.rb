require "spec_helper"

describe "Admin badges page" do

  describe "for unauthorized users" do

    before :each do
      @user = FactoryGirl.create :user
      @badge = FactoryGirl.create :badge
    end

    it "should request users authentication" do
      [admin_badges_path, new_admin_badge_path, admin_badge_path(@badge), edit_admin_badge_path(@badge)].each do |page_name|
        visit page_name
        current_path.should == login_path
        page.should have_content "You should log in to access this page."
      end
    end

    it "should not allow no admin users" do
      login @user
      [admin_badges_path, new_admin_badge_path, admin_badge_path(@badge), edit_admin_badge_path(@badge)].each do |page_name|
        visit page_name
        current_path.should == root_path
        page.should have_content "You are not authorized to access this page."
      end
    end
  end

  describe "for authorized admin users" do

    before :each do
      @badges = []
      %w(funny inspiring viral news pop).each do |b|
        @badges << FactoryGirl.create(:badge, name: b)
      end
      @user = FactoryGirl.create :user, roles: [:admin, :super]
      login @user
    end

    it "should allow to browse badges" do 
      visit admin_badges_path
      current_path.should == admin_badges_path
      page.should_not have_content "You are not authorized to access this page."
      page.should have_content @badges[4].name
    end

    it "should allow to see badge detail" do 
      visit admin_badges_path
      click_link @badges[1].name
      current_path.should == admin_badge_path(@badges[1])
      page.should_not have_content "You are not authorized to access this page."
      page.should have_content @badges[1].name
      page.should have_content @badges[1].description
    end

    it "should allow to create badge" do 
      visit new_admin_badge_path
      current_path.should == new_admin_badge_path
      page.should_not have_content "You are not authorized to access this page."
      fill_in :badge_name, with: "Test Badge"
      fill_in :badge_description, with: "this is a test"
      attach_file :badge_icon, "spec/fixtures/files/test.png"
      click_button "Create Badge"
      page.should have_content "Badge was successfully created!"
      page.should have_content "Test Badge"
      page.should have_content "this is a test"
      Badge.all.count.should == 6
    end

    it "should not allow to create badge without icon" do 
      visit new_admin_badge_path
      fill_in :badge_name, with: "Test Badge"
      fill_in :badge_description, with: "this is a test"
      click_button "Create Badge"
      page.should have_content "Please review the problems below:"
      page.should have_content "can't be blank"
      Badge.all.count.should == 5
    end

    it "should not allow to create badge with invalid information" do 
      visit new_admin_badge_path
      fill_in :badge_name, with: ""
      click_button "Create Badge"
      page.should have_content "Please review the problems below:"
      page.should have_content "can't be blank"
      Badge.all.count.should == 5
    end

    it "should allow to update badge" do 
      visit edit_admin_badge_path(@badges[1])
      current_path.should == edit_admin_badge_path(@badges[1])
      page.should_not have_content "You are not authorized to access this page."
      fill_in :badge_name, with: "Test Badge"
      click_button "Update Badge"
      page.should have_content "Badge was successfully updated!"
      page.should have_content "Test Badge"
    end

    it "should not allow to update badge with invalid information" do 
      visit edit_admin_badge_path(@badges[1])
      fill_in :badge_name, with: ""
      click_button "Update Badge"
      page.should have_content "Please review the problems below:"
      page.should have_content "can't be blank"
    end

    it "should allow destroy badge" do 
      visit admin_badge_path(@badges[1])
      click_link "Delete"
      current_path.should == admin_badges_path
      page.should have_content "Badge was successfully deleted!"
      Badge.all.count.should == 4
    end
  end

end