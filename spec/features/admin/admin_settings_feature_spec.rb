require "spec_helper"

describe "Admin setting page" do

  describe "for unauthorized users" do

    before :each do
      @user = FactoryGirl.create :user
      create_categories
    end

    it "should request users authentication" do
      visit admin_settings_path
      current_path.should == login_path
      page.should have_content "You should log in to access this page."
    end

    it "should not allow non admin users" do
      login @user
      visit admin_settings_path
      current_path.should ==  root_path
      page.should have_content "You are not authorized to access this page."
    end
  end

  describe "for authorized admin users" do
    before :all do
      Setting.wt_test_key = 'test_value'
      @user = FactoryGirl.create :user, roles: [:admin, :super]
    end

    before :each do
      login @user
      visit admin_settings_path
    end

    it "should allow to browse settings" do 
      current_path.should == admin_settings_path
      page.should_not have_content "You are not authorized to access this page."
      page.should have_content 'test_key'
      page.should have_content 'test_value'
    end

    it "should allow to update a setting", js: true do
      click_link 'Edit'
      fill_in :setting_value, with: 'New test value'
      click_button 'Save'
      page.should have_content 'New test value'
    end

  end

end