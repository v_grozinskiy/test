require "spec_helper"

describe "The post" do

  before do
    create_categories

    @user = FactoryGirl.create :user
    @post = FactoryGirl.create :post, category: @categories.sample, creator: @user, title: 'title', description: 'description'
    @tag = FactoryGirl.create :tag, name: 'name'
    @post.authors << @user
    @post.tags << @tag
  end

  describe "search" do

    before :each do
      visit root_path
    end

    it "should search post with title", js: true do
      fill_in :q, with: 'title'
      submit_form = "$('#search-form').submit();"
      page.driver.execute_script(submit_form)
      page.should have_content @post.title
      page.should have_content @post.description
    end

    it "should search post with description", js: true do
      fill_in :q, with: 'DESC'
      submit_form = "$('#search-form').submit();"
      page.driver.execute_script(submit_form)
      page.should have_content @post.title
      page.should have_content @post.description
    end

    it "should search post with tag", js: true do
      fill_in :q, with: 'nam'
      submit_form = "$('#search-form').submit();"
      page.driver.execute_script(submit_form)
      page.should have_content @post.title
      page.should have_content @post.description
    end

    it "should not search post with invalid key", js: true do
      fill_in :q, with: 'invalid'
      submit_form = "$('#search-form').submit();"
      page.driver.execute_script(submit_form)
      page.should have_content 'No results found.'
      page.should_not have_content @post.title
    end

  end 

end