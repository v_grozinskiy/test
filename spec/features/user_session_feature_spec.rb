require "spec_helper"

describe "The user session" do

  before :each do
    @user = FactoryGirl.create(:user, password: "password", password_confirmation: "password")
  end

  it "should not work for invalid login" do
    visit login_path
    fill_in :user_session_login, with: "invalid_login"
    fill_in :user_session_password, with: "password"
    click_button "Login"

    page.should have_content "Invalid login or password"
  end

  it "should not work with invalid password" do
    visit login_path
    fill_in :user_session_login, with: @user.login
    fill_in :user_session_password, with: "invalid password"
    click_button "Login"

    page.should have_content "Invalid login or password"
  end

  it "should login" do
    visit login_path
    fill_in :user_session_login, with: @user.login
    fill_in :user_session_password, with: "password"
    click_button "Login"

    current_path.should == root_path
    page.should have_content "Welcome back #{@user.name}!"
  end

  it "should logout" do
    login @user
    visit logout_path
    # page.should have_content "Successfully logged out."
  end

  it "should redirect to a referrer when present" do
    visit user_path(@user)
    current_path.should == login_path
    page.should have_content "You should log in to access this page."

    fill_in :user_session_login, with: @user.login
    fill_in :user_session_password, with: "password"
    click_button "Login"

    current_path.should == user_path(@user)
  end

  it "should redirect to dashboard for admin users" do
    @user.update(roles: [:admin])
    login @user
    current_path.should == admin_dashboard_index_path
  end
end