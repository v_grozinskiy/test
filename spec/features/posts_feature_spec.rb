require "spec_helper"

describe "The post" do

  before do
    create_categories

    @users = FactoryGirl.create_list :user, 2
    @posts = FactoryGirl.create_list :post, 2, category: @categories.sample, creator: @users.first
    @posts.first.authors << @users[1]
  end

  describe "show" do
    before do
      @post = @posts.first
      @post_items = FactoryGirl.create_list :post_item, 5, post: @post, image: File.open(Rails.root.join('spec', 'fixtures', 'files', 'test.png'), 'r')
    end

    it "should show post detail" do
      visit root_path
      visit post_path(@post)
      page.should have_content @post.title
      page.should have_content @post.description
      page.should have_content @post.category.name
    end

    it "should show post items" do
      visit seo_post_path(prefix: @post.category.name, id: @post.id, slug: @post.slug)
      5.times do |i|
        page.should have_content @post_items[i].title
      end
    end
  end

end