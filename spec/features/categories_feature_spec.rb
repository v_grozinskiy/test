require "spec_helper"

describe "The category" do

  before do
    create_categories
  end

  describe "index" do
    before do
      @post1 = FactoryGirl.create :post, category: @categories.sample
      @post2 = FactoryGirl.create :post, category: @categories.sample, title: 'fdsafasf'
    end

    it "should show published posts" do
      visit root_path
      page.should have_content @post1.title
    end

    it "should not show unpublished posts" do
      @post2.unpublish
      visit root_path
      page.should_not have_content @post2.title
    end

    it "should responds with xml" do
      get categories_path, format: :xml
      xml = Nokogiri::XML(response.body)
      xml.to_s.should have_content @post1.title
    end
  end

  describe "search" do
    before do
      @posts = []
      @posts << FactoryGirl.create(:post, category: @categories.sample, title: "Test Post")
      @posts << FactoryGirl.create(:post, category: @categories.sample, description: "Eest Europe")
      @posts << FactoryGirl.create(:post, category: @categories.sample, title: "Unmatched", description: "Unmatched")
      visit root_path
    end

    it "should search post with title and description" do
      get root_path, q: "st"
      page.should have_content @posts.first.title
      page.should have_content @posts[1].title
    end

  end

  describe "show" do
    before do
      @posts = []
      @posts += FactoryGirl.create_list(:post, 2, category: @categories.first)
      @posts += FactoryGirl.create_list(:post, 2, category: @categories[1], title: 'dsafadsfas')
    end

    it "should show posts of the category" do
      visit category_path(@categories.first)
      page.should have_content @posts[0].title
      page.should have_content @posts[1].title
    end
 
    it "should responds with xml" do
      get category_path(@categories.first), format: :xml
      xml = Nokogiri::XML(response.body)
      xml.to_s.should have_content @posts[0].title
    end

  end

end