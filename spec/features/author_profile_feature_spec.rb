require "spec_helper"

describe "Author Profile" do

  before :each do
    @users = FactoryGirl.create_list :user, 2
  end

  it "should request authentication to see account page" do
    visit root_path
    page.should_not have_content "Account"
    visit edit_user_path
    current_path.should == login_path
    page.should have_content "You should log in to access this page."
  end

  # it "should not allow unauthorized users to access account page" do 
  #   login @users.first
  #   visit edit_user_path
  #   current_path.should_not == edit_user_path
  #   page.should have_content "You are not authorized to access this page."
  #   visit edit_user_path(@users[1])
  #   current_path.should_not == edit_user_path
  #   page.should have_content "You are not authorized to access this page."
  # end

  it "should allow user to see his account page" do 
    login @users.first
    visit edit_user_path
    current_path.should == edit_user_path
    find_field(:user_first_name).value.should eq @users.first.first_name
    find_field(:user_last_name).value.should eq @users.first.last_name
    find_field(:user_email).value.should eq @users.first.email
  end

  it "should not allow to update with invalid profile" do 
    login @users.first
    visit edit_user_path
    fill_in :user_email, with: "invalid_email"
    click_button "Update Profile"
    page.should have_content "Please review the problems below:"
    page.should have_content "should look like an email address."
  end

  it "should allow user to update his profile" do 
    login @users.first
    visit edit_user_path
    current_path.should == edit_user_path
    fill_in :user_first_name, with: "Test"
    fill_in :user_last_name, with: "User"
    fill_in :user_email, with: "testuser@xolator.com"
    click_button "Update Profile"
    current_path.should == edit_user_path
    find_field(:user_first_name).value.should eq 'Test'
    find_field(:user_last_name).value.should eq 'User'
    find_field(:user_email).value.should eq 'testuser@xolator.com'
    page.should have_content "Successfully updated profile."
    @users.first.reload.name.should == "Test User"
  end

end