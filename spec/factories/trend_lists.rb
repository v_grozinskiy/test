FactoryGirl.define do
  factory :trend_list do
    list_type               { Faker::Lorem.word }
    total_score             { rand(100000) }
    state                   { Faker::ArrayUtils.rand(%W(published draft)) }
  end
end
