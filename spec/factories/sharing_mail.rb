FactoryGirl.define do
  factory :sharing_mail do
    sender_name             { Faker::Name.name }
    sender_email            { Faker::Internet.email }
    receiver_email          { Faker::Internet.email }
    message                 { Faker::Lorem.paragraph(3) }
    target_id               { rand(1000) }
    target_type             { %W(Post PostItem).sample }
  end
end
