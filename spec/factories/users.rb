FactoryGirl.define do
  factory :user do
    first_name              { Faker::Name.name.split(' ')[0] }
    last_name               { Faker::Name.name.split(' ')[1] }
    email                   { Faker::Internet.email }
    login                   { "#{first_name} #{last_name}".gsub(/\W+/, '').underscore }
    byline                  { Faker::Lorem.word }
    facebook_id             { SecureRandom.hex }
    facebook_handle         { Faker::Lorem.word }
    twitter_id              { SecureRandom.hex }
    twitter_handle          { Faker::Lorem.word }
    password                "password"
    password_confirmation   "password"
    avatar                  nil
    roles                   [:user]
  end
end
