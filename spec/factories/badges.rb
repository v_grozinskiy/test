FactoryGirl.define do
  factory :badge do
    name                { Faker::Lorem.word }
    description         { Faker::Lorem.sentence }
    # icon                { File.open("spec/fixtures/files/test.png", "r") }
    icon_file_name      'test.jpg'
    icon_content_type   'image/jpeg'
    icon_file_size      { rand(1000) + 10000 }
    icon_updated_at     { Time.now }
    posts_count         { rand(10000) }
    nsfw                false
    active              true
  end
end
