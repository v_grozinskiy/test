# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :support_message do
    session_id          { SecureRandom.hex(8) }
    name                { Faker::Name.name }
    email               { Faker::Internet.email }
    phone               { Faker.numerify('(###)###-####') }
    message             { Faker::Lorem.sentence }
    ip_address          { Faker::Internet.ip_v4_address }
    browser             { 'Fake User Agent by XOlator' }
    referrer            { 'http://xolator.com' }

  end
end
