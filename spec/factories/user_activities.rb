FactoryGirl.define do
  factory :user_activity do
    user_id             { rand(10000) }
    item                nil
    action              { Faker::Lorem.word }
    action_object       { Faker::Lorem.sentence }
    automatic           false
  end
end
