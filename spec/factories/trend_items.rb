FactoryGirl.define do
  factory :trend_item do
    keyword_uuid            { rand(100000000) }
    post_id                 { rand(1000) }
    keyword                 { Faker::Lorem.word }
    display_keyword         { Faker::Lorem.word }
    format                  { Faker::Lorem.word }
    grouping                { Faker::Lorem.word }
    source_url              { Faker::Internet.http_url }
    image                   nil
  end
end
