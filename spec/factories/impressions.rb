FactoryGirl.define do
  factory :impression do
    ignore do
      impressionable { FactoryGirl.build :impressionable }
    end

    impressionable_type     { impressionable.class.name }
    impressionable_id       { impressionable.id }
    user_id                 nil
    controller_name         'categories'
    action_name             'index'
    request_hash            { SecureRandom.hex }
    ip_address              { Faker::Internet.ip_v4_address }
    session_hash            { SecureRandom.hex }
    message                 nil
    referrer                { Faker::Internet.http_url }
  end
end