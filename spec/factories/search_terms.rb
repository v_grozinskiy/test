# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :search_term do
    term                { Faker::Lorem.sentence }

    after(:create) do |search_term|
      create(:impression, impressionable: search_term)
    end
  end
end
