FactoryGirl.define do
  factory :post_item do
    post_id             { rand(10000) }
    slug                { SecureRandom.hex(8) }
    style               { PostItem.styles.keys.sample }
    title               { Faker::Lorem.word }
    text                { Faker::Lorem.sentence }
    # image               { File.open("spec/fixtures/files/test.png", "r") }
    image_file_name     'test.jpg'
    image_content_type  'image/jpeg'
    image_file_size     { rand(1000) + 10000 }
    image_updated_at    { Time.now }
    image_width         800
    image_height        600
    image_animated      false
    media               { Faker::Lorem.word }
    source              { Faker::Lorem.word }
    via                 { Faker::Lorem.word }
    bulleted            false
    visible             false
    position            { rand(100) }
    nsfw                false
  end
end
