FactoryGirl.define do
  factory :post_recommendation do
    post_id             { rand(10000) }
    recommended_post_id { rand(10000) }
  end
end
