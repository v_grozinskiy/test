FactoryGirl.define do
  factory :category do
    name                { Faker::Lorem.word }
    description         { Faker::Lorem.sentence }
    banner_image        nil
    posts_count         { rand(10000) }
    promotional         false
    nsfw                false
    active              true
  end
end
