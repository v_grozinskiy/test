FactoryGirl.define do
  factory :twitter_hash, class:Hash do
    info          {{ 'name' => Faker::Name.name,
                     'nickname' => Faker::Name.first_name,
                     'image' => Faker::Internet.http_url,
                     'urls' => { 'public_profile' => Faker::Internet.http_url },
                     'description' => Faker::Lorem.sentence,
                     'location' => "#{Faker::AddressUS.city}, #{Faker::AddressUS.state}, USA" }}
    uid           rand(1000000..9999999)
    provider      'twitter'
    credentials   {{ 'token' => SecureRandom.hex,
                     'secret' => SecureRandom.hex,
                     'expires_at' => 3.months.from_now }}
    extra         {{ 'user_hash' => {} }}

    initialize_with { attributes }
  end

  factory :facebook_hash, class:Hash do
    info          {{ 'name' => Faker::Name.name,
                     'nickname' => Faker::Name.first_name,
                     'image' => Faker::Internet.http_url,
                     'urls' => { 'public_profile' => Faker::Internet.http_url },
                     'description' => Faker::Lorem.sentence,
                     'location' => "#{Faker::AddressUS.city}, #{Faker::AddressUS.state}, USA" }}
    uid           rand(1000000..9999999)
    provider      'facebook'
    credentials   {{ 'token' => SecureRandom.hex,
                     'secret' => SecureRandom.hex }}
    extra         {{ 'user_hash' => {} }}

    initialize_with { attributes }
  end
end