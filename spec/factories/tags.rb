FactoryGirl.define do
  factory :tag do
    name                { Faker::Lorem.word }
    posts_count         { rand(10000) }
    nsfw                false
    active              true
  end
end
