FactoryGirl.define do
  factory :post_author do
    post_id             { rand(10000) }
    user_id             { rand(10000) }
    position            { rand(20) }
  end
end
