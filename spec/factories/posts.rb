FactoryGirl.define do
  factory :post do
    title                       { Faker::Lorem.sentence }
    description                 { Faker::Lorem.sentence }
    promo_title                 { Faker::Lorem.word }
    promo_description           { Faker::Lorem.sentence }
    # promo_image                 { File.open("spec/fixtures/files/test.png", "r") }
    promo_image_file_name       'test.jpg'
    promo_image_content_type    'image/jpeg'
    promo_image_file_size       { rand(1000) + 10000 }
    promo_image_feed_file_size  { rand(1000) + 1000 }
    promo_image_updated_at      { Time.now }
    category_id                 { rand(10000) }
    style                       { Faker::Lorem.word }
    format                      { Post.formats.keys.sample }
    creator_id                  { rand(10000) }
    nsfw                        false
    published_at                { 1.day.ago }
    state                       'saved'

    after(:create) do |post|
      create(:post_item, post: post)
      post.publish
    end
  end
end
