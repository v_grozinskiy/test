module SpecTestHelper   

  def login(user)
    visit login_path
    fill_in :user_session_login, with: user.login
    fill_in :user_session_password, with: "password"
    click_button "Login"
  end

  def logout
    visit logout_path
  end

  def create_user_session(user)
    post user_session_path, user_session: {login: user.login, password: user.password}
  end

  module CachingTestHelpers
    #ActionController::Base.public_class_method :page_cache_path
 
    module ResponseHelper
      def action_cached?(request)
        request.path.is_action_cached?
      end
 
      def page_cached?(request)
        request.path.is_page_cached?
      end

      def fragment_cached?(cache_key)
        Rails.cache.exist? "views/#{cache_key}"
      end
    end
    ActionDispatch::TestResponse.send(:include, ResponseHelper)
  end

  def create_categories
    @categories = []
    %w(funny inspiring viral news pop music shows).each do |n|
      c = Category.find(n.downcase) rescue nil
      c ||= FactoryGirl.create(:category, name: n)
      @categories << c
    end
  end

end
 
class String
  def is_action_cached?
    Rails.cache.exist?("views/#{self}")
  end
 
  def is_page_cached?
    File.exists? ActionController::Base.page_cache_path(self)
  end

end
 
RSpec.configure do |config|
  config.include SpecTestHelper::CachingTestHelpers, :type => :request
end