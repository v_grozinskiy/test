module WaitForAjax
  def wait_for_video_load
    Timeout.timeout(Capybara.default_wait_time) do
      loop until finished_all_ajax_requests?
    end
  end

  def finished_all_ajax_requests?
    !page.evaluate_script('$("#contest_escape_submission_video_id").val()').blank?
  end
end

RSpec.configure do |config|
  config.include WaitForAjax, type: :feature
end