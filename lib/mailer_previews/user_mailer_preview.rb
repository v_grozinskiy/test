if ['test','development'].include?(Rails.env)

  class UserMailerPreview < ActionMailer::Preview

    def password_reset_instructions
      with_transaction do
        @user = FactoryGirl.create(:user)
        @user.reset_perishable_token!
        UserMailer.password_reset_instructions(@user)
      end
    end

    def share_mail
      with_transaction do
        @category = FactoryGirl.create(:category, name: 'funny')
        @target = FactoryGirl.create(:post, category: @category, description: Faker::Lorem.paragraph(2), promo_image: File.open("spec/fixtures/files/test.png", "r"))
        @sharing_mail = FactoryGirl.create(:sharing_mail, target: @target)
        @url = 'http://whatstrending.com/funny/6266-patton-oswalt-birthday'
        UserMailer.share_mail(@sharing_mail, @sharing_mail.receiver_email, @url)
      end
    end

    def admin_support_message
      with_transaction do
        @support_message = FactoryGirl.create :support_message
        UserMailer.admin_support_message(@support_message)
      end
    end

    private

    def with_transaction(&block)
      DatabaseCleaner.strategy = :transaction
      DatabaseCleaner.start
      begin
        require "factory_girl"
        FactoryGirl.reload
        (yield block).tap { DatabaseCleaner.clean }
      rescue Exception => e
        DatabaseCleaner.clean
        raise e
      end
    end
  end

end