namespace :stats do

  task start: :environment do
    CategoryWorker.start
    PostWorker.start
    VelocityWorker.start
    TrendWorker.start
    SubmissionWorker.start
  end

  task impressions_cleanup: :environment do
    dups = 0

    Post.published.find_each do |post|
      imp = post.impressions.where(action_name: :show).where('referrer LIKE ?', "%/#{post.id}-%")
      next unless imp.count > 0
      # imp.destroy_all
      #puts [post.id, post.counts_for(:show), imp.count].join(' > ')
      dups += imp.count
    end

    puts "Duplicates: #{dups}"
  end

  task impressions_rollups: :environment do
    puts "Impressions Rollups",""
    start_time = Time.now
    first_date = Impression.order('created_at ASC').first.created_at.to_date
    last_date = (Date.today - ImpressionRollup.timeframe).to_date

    info = ActiveRecord::Base.connection.instance_variable_get(:@config)

    fpath = '/vol/whatstrending.com/backups' if Rails.env.production?
    fpath = '/vol/app.testing.whatstrending.com/backups' if Rails.env.staging?
    fpath ||= File.absolute_path('..')

    sqlcmd = ["mysqldump"]
    sqlcmd << "-u" << info[:username]
    sqlcmd << "-p#{info[:password]}" unless info[:password].blank?
    sqlcmd << "-h" << info[:hostname] unless info[:hostname].blank?
    sqlcmd << "-h" << info[:host] unless info[:host].blank?
    sqlcmd << info[:database]
    sqlcmd = sqlcmd.join(' ')

    (first_date..last_date).each do |v| 
      puts v.to_s
      puts " #{Impression.where('DATE(created_at) = ?', v.to_s).count} impressions"
      
      # Dump & gzip old data
      puts " > data dump"
      fname = "impressions-#{v.to_s(:db)}.sql.gz"
      dumpsql = sqlcmd + " #{Impression.table_name} --where=\"DATE(created_at) = '#{v.to_s}'\" --no-create-info --compact | gzip -c > #{File.join(fpath, fname)}"
      `#{dumpsql}`

      # TODO: Move to S3? We are storing full sql there as well.

      puts " > rollup"
      ImpressionRollup.on_date(v, true)
    end

    puts "","Completed in #{Time.now-start_time} seconds",""
  end

end