require 'rest_client'


namespace :followers_count do

  task refresh: :environment do
    def config
      {
        facebook: 'https://graph.facebook.com/whatstrending',
        twitter: 'https://cdn.syndication.twimg.com/widgets/followbutton/info.json?lang=en&screen_names=whatstrending',
        youtube: 'https://gdata.youtube.com/feeds/api/users/whatstrending?alt=json',
        google_plus: 'https://www.googleapis.com/plus/v1/people/102598412023069083727?key=AIzaSyBw-1I1LoaL0qiaQeTR0gfobcnvTY8-rI4'
      }
    end

    config.each_pair do |k, v|
      response = RestClient.get(v)
      json = JSON.parse(response)

      count = case k
        when :facebook; json['likes']# rescue nil
        when :twitter; json[0]['followers_count']# rescue nil
        when :youtube; json['entry']['yt$statistics']['subscriberCount']# rescue nil
        when :google_plus; json['plusOneCount']# rescue nil
        else; nil
      end

      Setting.send("follow_count_#{k.to_s}=", count.to_i) unless count.blank?
    end
  end
end