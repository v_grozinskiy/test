module MediaAdapters
  module Vine

    extend MediaAdapters::Base
    extend self


    # Return ID of video
    def parse(str)
      rgx_url = /.*(http(s)?\:\/\/).*(vine\.co\/v\/)([a-z0-9\-]{11}).*/im
      
      case str
        # when normal vine.co url
        when rgx_url
          id = str.gsub(rgx_url, '\4')
          (id != str ? id : nil)
        else
          nil
      end
    end


    # Get information from Vine's oEmbed endpoint
    def oembed(id)
      super('https://vine.co/oembed.json', permalink_url(id))
    end


    # Create usable URL for this
    def permalink_url(id)
      "https://vine.co/v/#{id}"
    end


    # Get the proper embed code.
    def embed_code(id)
      "<iframe class=\"vine-embed\" src=\"https://vine.co/v/#{id}/embed/postcard\" width=\"600\" height=\"600\" frameborder=\"0\"></iframe><script async src=\"//platform.vine.co/static/scripts/embed.js\"></script>"
    end

  end
end