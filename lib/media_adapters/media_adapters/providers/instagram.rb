module MediaAdapters
  module Instagram

    extend MediaAdapters::Base
    extend self


    # Return ID of video
    def parse(str)
      rgx_url = /.*(http(s)?:\/\/)?(www\.)?(instagram\.com|instagr\.am)\/p\/?([a-z0-9\-\_]{10}).*/im
      
      case str
        # when normal instagram.com url
        when rgx_url
          id = str.gsub(rgx_url, '\5')
          (id != str ? id : nil)
        else
          nil
      end
    end


    # Get information from Instagram's oEmbed endpoint
    def oembed(id)
      super('https://api.instagram.com/oembed/', permalink_url(id))
    end


    # Create usable URL for this
    def permalink_url(id)
      "https://instagram.com/p/#{id}"
    end


    # Get the proper embed code.
    def embed_code(id)
      "<iframe src=\"//instagram.com/p/#{id}/embed/\" width=\"612\" height=\"710\" frameborder=\"0\" scrolling=\"no\" allowtransparency=\"true\"></iframe>"
    end

  end
end