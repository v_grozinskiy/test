module MediaAdapters
  module Hulu

    extend MediaAdapters::Base
    extend self


    # Return ID of video
    def parse(str)
      rgx_url = /.*(http(s)?:\/\/)?(www\.)?hulu.com\/watch\/?(\d+)?.*/im
      rgx_embed_url = /.*(http(s)?:\/\/)?(www\.)?hulu.com\/embed(\/|\.html\?eid=)([\w-]+)?.*/im
      
      case str
        # when normal hulu.com url
        when rgx_url
          id = str.gsub(rgx_url, '\4')
          url = oembed(id)['embed_url'] rescue nil
          parse(url)

        # when hulu.com embed code url
        when rgx_embed_url
          id = str.gsub(rgx_embed_url, '\5')
          (id != str ? id : nil)
        else
          nil
      end
    end


    # Get information from Hulu's oEmbed endpoint
    def oembed(id)
      super('http://www.hulu.com/api/oembed.json', permalink_url(id))
    end


    # Create usable URL for this
    def permalink_url(id)
      case id
        when /^\d+$/; "http://www.hulu.com/watch/#{id}"
        else;         "http://www.hulu.com/embed/#{id}"
      end
    end


    # Get the proper embed code. Hulu uses different IDs in their embed code, so collect info from oEmbed to get HTML
    def embed_code(id)
      info = oembed(id) rescue {}
      info['html']
    end

  end
end