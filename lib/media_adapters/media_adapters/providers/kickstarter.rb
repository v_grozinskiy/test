module MediaAdapters
  module Kickstarter

    extend MediaAdapters::Base
    extend self


    # Return ID of video
    def parse(str)
      rgx_url = /.*(http(s):\/\/)?(www\.)?kickstarter.com\/projects\/[\w\d]+\/([\w-]+)?.*/im

      case str
        # when normal kickstarter.com url
        when rgx_url
          id = str.gsub(rgx_url, '\4')
          (id != str ? id : nil)
        else
          nil
      end
    end


    # Get information from Kickstarter's oEmbed endpoint
    def oembed(keyword)
      super('https://www.kickstarter.com/services/oembed.json', permalink_url(id))
    end


    # Create usable URL for this
    def permalink_url(id)
      "https://www.kickstarter.com/projects/0/#{id}" # Kickstarter works on slug, not user id, so id can be whatever
    end


    # Get the proper embed code.
    def embed_code(id)
      "<iframe src=\"https://www.kickstarter.com/projects/0/#{id}/widget/video.html\" frameborder=\"0\" scrolling=\"no\" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>"
    end

  end
end