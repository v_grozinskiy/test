module MediaAdapters
  module YouTube

    extend MediaAdapters::Base
    extend self


    # Return ID of video
    def parse(str)
      rgx_url = /.*(youtube\.com\/)((v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))(\??v?=?)([^#\&\?\"]*).*/im
      rgx_shorturl = /.*(youtu\.be\/)([^#\&\?\"]*).*/im

      case str
        # when normal youtube.com url
        when rgx_url
          id = str.gsub(rgx_url, '\8')
          (id != str ? id : nil)
        # when youtu.be short url
        when rgx_shorturl
          id = str.gsub(rgx_shorturl, '\2')
          (id != str ? id : nil)
        else
          nil
      end
    end


    # Get information from YT's oEmbed endpoint
    def oembed(id)
      super('http://www.youtube.com/oembed.json?&format=json', permalink_url(id), as: :json)
    end


    # Create usable URL for this
    def permalink_url(id)
      "https://youtube.com/watch?v=#{id}"
    end


    # Get the proper embed code.
    def embed_code(id)
      "<iframe src=\"//www.youtube.com/embed/#{id}\" width=\"640\" height=\"360\" frameborder=\"0\" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>"
    end

  end
end