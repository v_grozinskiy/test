module MediaAdapters
  module Vimeo

    extend MediaAdapters::Base
    extend self


    # Return ID of video
    def parse(str)
      rgx_url = /.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/)|(video\/))?([0-9]+).*/im

      case str
        # when normal vimeo.com url
        when rgx_url
          id = str.gsub(rgx_url, '\6')
          (id != str ? id : nil)
        else
          nil
      end
    end


    # Get information from Vimeo's oEmbed endpoint
    def oembed(id)
      super('http://vimeo.com/api/oembed.json', permalink_url(id))
    end


    # Create usable URL for this
    def permalink_url(id)
      "https://vimeo.com/#{id}"
    end


    # Get the proper embed code.
    def embed_code(id)
      "<iframe src=\"//player.vimeo.com/video/#{id}\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>"
    end

  end
end