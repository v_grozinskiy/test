module MediaAdapters
  module TwitchTv

    extend MediaAdapters::Base
    extend self


    # Return ID of video
    def parse(str)
      rgx_url = /.*(http(s):\/\/)?(www\.)?twitch.tv\/([\w\d_-]+)\/([\d\w])\/(\d+)?.*/im # id not has slash but they add it in there
      rgx_video_embed = /twitch\.tv\/(widgets\/archive_embed_player\.swf|swflibs\/TwitchPlayer\.swf)/im

      case str
        # when normal twitch.tv url
        when rgx_url
          id = str.gsub(rgx_url, '\5\6')
          (id != str ? id : nil)

        # when twitch embed url. look for videoId in <object>
        when rgx_video_embed
          id = str.gsub(/.*videoId\=([\w\d]\d+)?.*/im, '\1')
          (id != str ? id : nil)

        else
          nil
      end
    end


    # Get information from Twitch's video API endpoint (not oEmbed, but works ok enough)
    def oembed(id)
      super("https://api.twitch.tv/kraken/videos/#{id}", '', as: :json)
    end


    # Create usable URL for this
    def permalink_url(id)
      "http://www.twitch.tv/videos/#{id.gsub(/^([\w\d])(.*)/, '\1/\2')}"
    end


    # Get the proper embed code.
    def embed_code(id)
      "" 
      # TODO : something is borked and these embeds (from their site) are not playing videos...
      # <object bgcolor='#000000' 
      #         data='http://www.twitch.tv/widgets/archive_embed_player.swf' 
      #         height='300' 
      #         type='application/x-shockwave-flash' 
      #         width='400'> 
      #   <param  name='movie' 
      #           value='http://www.twitch.tv/widgets/archive_embed_player.swf' /> 
      #   <param  name='allowScriptAccess' 
      #           value='always' /> 
      #   <param  name='allowNetworking' 
      #           value='all' /> 
      #   <param  name='allowFullScreen' 
      #           value='true' /> 
      #   <param  name='flashvars' 
      #           value='videoId=#{id}&start_volume=25&auto_play=false' />
      # </object>
    end

  end
end