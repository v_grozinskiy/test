module MediaAdapters
  module FunnyOrDie

    extend MediaAdapters::Base
    extend self


    # Return ID of video
    def parse(str)
      rgx_url = /.*(http(s):\/\/)?(www\.)?funnyordie.com\/(videos|embed)\/?([\w]+)?.*/im
      
      case str
        # when normal funnyordie.com url
        when rgx_url
          id = str.gsub(rgx_url, '\5')
          (id != str ? id : nil)
        else
          nil
      end
    end


    # Get information from FoD's oEmbed endpoint
    def oembed(id)
      super('http://www.funnyordie.com/oembed.json', permalink_url(id))
    end


    # Create usable URL for this
    def permalink_url(id)
      "http://www.funnyordie.com/videos/#{id}"
    end


    # Get the proper embed code.
    def embed_code(id)
      "<iframe src=\"http://www.funnyordie.com/embed/#{id}\" frameborder=\"0\" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>"
    end

  end
end