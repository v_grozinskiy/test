module MediaAdapters
  extend self


  # Return current defined adapter
  def adapter
    return @adapter if @adapter
    @adapter
  end

  # Assign an adapter
  def adapter=(adapter_name)
    @adapter = const_get(adapter_name.to_s.camelize)
  end

  # Return list of available adapters
  def list
    return @@adapters if defined?(@@adapters)
    @@adapters = self.constants.map{|c| Module === const_get(c) ? const_get(c) : nil}.compact
  end

  # Get adapter
  def get(adapter)
    const_get(adapter)
  end

  # Identify an item by running through various adapter parse methods
  def identify(str)
    list.map do |adapter|
      id = adapter.parse(str) rescue nil
      id.present? ? {adapter: adapter.adapter_name, id: id, adapter_slug: adapter.adapter_name.downcase} : nil
    end.flatten.compact.uniq
  rescue
    []
  end

  # Get embed code for item
  def embed_code(str, *args)
    opts = args.extract_options!.reverse_merge({prepend: '', append: ''})
    item = identify(str).first
    (opts[:prepend] + get(item[:adapter]).embed_code(item[:id]) + opts[:append]).gsub(/\%\{([a-z0-9\_]+)\}/i){item[Regexp.last_match[1].to_sym] || ''}
  rescue
    nil
  end


  module Base
    extend self

    def adapter_name
      self.to_s.gsub(/(.*\:\:)?(.*)/, '\2')
    end

    # Fetch and parse content from oEmbed endpoint
    def get_from_oembed(oembed_url, item_url, *args)
      opts = args.extract_options!
      opts.reverse_merge!({id_key: :url})

      # Make oEmbed query URL
      url = Addressable::URI.parse(oembed_url)
      url.query_values = {"#{opts[:id_key]}" => item_url}.reverse_merge(url.query_values || {})

      # Gather page contents from oEmbed endpoint
      str = get_page_contents(url.normalize.to_s)

      # Different places do xml, json, or both. Anything else, dunno?
      if oembed_url.match(/\.json/) || opts[:as] == :json
        parse_json(str)
      elsif oembed_url.match(/\.xml/) || opts[:as] == :xml
        parse_xml(str)
      else
        str
      end
    rescue => err
      puts "ERR 5: #{err}"
      {}
    end



  private

    def get_page_contents(url)
      Timeout::timeout(5) do # 5 seconds
        open(Addressable::URI.parse(url), read_timeout: 5, "User-Agent" => WT_USER_AGENT, allow_redirections: :all)
      end
    rescue => err
      puts "ERR1: #{err}"
    end

    # Parse JSON with JSON.load
    def parse_json(str)
      JSON.load(str)
    end

    # Parse HTML content with Nokogiri::HTML
    def parse_html(str)
      Nokogiri::HTML(str)
    end

    # Parse XML content with Nokogiri::HTML
    def parse_xml(str)
      Nokogiri::XML(str)
    end

  end
end