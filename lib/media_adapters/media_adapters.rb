
# Load each of the respective files
require File.join(File.dirname(__FILE__), 'media_adapters/base.rb')

# Load available adapter provider modules
Dir.glob(File.join(File.dirname(__FILE__), 'media_adapters/*/*.rb')).each{|r| require r }
