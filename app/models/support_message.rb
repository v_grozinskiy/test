class SupportMessage < ActiveRecord::Base

  include Uuidable
  extend FriendlyId

  friendly_id :uuid, use: [:finders]

  belongs_to :user
  has_many :impressions, primary_key: 'session_id', foreign_key: 'session_id'


  validates :email,     presence: true,     email: true
  validates :name,      presence: true,     length: {in: 2..200}
  validates :phone,     allow_blank: true,  length: {in: 2..200}, format: {with: /\A[\d\s\.\-\+\(\)]+\Z/}
  validates :message,   presence: true,     length: {in: 2..5000}

  after_create :send_admin_email


private

  def send_admin_email
    UserMailer.delay.admin_support_message(self)
  end

end
