class Badge < ActiveRecord::Base

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  has_attached_file :icon, 
    styles: { large: ["100x100#",:jpg], medium: ["50x50#",:jpg], icon: ["16x16#",:jpg] }, 
    default_style: :medium, default_url: "/images/:class/:attachment/:style.png"

  has_and_belongs_to_many :posts, join_table: 'post_badges'

  validates :name, presence: true
  validates_uniqueness_of :name
  validates :name, length: {maximum: 250}
  validates :description, length: {maximum: 500}, allow_blank: true, allow_nil: true
  validates_attachment :icon, 
    presence: true,
    content_type: { content_type: ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'] },
    size: { in: 0..100.kilobytes }
end
