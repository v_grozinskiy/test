class UserActivity < ActiveRecord::Base

  belongs_to :item, polymorphic: true
  belongs_to :user

  validates :user, :item, presence: true
end
