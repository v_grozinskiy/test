class ImpressionRollup < ActiveRecord::Base

  belongs_to :impressionable, polymorphic: true


  def self.timeframe; 30.days; end

  def self.on_date(d=nil,to_delete=false)
    raise "Date is blank" if d.blank?
    d = d.to_date # enforce
    return "Date not in timeframe" if d > (Date.today - self.timeframe).to_date

    # Create rollups, unique and total
    Impression.where('DATE(created_at) = ?', d).group(:impressionable_type, :impressionable_id, :controller_name, :action_name, :view_name, :message).select('COUNT(DISTINCT request_hash) as imp_ct, COUNT(DISTINCT session_hash) as imp_uniq_ct, impressionable_type, impressionable_id, controller_name, action_name, view_name, message').each do |v|
      self.where(rollup_date: d, impressionable_type: v.impressionable_type, impressionable_id: v.impressionable_id, controller_name: v.controller_name, action_name: v.action_name, view_name: v.view_name, message: v.message).first_or_create.update(impression_count: v.imp_ct, unique_impression_count: v.imp_uniq_ct)
    end

    # Delete all associated records, if true
    Impression.where('DATE(created_at) = ?', d).delete_all if to_delete

    true
  rescue => err
    puts "Rollup Error: #{err}"
    false
  end

private

end
