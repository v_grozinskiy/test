class Post < ActiveRecord::Base

  require 'addressable/uri'


  # VARIABLES -----------------------------------------------------------------

  enum format: {
    article:  0,
    video:    1,
  }

  # Share platforms posts can be seen in, based on screen/device width
  #   :all, shown on all screens
  #   :web, shown only on web devices
  #   :tablet, shown only on tablet and web devices
  #   :mobile, shown only on mobile devices

  SHARE_PLATFORMS = {
    facebook:     :all,
    twitter:      :all,
    pinterest:    :all,
    googleplus:   :tablet,
    reddit:       :tablet,
    stumbleupon:  :web,
    whatsapp:     :mobile,
    email:        :all,
  }


  # EXTENSIONS ----------------------------------------------------------------

  # FriendlyId
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  # Paperclip
  has_attached_file :promo_image, 
    styles: { thumbnail: ['208x110#',:jpg], feed: ['470x250#',:jpg], large: ['700x372#',:jpg] },
    processors: [:thumbnail, :compression],
    default_style: :feed, default_url: "/images/:class/:attachment/:style.png"

  # Impressionist
  is_impressionable
  has_many :impression_rollups, as: :impressionable


  # STATE MACHINE -------------------------------------------------------------
  state_machine initial: :draft do

    # --- Callbacks ---
    before_transition any - :published => :published, do: :after_publish
    before_transition :published => :unpublished, do: :after_unpublish
    before_transition any => :published do |a,s|
      if a.promo_image_file_name.blank?
        a.errors.add(:promo_image, 'must be included before publishing')
        throw :halt
      end
    end

    # --- Events ---
    event :submit do
      transition :draft => :saved, [:saved, :unpublished, :published, :queued] => same
    end
    event :publish do
      transition any => :published
    end
    event :queue do
      transition any - :published => :queued
    end
    event :unpublish do
      transition :published => :unpublished
    end

    # --- State-specific Validations & Callbacks ---
    state :draft do
      # Validations
      validates :creator_id, presence: true
    end

    state :saved, :published, :unpublished, :queued do
      # Validations
      validates :category_id, :title, :creator_id, presence: true
      validates :description, length: { in: 2..1500 }
      validates :title, length: { in: 2..250 }
      validates :promo_title, length: {maximum: 250}, allow_blank: true, allow_nil: true
      validates :promo_description, length: {maximum: 1500}, allow_blank: true, allow_nil: true
      # validates_uniqueness_of :slug
      # validates :slug, length: { in: 5..48 }, uniqueness: { case_sensitive: false }

      # Callbacks
      after_save :delay_cache_post_count_on_tags, :delay_cache_post_count_on_badges
    end

    state :published, :queued do
      validates :post_items, length: {minimum: 1, message: 'must include at least one item.'}
    end
  end


  # ASSOCIATIONS --------------------------------------------------------------

  belongs_to :category, counter_cache: true
  belongs_to :creator, class_name: 'User'
  has_and_belongs_to_many :badges, join_table: 'post_badges'
  has_and_belongs_to_many :tags, join_table: 'post_tags'
  has_many :post_recommendations, dependent: :destroy
  has_many :recommended_posts, through: :post_recommendations
  has_many :post_authors, -> { order 'position' }, dependent: :destroy
  has_many :authors, through: :post_authors, source: :user
  has_many :post_items, -> { order 'position' }, dependent: :destroy
  has_one :post_queue, dependent: :destroy

  accepts_nested_attributes_for :post_authors, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :post_items, reject_if: :all_blank, allow_destroy: true


  # CALLBACKS -----------------------------------------------------------------
  # (most callbacks are in state_machine.)

  after_post_process :delay_get_feed_promo_image_file_size
  before_destroy :decrease_cache_count_on_tags, :decrease_cache_count_on_badges
  before_validation :generate_slug, :image_from_url
  # after_save :delay_reload_share_counts!


  # VALIDATIONS ----------------------------------------------------------------
  # (most validations are in state_machine.)

  attr_accessor :promo_image_url

  validates_attachment :promo_image, 
    content_type: { content_type: ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'] },
    size: { less_than: 10.megabytes }


  # SCOPES --------------------------------------------------------------------

  scope :active,              -> { where.not(state: :draft) }
  scope :draft,               -> { where(state: :draft) }
  scope :queued,              -> { where(state: :queued) }
  scope :recent,              -> { order(published_at: :desc, created_at: :desc) }
  scope :created,             -> { order(created_at: :desc) }
  scope :sfw,                 -> { active.where(nsfw: false) }
  scope :google_news,         -> { where(google_news: true) }
  scope :visible_on_homepage, -> { published.where(visible_homepage: true) }
  scope :published,           -> { where(state: :published).where('published_at < ?', Time.now) }
  scope :not_published,       -> { where( Post.arel_table[:state].not_eq(:published).or(Post.arel_table[:published_at].gteq(Time.now)) ) }
  scope :nested_category, ->(c) { (ids = c.self_and_descendants.map(&:id)).blank? ? none : where(category_id: ids) }

  # Uses setting to match against weighted posts
  scope :recommended,         -> { (ids = Setting.recommended_post_ids).blank? ? none : visible_on_homepage.weighted_homepage.where(posts: {id: ids}) }
  scope :unrecommended,       -> { (ids = Setting.recommended_post_ids).blank? ? visible_on_homepage : visible_on_homepage.where.not(posts: {id: ids}) }
  scope :unrecommended_all,   -> { (ids = Setting.recommended_post_ids).blank? ? where(1) : where.not(posts: {id: ids}) }

  # Order by weight (recommended, then everything else)
  scope :weighted_homepage, -> { (ids = Setting.recommended_post_ids).blank? ? recent : order("FIELD(#{Post.table_name}.id,#{ids.reverse.map(&:to_i).join(',')}) DESC, #{Post.table_name}.published_at desc") }

  # Search scope that includes tags
  scope :search, ->(q) { joins("LEFT JOIN post_tags ON post_tags.post_id = posts.id LEFT JOIN tags ON tags.id = post_tags.tag_id").group('posts.id').where("lower(posts.title) LIKE ? OR lower(posts.description) LIKE ? OR lower(tags.name) LIKE ?", "%#{q}%", "%#{q}%", "%#{q}%") }


  # CLASS METHODS -------------------------------------------------------------

  # Search top posts from Impressionist data
  def self.top(t=:viewed,d=1.day)
    o = case t
      when :shared, :shares;      :share
      when :commented, :comments; :comment
      else;                       :show
    end
    obj = Post.published.joins("LEFT JOIN impressions on posts.id=impressions.impressionable_id AND impressions.impressionable_type='Post'").group('posts.id').where(impressions: {action_name: o}).select('posts.*, COUNT(impressions.id) as top_count').order('top_count desc')
    obj = obj.where('impressions.created_at > ?', Time.now-d) unless d == :all
    obj
  end

  def self.find_old_post(slug,year='2014',month='01')
    self.where("CONVERT(DATE_FORMAT(created_at,'%Y-%m-01 00:00:00'),DATETIME) = ?", "#{year.to_i}-#{'%02d' % month.to_i}-01 00:00:00").where('LOWER(slug) LIKE ?', "%#{slug.downcase[0,24]}%").first
  end


  def self.delete_homepage_cache
    Rails.cache.delete_matched('*homepage/featured_homepage_posts')
    Rails.cache.delete_matched('*homepage/latest_video_posts/*')
    Rails.cache.delete_matched('*homepage/latest_posts/*')
    Rails.cache.delete_matched('*homepage/featured_posts/*')
    Rails.cache.delete_matched('*homepage/all/*')
    true
  end

  def self.delete_global_cache
    Rails.cache.delete_matched('global/*')
    true
  end


  # METHODS -------------------------------------------------------------------

  # Get the most popular tags for this post
  def news_keywords
    self.tags.order('posts_count DESC').limit(10).pluck(:name)
  end

  def counts_for(v=:show, b=nil, d=1.day, *args)
    opts = args.extract_options!
    v = v.to_s.singularize.to_sym
    if b
      counts = impressions.where(action_name: v, created_at: (Time.now - (b + d))..(Time.now - b))
    else
      counts = impressions.where(action_name: v)
    end

    counts = counts.select(opts[:filter]).distinct unless opts[:filter].blank?
    counts.count || 0
  end

  def velocity_delta(v=:show, b=1.day)
    n,p = counts_for(v,0,b), counts_for(v,b,b)
    n - p
  end

  def velocity_score(v=:show, b=1.day)
    n,p = counts_for(v,0,b), counts_for(v,b,b)
    if p == 0
      d = ((Time.now-self.published_at) / 1.hour)# / 24
      n.to_f/d.to_f rescue 0.0
    else
      ((n-p).to_f / 24) rescue 0
    end
  end

  def text; self.description; end

  def tag_list
    tags.map(&:name).join(', ')
  end

  def tag_list=(list)
    list = list.split(',').reject(&:blank?).map(&:strip).map(&:downcase)
    self.tag_ids = Tag.find_or_create_all_with_like_by_name(list).map(&:id)
  end

  def normalize_friendly_id(string)
    super[0..48].gsub(/\-$/, '')
  end

  def as_json(options={})
    super(options).merge({
      text:     title,
      image:    promo_image.url(:thumbnail)})
  end

  def video_duration
    self.post_items.videos.first.duration rescue nil
  end

  def delay_reload_share_counts!
    self.delay.reload_share_counts! if self.published?
  end

  def reload_share_counts!
    Rails.cache.fetch([self,'share_counts'], force: true) do
      obj = {}
      Post::SHARE_PLATFORMS.keys.each{|p| obj[p] = self.get_total_impression_count(action_name: (p == :email ? :create : :share), message: p) }
      obj
    end
    Rails.cache.delete_matched("posts/#{self.id}-*/show/*") # Delete post#show main wrapper
    Rails.cache.delete_matched("posts/#{self.id}-*/share/*") # Delete post#show share button wrappers
  end

  def share_counts
    Rails.cache.read([self,'share_counts']) || {}
  end

  def delete_cache
    Rails.cache.delete_matched("posts/#{self.id}-*")
  end


  # Update promo_image :feed style file size, for use with RSS. Delay method for calling as :after_save
  def delay_get_feed_promo_image_file_size; self.delay_for(15.seconds).get_feed_promo_image_file_size; end
  def get_feed_promo_image_file_size
    begin
      Timeout::timeout(5) do # 5 seconds
        uri, resp = Addressable::URI.parse(self.promo_image.url(:feed)), nil
        Net::HTTP.start(uri.host,80){|http| resp = http.head(uri.path, {"User-Agent" => WT_USER_AGENT}) }
        self.update_column(:promo_image_feed_file_size, resp.content_length) rescue nil
      end
    rescue => err
      nil
    end
  end


  # Update posts count for tags. Delay method for calling as :after_save
  def delay_cache_post_count_on_tags; self.delay_for(15.seconds).cache_post_count_on_tags; end
  def cache_post_count_on_tags; tags.each {|t| t.update_attribute(:posts_count, t.posts.active.size)}; end

  # Update posts count for badges. Delay method for calling as :after_save
  def delay_cache_post_count_on_badges; self.delay_for(15.seconds).cache_post_count_on_badges; end
  def cache_post_count_on_badges; badges.each {|b| b.update_attribute(:posts_count, b.posts.active.size)}; end


protected

  def has_a_post_item
    errors.add(:post_items, "cannot be blank") unless post_items.size > 0
  end

  def generate_slug
    self.slug = title.parameterize[0,48].gsub(/(\-)+$/, '') if title && slug.blank?
    self.slug = self.slug.gsub(/(\-)+$/, '')[0,48]
  end

  def decrease_cache_count_on_tags
    tags.each {|t| t.update_attribute(:posts_count, t.posts.active.size - 1)}
  end

  def decrease_cache_count_on_badges
    badges.each {|b| b.update_attribute(:posts_count, b.posts.active.size - 1)}
  end

  def after_publish
    update(published_at: Time.now)
    delete_queue
  end

  def after_unpublish
    update(published_at: nil)
    delete_queue
  end

  def delete_queue
    self.post_queue.destroy! if self.post_queue
  end

  def image_from_url
    return if self.promo_image_url.blank?

    begin
      uri = Addressable::URI.parse(self.promo_image_url)
      Timeout::timeout(30) do # 30 seconds
        io = open(uri, read_timeout: 30, "User-Agent" => WT_USER_AGENT, allow_redirections: :all)
        io.class_eval { attr_accessor :original_filename }
        raise "invalid content-type" unless io.content_type.match(/^image\//i)
        io.original_filename = File.basename(uri.path || self.title.parameterize)
        io.original_filename << ".#{io.content_type.gsub(/^(image\/)(.*)$/i, '\2')}" unless io.original_filename.match(/\.(jpg|jpeg|png|tif|gif)$/) # for urls w/o file extension
        self.promo_image = io
      end
    rescue OpenURI::HTTPError => err
      self.errors.add(:promo_image, "unable to retrieve from URL [e:1]")
    rescue Timeout::Error => err
      self.errors.add(:promo_image, "unable to retrieve from URL [e:2]")
    rescue => err
      self.errors.add(:promo_image, "unable to retrieve from URL (#{err})")
    end
  end

end
