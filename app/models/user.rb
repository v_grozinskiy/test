class User < ActiveRecord::Base

  # VARIABLES -----------------------------------------------------------------

  # Re-ordering will alter user permissions
  ROLES = {
    # Default access
    user: 0,
    admin: 1,
    # muted: 2,
    # banned: 3,

    # Writing levels  (in order of seniority)
    super: 10,
    manager: 14,
    editor: 11,
    writer: 13,
    contributor: 12,
    intern: 16,
    # moderator: 15
  }

  LANGUAGES = {
     'en' =>    'English',
     # 'en-gb' => 'British English'
     # 'es' =>    'Español',
     # 'zh-cn' => '简体中文'
   }


  # EXTENSIONS ----------------------------------------------------------------

  # Authlogic
  acts_as_authentic do |c|
    c.transition_from_crypto_providers = Authlogic::CryptoProviders::Sha512
    c.crypto_provider = Authlogic::CryptoProviders::SCrypt
  end

  # FriendlyId
  extend FriendlyId
  friendly_id :login, use: [:slugged, :finders]

  # Paperclip
  has_attached_file :avatar, 
    styles: { large: ["500x500#",:jpg], medium: ["200x200#",:jpg], small: ["64x64#",:jpg] }, 
    processors: [:thumbnail, :compression],
    default_style: :medium, default_url: "/images/:class/:attachment/:style.png"


  # ASSOCIATIONS --------------------------------------------------------------

  has_many :post_authors, dependent: :destroy
  has_many :posts, through: :post_authors
  has_many :created_posts, foreign_key: :creator_id, class_name: 'Post'
  has_many :activities, class_name: 'UserActivity'


  # CALLBACKS -----------------------------------------------------------------

  before_save :update_name


  # VALIDATIONS ----------------------------------------------------------------

  validates :first_name, :last_name, :login, presence: true
  validates :first_name, :last_name, :login, length: {maximum: 250}

  validates_attachment :avatar, 
    content_type: { content_type: ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'] },
    size: { less_than: 10.megabytes }


  # SCOPES --------------------------------------------------------------------

  scope :with_roles, Proc.new{|*roles| where( (roles.flatten.map(&:to_sym) & ROLES.keys).map{|role| "roles_mask & #{User.bitmask_for_roles(role)} <> 0"}.join(' AND ') ) }
  scope :with_any_roles, Proc.new{|*roles| where("roles_mask & ? <> 0", User.bitmask_for_roles(roles)) }
  scope :authors, -> { with_any_roles(User.admin_roles) }
  scope :admins, -> { with_roles(:admin).with_any_roles(User.admin_roles) }
  scope :community, -> { with_roles(:user) }
  scope :search, Proc.new{|q| s = "%#{q}%"; where("LOWER(name) LIKE ? OR LOWER(login) LIKE ? OR LOWER(email) LIKE ?", s, s, s) }


  # CLASS METHODS -------------------------------------------------------------

  def self.is_valid_language(l); User::LANGUAGES.keys.map{|v| v.to_s.downcase}.include?(l.to_s.downcase) rescue false; end

  # Roles assertions
  def self.access_roles; ROLES.select{|r,i| i < 10}.keys; end
  def self.admin_roles; ROLES.select{|r,i| i > 9 && i < 100}.keys; end
  def self.bitmask_for_roles(*roles); (roles.flatten.map(&:to_sym) & ROLES.keys).map {|r| 2**ROLES[r.to_sym]}.inject(0, :+); end


  # METHODS -------------------------------------------------------------------

  # User role
  def is?(*roles); (roles.flatten.map(&:to_sym) & ROLES.keys).select{|r| self.roles.include?(r)}.size > 0; end
  def admin?; is?(:admin); end
  def editor?; is?(:super,:manager,:editor); end
  def author?; is?(User.admin_roles); end
  def roles=(roles); self.roles_mask = User.bitmask_for_roles(roles); end
  def roles; ROLES.reject{|r,i| ((self.roles_mask.to_i || 0) & 2**i).zero? }.keys; end

  def deliver_password_reset_instructions!
    reset_perishable_token!
    UserMailer.password_reset_instructions(self).deliver
  end


private

  def update_name
    self.name = [self.first_name, self.last_name].compact.join(' ')
  end

end
