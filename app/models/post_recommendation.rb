class PostRecommendation < ActiveRecord::Base

  belongs_to :post
  belongs_to :recommended_post, class_name: 'Post'
  validates_uniqueness_of :recommended_post_id, scope: :post_id
  validates_presence_of :post_id, :recommended_post_id

end
