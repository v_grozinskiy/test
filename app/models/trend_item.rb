class TrendItem < ActiveRecord::Base

  require 'addressable/uri'

  has_many :list_items, class_name: 'TrendListItem'
  belongs_to :post

  has_attached_file :image, 
    styles: { sidebar: ['300x105#',:jpg], feed: ['470x205#',:jpg] },
    processors: [:thumbnail, :compression],
    default_style: :feed, default_url: "/images/:class/:attachment/:style.png"

  before_validation :ensure_source_url_schema
  before_validation :image_from_url

  validates :keyword, presence: true
  validates_attachment :image, 
    content_type: { content_type: ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'] },
    size: { less_than: 10.megabytes }

  scope :latest, -> { order('created_at desc') }
  scope :recently, Proc.new{|t| where('created_at > ?', Time.now - (t || 1.day)) }


  def name
    !self.display_keyword.blank? ? self.display_keyword : self.keyword
  end

  def nice_source_url
    Addressable::URI.parse(self.source_url).host
    # u, uri = '', Addressable::URI.parse(self.source_url) rescue nil
    # if uri.present?
    #   u << uri.host << uri.path
    #   u << '?' << uri.query if uri.query.present?
    # end
    # u
  end

  def nice_via_url
    Addressable::URI.parse(self.via_url).host
    # u, uri = '', Addressable::URI.parse(self.via_url) rescue nil
    # if uri.present?
    #   u << uri.host << uri.path
    #   u << '?' << uri.query if uri.query.present?
    # end
    # u
  end


protected

  def ensure_source_url_schema
    self.source_url[0,0] = 'http:' if (self.source_url || '').match(/^\/\//)
  end

  def image_from_url
    return if self.image_url.blank?
    return if self.image_file_name.present? && !self.changed.include?(:image_url)

    begin
      uri = Addressable::URI.parse(image_url)
      Timeout::timeout(30) do # 30 seconds
        io = open(uri, read_timeout: 30, "User-Agent" => WT_USER_AGENT, allow_redirections: :all)
        io.class_eval { attr_accessor :original_filename }
        raise "invalid content-type" unless io.content_type.match(/^image\//i)
        io.original_filename = File.basename(uri.path || self.title.parameterize)
        io.original_filename << ".#{io.content_type.gsub(/^(image\/)(.*)$/i, '\2')}" unless io.original_filename.match(/\.(jpg|jpeg|png|tif|gif)$/) # for urls w/o file extension
        self.image = io
      end
    rescue OpenURI::HTTPError => err
      self.errors.add(:image, "unable to retrieve from URL [e:1]")
    rescue Timeout::Error => err
      self.errors.add(:image, "unable to retrieve from URL [e:2]")
    rescue => err
      self.errors.add(:image, "unable to retrieve from URL (#{err})")
    end
  end

end
