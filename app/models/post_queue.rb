class PostQueue < ActiveRecord::Base

  require 'sidekiq/api'

  belongs_to :post

  validates :post, :publish_at, presence: true
  validate :publish_at_cannot_be_in_the_past

  before_save :schedule_job
  before_destroy :delete_job


  def publish_at_cannot_be_in_the_past
    if self.publish_at < Time.now
      errors.add(:publish_at, "can't be in the past")
    end
  end

  private

  def schedule_job
    delete_job unless self.jid.blank?

    if self.jid = PostQueueWorker.perform_at(self.publish_at, post_id)
      post.queue unless post.queued?
    else
      errors.add(:base, 'error occured while queuing. Please try later.')
    end
  end

  def delete_job
    if Rails.env.test?
      queue = PostQueueWorker.jobs
      queue.delete_if { |x| x['jid'] == self.jid }
    else
      queue = Sidekiq::ScheduledSet.new
      queue.each { |job| job.delete if job.jid == self.jid }
    end
  end
end
