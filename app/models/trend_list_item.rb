class TrendListItem < ActiveRecord::Base

  belongs_to :item, class_name: 'TrendItem', foreign_key: :trend_item_id
  belongs_to :list, class_name: 'TrendList', foreign_key: :trend_list_id

  scope :active, -> { where(active: true) }


  def nice_score
    (self.score * 10).round(2)
  end


end
