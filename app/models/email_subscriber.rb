class EmailSubscriber < ActiveRecord::Base

  validates :email, presence: true, email: true

  after_create :subscribe_to_mailchimp

  private

  def subscribe_to_mailchimp
    return false unless Rails.configuration.mailchimp

    response = Rails.configuration.mailchimp.lists.subscribe({
      id: ENV['MAILCHIMP_LIST_ID'],
      email: { email: email },
      double_optin: false,
    })

    if response['status'] == 'error'
      self.errors.add(:base, response['error'])
      return false
    end
    
    return true
  end
end
