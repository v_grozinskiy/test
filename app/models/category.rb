class Category < ActiveRecord::Base

  # VARIABLES -----------------------------------------------------------------


  # EXTENSIONS ----------------------------------------------------------------

  # Awesome Nested Set
  acts_as_nested_set

  # FriendlyId
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  # Paperclip
  has_attached_file :banner_image, styles: { large: ["128x128#",:jpg], medium: ["64x64#",:jpg] }, default_style: :medium, default_url: "/images/:class/:attachment/:style.png"


  # ASSOCIATIONS --------------------------------------------------------------

  has_many :posts


  # CALLBACKS -----------------------------------------------------------------

  after_save :delete_nested_categories_set


  # VALIDATIONS ----------------------------------------------------------------

  validates :name, presence: true, length: {maximum: 250}, uniqueness: true
  validates :description, length: {maximum: 1500}, allow_blank: true, allow_nil: true
  validates_attachment :banner_image, content_type: { content_type: ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'] }, size: { less_than: 10.megabytes }


  # SCOPES --------------------------------------------------------------------

  scope :sfw, -> { where(nsfw: false) }
  scope :active, -> { where(active: true) }


  # CLASS METHODS -------------------------------------------------------------

  def self.delete_categories_cache(ids=[])
    Category.where(id: ids).find_each { |c| ActionController::Base.new.expire_fragment(c) }
    true
  end


  # METHODS -------------------------------------------------------------------


  def mapped_name(glue=' > ')
    self.self_and_ancestors.map(&:name).join(glue)
  end

  def level_name(mark='-')
    "#{mark * level} #{name}"
  end

  def delete_nested_categories_set
    Rails.cache.delete(['categories','nested_set_options'])
  end

end
