class TrendList < ActiveRecord::Base


  has_many :list_items, -> { order('rank asc, avg_score desc, score desc, trend_list_items.id asc') }, dependent: :destroy, class_name: 'TrendListItem'
  # has_many :items, through: :list_items, class_name: 'TrendItem'
  # has_many :posts, through: :items


  state_machine initial: :loading do
    before_transition on: :publish, do: :publish_posts

    event :loaded do
      transition loading: :draft
    end

    event :publish do
      transition draft: :published
    end

    event :unpublish do
      transition published: :draft
    end
  end

  scope :published, -> { where(state: :published) }
  scope :draft, -> { where.not(state: :published) }
  scope :recent, -> { order(created_at: :desc) }


  def self.latest
    self.recent.first
  end

  def self.regenerate_trend_cache
    Rails.cache.delete_matched('*trend*')
    TrendWorker.new.trend_list # Invoke directly
    Post.delete_homepage_cache
  end



protected

  def publish_posts
    # so hacky, redo later:
    n = 0
    list_items.active.limit(100).each do |item|
      n+=1
      if item.item && item.item.post.present?
        if item.item.post.present? && !item.item.post.published?
          unless item.item.post.publish
            item.update_attribute(:active, false)
            n -= 1
          end
        end
      end
      break if n >= 10
    end
  end

end
