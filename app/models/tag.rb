class Tag < ActiveRecord::Base

  # VARIABLES -----------------------------------------------------------------


  # EXTENSIONS ----------------------------------------------------------------

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]


  # ASSOCIATIONS --------------------------------------------------------------

  has_and_belongs_to_many :posts, join_table: 'post_tags'


  # CALLBACKS -----------------------------------------------------------------

  after_save :clear_tags_cache


  # VALIDATIONS ----------------------------------------------------------------

  validates :name, presence: true
  validates_uniqueness_of :name
  validates :name, length: {maximum: 250}


  # SCOPES --------------------------------------------------------------------


  # CLASS METHODS -------------------------------------------------------------

  def self.find_or_create_all_with_like_by_name(list)
    return [] if list.empty?

    tags = Tag.named_any(list)

    list.map do |tag_name|
      existing_tag = tags.detect { |tag| tag.name == tag_name }
      tags << Tag.create(name: tag_name) unless existing_tag
    end

    return tags
  end


  # METHODS -------------------------------------------------------------------

  def as_json(options={})
    super(options).merge(text: self.name )
  end

  def self.named_any(list)
    clause = list.map { |tag|
      sanitize_sql(["lower(name) = ?", tag])
    }.join(" OR ")
    where(clause)
  end
  def clear_tags_cache
    Rails.cache.delete(['admin','tags_list'])
  end


private


end
