class PostItem < ActiveRecord::Base

  require 'addressable/uri'

  # VARIABLES -----------------------------------------------------------------

  enum style: {
    text_item:    0,
    image_item:   1,
    video_item:   2,
    tweet_item:   3,
    embed_item:   4,
  }

  # EXTENSIONS ----------------------------------------------------------------

  # FriendlyId
  extend FriendlyId
  friendly_id :slug, use: [:finders]

  # Paperclip
  has_attached_file :image, 
    styles: Proc.new{ |a| a.instance.image_set_dimensions }, 
    processors: [:cropper, :thumbnail, :compression],
    default_style: :medium, default_url: "/images/:class/:attachment/:style.png"

  # Impressionist
  is_impressionable
  has_many :impression_rollups, as: :impressionable


  # ASSOCIATIONS --------------------------------------------------------------

  belongs_to :post, touch: true


  # CALLBACKS -----------------------------------------------------------------

  before_create :generate_slug
  before_validation :image_from_url
  after_post_process :extract_dimensions
  before_save :sanitize
  before_post_process :image_flag_if_animated


  # VALIDATIONS ----------------------------------------------------------------

  attr_accessor :image_url, :crop_x, :crop_y, :crop_w, :crop_h

  validates :source, :via, length: {maximum: 250}, allow_blank: true, allow_nil: true
  validates :media, length: {maximum: 5000}
  validates :text, length: {maximum: 999999}
  validates :image_url, url: true, allow_blank: true, allow_nil: true
  validates_attachment :image, content_type: { content_type: ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'] }, size: { less_than: 10.megabytes }
  validates :text, presence: true, if: Proc.new{|v| %w(text_item).include?(v.style.to_s)}
  validates :media, presence: true, if: Proc.new{|v| %w(video_item embed_item tweet_item).include?(v.style.to_s)}
  validates_attachment :image, presence: true, if: Proc.new{|v| %w(image_item).include?(v.style.to_s)}


  # SCOPES --------------------------------------------------------------------

  scope :has_text_content, -> { where("text is not null and text != ''") }
  default_scope -> { order("position ASC") }


  # CLASS METHODS -------------------------------------------------------------


  # METHODS -------------------------------------------------------------------

  # Generate sitemap.xml formatted output
  def to_sitemap
    obj = {}

    case self.style
      when 'image_item'
        obj = {loc: self.image.url(:large), title: self.title, caption: self.text}
      when 'video_item'
        obj = {title: self.title, description: self.text, thumbnail_loc: self.image.url(:large), duration: nil, family_friendly: !self.nsfw, publication_date: self.created_at}

        if (self.media || '').match(/^(http(s)?\:\/\/)(m\.|www\.)?(youtube\.com|youtu\.be|vimeo\.com)\//i)
          obj[:player_loc] = self.media
        else
          obj[:content_loc] = self.media
        end
    end

    obj
  end

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end

  def image_set_dimensions
    styles = { large: ["900x", :jpg], medium: ["440x330", :jpg], small: ["220x165", :jpg] }
    
    if (self.image_content_type || '').match(/image\/gif/i) # Animated GIF support
      styles = styles.merge({large_gif: ["900x", :gif]})
    end

    styles
  end

  def platform?(p)
    case p.to_s
      when 'instagram';   self.media.match(/instagram\.com/i);
      else;               false
    end
  rescue
    false
  end


protected

  def generate_slug
    self.slug = SecureRandom.hex(8)[0,6]
  end


private

  def image_from_url
    return if image_url.blank?

    begin
      uri = Addressable::URI.parse(image_url)
      Timeout::timeout(30) do # 30 seconds
        io = open(uri, read_timeout: 30, "User-Agent" => WT_USER_AGENT, allow_redirections: :all)
        io.class_eval { attr_accessor :original_filename }
        raise "invalid content-type" unless io.content_type.match(/^image\//i)
        io.original_filename = File.basename(uri.path || self.title.parameterize)
        io.original_filename << ".#{io.content_type.gsub(/^(image\/)(.*)$/i, '\2')}" unless io.original_filename.match(/\.(jpg|jpeg|png|tif|gif)$/) # for urls w/o file extension
        self.image = io
      end
    rescue OpenURI::HTTPError => err
      self.errors.add(:image, "unable to retrieve from URL #{image_url} [e:1]")
    rescue Timeout::Error => err
      self.errors.add(:image, "unable to retrieve from URL #{image_url} [e:2]")
    rescue => err
      self.errors.add(:image, "unable to retrieve from URL #{image_url} (#{err})")
    end
  end

  def extract_dimensions
    return unless image
    geo = Paperclip::Geometry.from_file(image.queued_for_write[:original])
    self.image_width = geo.width
    self.image_height = geo.height
  end

  def sanitize
    self.title = sanitize_clean(title) if title
    self.media = sanitize_clean(media) if media
    self.text = sanitize_clean(text) if text
  end

  def sanitize_clean(html)
    Sanitize.fragment(html, Sanitize::Config.merge(Sanitize::Config::BASIC,
      elements: %w[p span a b strong em i s ins blockquote code q img iframe object embed param script],
      attributes: { 
        all: %w[href src class name value target title style],
        'iframe' => %w[frameborder framespacing width height webkitallowfullscreen mozallowfullscreen allowfullscreen],
        'script' => %w[async]
      },
      remove_contents: true
    ))
  end

  # Flag if file is animated GIF
  def image_flag_if_animated
    if (self.image_content_type.match(/^image/i) rescue false)
      path = self.image.queued_for_write[:original].path
      frames = Paperclip.run("identify #{path} | fgrep .gif[1]") rescue nil
      self.image_animated = true unless frames.blank?
    end
  end

end
