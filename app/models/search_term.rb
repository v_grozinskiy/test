class SearchTerm < ActiveRecord::Base

  extend FriendlyId
  friendly_id :term, use: [:slugged, :finders]

  is_impressionable counter_cache: true
  has_many :impression_rollups, as: :impressionable

  validates :term, presence: true
  validates :term, uniqueness: true
  validates :term, length: {maximum: 250}

  scope :recent, -> { order(created_at: :desc) }
end
