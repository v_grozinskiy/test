class SharingMail < ActiveRecord::Base
  belongs_to :target, polymorphic: true

  validates :sender_email, :receiver_email, presence: true, length: {maximum: 250}
  validates :message, length: {maximum: 250}


end
