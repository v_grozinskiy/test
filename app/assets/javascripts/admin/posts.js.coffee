window.app = {
  updateItemPosition: ->
    pos_info = {}
    post_id = $('ul#post-items').data('post-id')

    if $('ul#post-items li.post-item').length
      $('ul#post-items').children('li.post-item').each (i, item) ->
        $(item).find('input[type="hidden"]').first().val i + 1
        pos_info[i] = { 'position': i + 1, id: $(item).data('post-item-id') }

      unless post_id == undefined
        $('ul#post-items').parents('form').data('form-changed', true)
        $.ajax
          url: '/admin/posts/' + post_id
          type: "PUT"
          dataType: 'json'
          data: { post: { "post_items_attributes": pos_info }, commit: "Arrange" }
}

position = 0

authorFormatResult = (author) ->
  tmp_container = $('<div></div>')
  tmp_container.append $('<img/>').attr('src', author.avatar)
  tmp_container.append author.name
  tmp_container.html()

authorFormatSelection = (post_author, container) ->
  relation_id = (post_author.relation_id || new Date().getTime())
  id_tag = $('<input value=\'' + (post_author.relation_id || '') + '\'/>').
    attr('name', 'post[post_authors_attributes][' + relation_id + '][id]').hide()

  position_tag = $('<input class=\'author_position\' value=\'' + position + '\'/>').
    attr('name', 'post[post_authors_attributes][' + relation_id + '][position]').hide()

  user_id_tag = $('<input value=\'' + post_author.id + '\'/>').
    attr('name', 'post[post_authors_attributes][' + relation_id + '][user_id]').hide()

  remove_tag = $('<input value=\'0\'/>').
    attr('name', 'post[post_authors_attributes][' + relation_id + '][_destroy]').
    attr('id', 'post_authors_remove_' + post_author.id).hide()
  $('form.post-form').append(remove_tag).append(id_tag)

  position += 1

  return post_author.name + $('<div/>').
    append(position_tag).append(user_id_tag).html()

resetAuthorPosition = (el) ->
  el.children('li.select2-search-choice').each (i, _) ->
    $(this).find('.author_position').val(i)

postFormatSelection = (post, container) ->
  '<img src="' + post.image + '" class="post-image">
  <div class="post-title">' + post.text + '</div>
  <input value="' + post.id + '" name="post[recommended_post_ids][]" style="display:none" id="' + 'recommended_post_' + post.id + '">'

getMaxDate = ->
  now = new Date()
  date = now.getDate()
  if now.getMonth() == 11
    current = new Date(now.getFullYear() + 1, 0, date)
  else
    current = new Date(now.getFullYear(), now.getMonth() + 1, date)

getMinDate = ->
  now = new Date()
  now.setDate(now.getDate() - 1)
  now

$ ->
  new PostItem($('ul#post-items'))

  $('#datetimepicker1').datetimepicker
    minuteStepping: 5
    minDate: getMinDate()
    maxDate: getMaxDate()
    sideBySide: true
    useSeconds: false

  $('#publish-modal-default').on('shown.bs.modal', ->
    $('#datetimepicker1').data("DateTimePicker").show();
  )

  $('.preview-btn').click ->
    f = $(this).parents('form')
    oa = f.attr('action')
    ot = f.attr('target') || ''
    f.attr('target', '_post_preview_'+ f.data('post-id'))
    f.attr('action', $(this).data('preview-path'))

    setTimeout( ->
      f.attr('target', ot || '').attr('action', oa)
    , 500)

    f.get(0).submit()
    return false

  # Do not show post in Google News if NSFW. Therefore, disable checkbox for Google News if marked NSFW.
  $('#post_nsfw').on 'change', ->
    if $(this).prop('checked')
      $('#google_news_wrap').attr 'disabled', true
    else
      $('#google_news_wrap').removeAttr 'disabled'
      

  $(".post-form .display-format").click ->
    $(this).parents(".radio_buttons").find(".radio").removeClass "active"
    $(this).parent().addClass "active"

  if $('#author_ids').length
    $("#author_ids").select2
      cacheDataSource: []
      placeholder: WT.Languages.default().post.search_placeholder
      minimumInputLength: 2
      multiple: true
      query: (query)->
        self = this
        term = query.term
        if self.cacheDataSource[term]
          query.callback {results: self.cacheDataSource[term]} 
          return

        $.getJSON "/admin/authors_search", { q: term }, 
          (data, status, xhr) ->
            self.cacheDataSource[term] = data
            query.callback {results: data}

      initSelection: (element, callback) ->
        id = $(".post-form").data("post-id")
        if id isnt undefined
          $.getJSON "/admin/posts/" + id + "/fetch_authors", {},
            (data, status, xhr) ->
              callback data
        else
          callback window.user

      formatResult: authorFormatResult
      formatSelection: authorFormatSelection 
      dropdownCssClass: "bigdrop"
      escapeMarkup: (m) -> 
        m

    $("#recommended_post_ids").select2
      cacheDataSource: []
      placeholder: WT.Languages.default().trend_item.search_placeholder
      minimumInputLength: 2
      multiple: true
      query: (query)->
        self = this
        term = query.term
        if self.cacheDataSource[term]
          query.callback {results: self.cacheDataSource[term]} 
          return

        $.getJSON "/admin/posts", { q: term }, 
          (data, status, xhr) ->
            self.cacheDataSource[term] = data
            query.callback {results: data}

      initSelection: (element, callback) ->
        callback recommended_posts

      formatSelection: postFormatSelection 
      dropdownCssClass: "bigdrop"
      escapeMarkup: (m) -> 
        m        

    $("#author_ids").on 'select2-removed', (e) ->
      position -= 1
      resetAuthorPosition $(e.target).prev().find('ul.select2-choices')
      $('#post_authors_remove_' + e.val).val(1)

    $("#author_ids").select2("container").find("ul.select2-choices").sortable
      containment: 'parent'
      start: ->
        $("#author_ids").select2("onSortStart")
      update: (x)->
        resetAuthorPosition $(x.target)
        $("#author_ids").select2("onSortEnd")

  if $('#post_tag_list').length
    $("#post_tag_list").select2
      tags: tags
      tokenSeparators: [',']
      minimumInputLength: 2,

      createSearchChoice : (term)->
        return {
          id: $.trim(term)
          text: $.trim(term)
        }
      tokenizer: (input, selection, selectCallback, opts)->
        original = input
        dupe = false
        token = undefined
        index = undefined
        i = undefined
        l = undefined
        separator = undefined

        if !opts.createSearchChoice || !opts.tokenSeparators || opts.tokenSeparators.length < 1
          return undefined

        while (true)
          index = -1
          for separator in opts.tokenSeparators
            index = input.indexOf(separator)
            if index >= 0
              break
          if index < 0
            break

          token = input.substring(0, index)
          input = input.substring(index + separator.length)

          if token.length > 0
            token = opts.createSearchChoice.call(this, token, selection);
            if token != undefined && token != null && opts.id(token) != undefined && opts.id(token) != null
              dupe = false
              for i in selection.length
                if equal(opts.id(token), opts.id(i))
                  dupe = true
                  break
              unless dupe
                selectCallback(token)

        if original != input
          return input

      # Based on http://jsfiddle.net/uLqc9/
      initSelection: (element, callback)->
        tags = element.val().split(', ')
        selection = this.tags.filter (metric)->
          tags.indexOf(metric.text) != -1
        callback(selection)
      query: (options)->
        pageSize = 5
        startIndex  = (options.page - 1) * pageSize
        filteredData = this.tags
        stripDiacritics = window.Select2.util.stripDiacritics

        if options.term && options.term.length > 0
          if !options.context
            term = stripDiacritics( options.term.toLowerCase() )
            options.context = this.tags.filter (metric,i)->
              if !metric.stripped_text
                metric.stripped_text = stripDiacritics( metric.text.toLowerCase() )
              return (metric.stripped_text.indexOf(term) != -1)
            filteredData = options.context

          options.callback {
            context: filteredData
            results: filteredData.slice(startIndex, startIndex + pageSize)
            more: (startIndex + pageSize) < filteredData.length
          }


  if $('ul#post-items').length
    $('ul#post-items.sortable').sortable
      axis: "y"
      placeholder: "sortable-placeholder"
      stop: app.updateItemPosition
      items: ' > li.can-sort'

    $('a[href="#publish-modal-default"]').click (e) ->
      e.preventDefault()

      f = $(this).parents('form')
      changed = f.data('form-changed') || false
      f.find('input[name], textarea[name]').each(->
        changed = true if $(this).val() != $(this).prop('defaultValue') && !$(this).prop('name').match(/(author_id|tag_list)/i)
      ) unless changed
      
      if changed
        alert WT.Languages.default().post.save_before_queue
        return false
      else
        return $('.post-form').parsley('validate')
