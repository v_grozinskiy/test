$ ->

  $('.add-setting').click (e) ->
    $('.setting_key input').val('').removeAttr('readonly');
    $('.setting_value input').val ''


  if $('.admin-setting').length
    $('.edit-setting').click (e) ->
      e.preventDefault()

      key = $(this).parents('tr').data('key')
      t = $(this).parents('tr').data('key-type')
      $('.setting_key input').val(key).attr('readonly','readonly')

      if (t == 'boolean')
        $('.setting_value input:eq(0)').prop('checked', window.wt_settings[key])
        $('#setting-modal-boolean').modal()
      else
        $('.setting_value input').val window.wt_settings[key]
        $('#setting-modal-default').modal()
