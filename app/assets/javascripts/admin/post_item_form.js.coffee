img_w = 400
img_h = 400
boundx = 400
boundy = 400
crop_x = 0
crop_y = 0
crop_w = 400
crop_h = 400
ratio = 1
jcrop_api = undefined
itemFormChanged = false

markItUpSettings = {
  onShiftEnter:   {keepDefault:false, replaceWith:'<br />\n'}
  onCtrlEnter:    {keepDefault:false, openWith:'\n<p>', closeWith:'</p>'}
  onTab:          {keepDefault:false, replaceWith:'    '}
  markupSet:  [   
    {name:'Bold', key:'B', openWith:'(!(<strong>|!|<b>)!)', closeWith:'(!(</strong>|!|</b>)!)' },
    {name:'Italic', key:'I', openWith:'(!(<em>|!|<i>)!)', closeWith:'(!(</em>|!|</i>)!)'  },
    {name:'Stroke through', key:'S', openWith:'<del>', closeWith:'</del>' },
    {separator:'---------------' },
    {name:'Link', key:'L', openWith:'<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!) rel="nofollow" target="_blank">', closeWith:'</a>', placeHolder:'Your text to link...' },
    {separator:'---------------' },
    {name:'Preview', className:'preview',  call:'preview'}
  ]
}

showCropModal = ->
  $('#cropping-modal').modal()
  window.setTimeout (->
    if img_w > 0
      $('.jcrop_target').Jcrop
        onChange: updatePreview
        onSelect: updatePreview
        minSize: [400 * ratio, 100 * ratio]
        setSelect: [0, 0, boundx, boundy]
        #aspectRatio: 4/3
      , ->
        jcrop_api = this
        return
    else
      alert WT.Languages.default().image_load_error
  ), 500

updatePreview = (coords) ->
  boundy = img_h * ratio
  xrate = boundx / coords.w
  yrate = boundy / coords.h
  coords_scale = coords.w / coords.h
  bound_scale = boundx / boundy;
  scale = if coords_scale >= bound_scale
    xrate
  else yrate

  $('img.preview').css
    width: Math.round(scale * boundx) + 'px'
    height: Math.round(scale * boundy) + 'px'
    marginLeft: '-' + Math.round(scale * coords.x) + 'px'
    marginTop: '-' + Math.round(scale * coords.y) + 'px'

  if coords_scale >= bound_scale
    preview_w = boundx
    preview_h = Math.round(coords.h * boundx / coords.w)
  else
    preview_w = Math.round(coords.w * boundy / coords.h)
    preview_h = boundy

  $('.crop-preview-wrapper').css
    width: preview_w + 'px'
    height: preview_h + 'px'

  crop_x = Math.round(coords.x / ratio)
  crop_y = Math.round(coords.y / ratio)
  crop_w = Math.round(coords.w / ratio)
  crop_h = Math.round(coords.h / ratio)

  $('#crop_x').val(crop_x)
  $('#crop_y').val(crop_y)
  $('#crop_w').val(crop_w)
  $('#crop_h').val(crop_h)

window.imageOnLoad = (val) ->
  img = new Image()
  img.onload = ->
    img_w = @width
    img_h = @height
    ratio = boundx / img_w
    crop_w = img_w
    crop_h = img_h
    boundy = img_h * ratio
    $('img.jcrop_target').css height: Math.round(boundy)
    showCropModal()

  img.src = val
  $('img.jcrop_target').attr 'src', img.src
  $('img.preview').attr 'src', img.src



class @PostItemForm
  constructor: (el) ->
    if el instanceof jQuery
      @el = el
    else
      @el = $(el)

    @itemId = @el.data('post-item-id')
    if @itemId != undefined
      prev = $('li.post-item[data-post-item-id="' + @itemId + '"]')
      prev.hide()
      WT.Media.isValid = true
    else
      prev = $('.media-types')
      @setPostition()

    @el.insertAfter(prev).fadeIn('slow')

    $('.post-item-text').markItUp markItUpSettings

    $('.post-form').parsley('destroy').parsley
      listeners:
        onFieldValidate: (elem) ->
          elem.parents('.has-error').removeClass('has-error')
          if (elem.hasClass('post-item-image-url') or elem.hasClass('post-item-media')) and !elem.is(':visible')
            return true
          return false

        onFieldError: (elem, constraints, ParsleyField) ->
          parsleyField = ParsleyField
          parsleyForm = '.post-form'
          ParsleyField.$element.parent().addClass 'has-error'

          unless $(parsleyForm).find('ul.alert').length
            $(parsleyForm).find('.post-form-inner').prepend('<ul class="alert alert-danger list-unstyled"><li>' + WT.Languages.default().form_error + '</li></ul>')
          else  
            $(parsleyForm).find('ul.alert').html '<li>' + WT.Languages.default().form_error + '</li>'

      validators:
        media: ->
          WT.Media.isValid

      messages:
        media: WT.Languages.default().invalid

    @el.find('.toggle-upload').click @toggleSource
    @el.find('.post-item-image').change @onFileAttach
    @el.find('.post-item-image-url').blur @onChangeUrl
    @el.find('.post-item-media').change @onChangeMedia
    @el.find('input, textarea').change @onChangeInput

  setPostition: () ->
    @el.find('input.post-item-position').val $('li.post-item').length + 1

  toggleSource: (e) ->
    e.preventDefault()
    $('.upload-format').toggle()

  onFileAttach: (e) ->
    reader = new FileReader
    reader.onload = ->
      imageOnLoad reader.result

    reader.readAsDataURL @files[0]

  onChangeUrl: (e) ->
    $this = $(this)
    val = $this.val().trim()
    unless val is ''
      imageOnLoad val

  onChangeMedia: (e) ->
    WT.Media.parseMedia $(this).val()

  onChangeInput: (e) =>
    itemFormChanged = true if @itemId


$ ->

  $('#cropping-modal button[data-dismiss="modal"]').click (e) ->
    $('.crop-input').val '' unless $(e.currentTarget).hasClass('modal-submit')

  $('#cropping-modal').on 'hidden.bs.modal', ->
    if jcrop_api
      img_w = null
      img_h = null
      jcrop_api.destroy()

  $('#cropping-modal .modal-submit').click ->
    if $('.upload-from-file').is(':visible')
      $('.post-item-source').val ''
      $('.post-item-image-url').val ''
    else
      $('.post-item-source').val $('.post-item-image-url').val()

    $('.item-media img').attr 'src', $('img.preview').attr('src')
    prev_x = $('.preview-wrapper').width()
    rate = img_w / prev_x
    scale = img_w / crop_w
    $('.preview-wrapper img').css
      width: Math.round(scale * prev_x) + 'px'
      marginLeft: '-' + Math.round(scale * crop_x / rate) + 'px'
      marginTop: '-' + Math.round(scale * crop_y / rate) + 'px'

    $('.preview-wrapper').css height: Math.round(prev_x * crop_h / crop_w) + 'px'


  $(document).ready ->
    if $('#post-items').length
      $('.add_fields').click (e) ->
        if itemFormChanged
          alert WT.Languages.default().unsaved_msg
          e.preventDefault()
          return false

      # Trigger PostItemForm on submit error
      if $('.nested-fields.form-error').size() > 0
        new PostItemForm( $('.nested-fields.form-error') )

      $('#post-items').on('cocoon:before-insert', (e, itemToBeAdded) ->
        $('.nested-fields').remove()
        $('li.post-item').show()
      ).on('cocoon:after-insert', (e, addedItem) ->
        new PostItemForm(addedItem)
        $($.scrollWin).scrollTop( $('.nested-fields').offset().top - $('header.navbar').outerHeight() - 10)
      ).on('cocoon:before-remove', (e, itemToBeDeleted) ->
        itemFormChanged = false
        itemToBeDeleted.fadeOut('slow').remove()
      ).on 'cocoon:after-remove', ->
        $('li.post-item').show()
