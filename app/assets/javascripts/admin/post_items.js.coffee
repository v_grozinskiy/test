class @PostItem
  constructor: (container) ->
    if container instanceof jQuery
      @container = container
    else
      @container = $(container)

    @container.find('a.item-position-up').click @moveItemUp
    @container.find('a.item-position-down').click @moveItemDown

  moveItemUp: (e) ->
    item = $(e.target).parents('li.post-item')
    destination = item.prev()
    if destination.length
      item.insertBefore destination
      app.updateItemPosition()

  moveItemDown: (e) ->
    item = $(e.target).parents('li.post-item')
    destination = item.next()
    if destination.length
      item.insertAfter destination
      app.updateItemPosition()