window.currentObject = undefined
window.originalObject = undefined

onRemove = (e) ->
  el = $(e.currentTarget).parents('li')

  while el.length
    next = el.next()
    unless next.length
      el.data('id', '')
      el.find('.list-group-item').html ''
      break
    el.html next.html()
    el.data('id', next.data('id'))
    el.find('button.remove-post').on 'click', onRemove
    el = el.next()

arrangePosition = ->
  i = 0
  $('ul#recommended-posts li').each ->
    if i > 4
      $(this).remove()
    else
      $(this).data('position', i)
    i += 1

class @LayoutItem
  constructor: (el) ->
    if el instanceof jQuery
      @el = el
    else
      @el = $(el)

    @moveFoward() unless @el.data('id') is ''
    arrangePosition()

    @el.on 'drag', @onDrag
    @el.on 'dragover', @onDragover
    @el.on 'drop', @onDrop
    @el.find('button.remove-post').on 'click', onRemove

  moveFoward: ->
    prev = @el.prev()
    while prev.data('id') == ''
      @el.insertBefore prev
      prev = @el.prev()

  onDrag: (e) ->
    e.preventDefault()
    window.originalObject = $(e.currentTarget)
    window.currentObject = $(e.currentTarget).clone()

  onDragover: (e) ->
    e.preventDefault()
    $(e.currentTarget).find('.list-group-item').addClass 'active'

  onDrop: (e) ->
    e.preventDefault()
    currentTarget = $(e.currentTarget)

    if window.currentObject && window.currentObject.data('id') isnt ''
      if window.originalObject.parents('#recommended-posts').length
        if $(e.currentTarget).data('id') isnt '' && parseInt(currentTarget.data('position')) > parseInt(window.originalObject.data('position'))
          window.originalObject.insertAfter $(e.currentTarget)
        else
          window.originalObject.insertBefore $(e.currentTarget)
      else
        window.originalObject.insertBefore $(e.currentTarget)
        new LayoutItem(window.originalObject)

      arrangePosition()
      window.currentObject.remove()

    $('#recommended-posts .list-group-item').removeClass 'active'


$ ->

  if $('.admin-layout').length
    $('#recommended-posts li').each ->
      new LayoutItem($(this))

    $('#post-candidates li').on 'drag', (e) ->
      window.originalObject = $(e.currentTarget)
      window.currentObject = $(e.currentTarget).clone()

    $('#layout-form').submit ->
      $this = $(this)
      $('ul#recommended-posts li').each ->
        if $(this).data('id') isnt ''
          $this.append '<input type="hidden" name="post_ids[]" value="' +  $(this).data('id') + '">'
