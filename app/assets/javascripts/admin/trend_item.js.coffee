postFormatSelection = (post) ->
  $('#post_wrapper').show()
  $('#s2id_trend_item_post_id').hide()
  $('#post_wrapper img').attr('src', post.image)
  $('#post_wrapper h4.post-title span').html(post.text)
  $('#post_wrapper .post-title .label').toggle(post.state != 'published')
  return ('<span class="select2-chosen">' + post.text + '</span>')

$ ->

  if $('.edit_trend_item').length
    $("#trend_item_post_id").select2
      cacheDataSource: []
      placeholder: WT.Languages.default().trend_item.search_placeholder
      minimumInputLength: 2
      multiple: false
      dropdownCssClass: "bigdrop"

      query: (query)->
        self = this
        term = query.term
        if self.cacheDataSource[term]
          query.callback {results: self.cacheDataSource[term]} 
          return

        $.getJSON "/admin/posts", { q: term }, 
          (data, status, xhr) ->
            self.cacheDataSource[term] = data
            query.callback {results: data}

      initSelection: (element, callback) ->
        if post
          callback { id: post.id, text: post.title, image: post.image, state : post.state }

      formatSelection: postFormatSelection

      escapeMarkup: (m) -> 
        m

    $('#post_wrapper .close').click ->
      $('#post_wrapper').hide()
      $('#s2id_trend_item_post_id').show()
      $('#trend_item_post_id').val ''
      $('span.select2-chosen').html WT.Languages.default().trend_item.search_placeholder

