clearForm = (form) ->
  $('#post-mail-wrapper .error-expalnation').html ''
  $('#post-mail-wrapper .error-expalnation').hide()
  form.find('.has-error').removeClass 'has-error'

validForm = ->
  checkName($('#sharing_mail_sender_name')) && checkEmail($('#sharing_mail_sender_email')) && checkEmail($('#sharing_mail_receiver_email'))

checkName = (el) ->
  mailRegEx = /^[A-Z]{2,}/i
  val = el.val().trim()

  if val is ''
    ret = false
  else
    ret = mailRegEx.test(val)

  unless ret
    showError(el)
    showErrorExplanation(WT.Languages.default().invalid_name)

  return ret

checkEmail = (el) ->
  mailRegEx = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/
  val = el.val().trim()
  ret = true

  if val is ''
    ret = false
  else
    if el.attr('id') is 'sharing_mail_sender_email'
      ret = mailRegEx.test(val)
    else
      for email in val.split(',')
        email = email.trim()
        ret = ret && mailRegEx.test(email) unless email is ''

  unless ret
    showError(el)
    showErrorExplanation(WT.Languages.default().invalid_email)

  return ret

showError = (el) ->
  el.parent().addClass 'has-error'

showErrorExplanation = (str) ->
  el = $('#post-mail-wrapper .error-expalnation')
  el.append '<li class="error">' + str + '</li>'
  el.show()


$ ->

  if $('#new_sharing_mail').length
    
    $('#new_sharing_mail').submit (e) -> 
      $this = $(this)
      clearForm($this)

      unless validForm()
        e.preventDefault()
        return false

      return true

    $('#toggle-mode').click (e) ->
      e.preventDefault()
      clearForm($('#new_sharing_mail'))
      if $('#post-mail-content').hasClass('edit') 
        if validForm()
          $('#post-mail-content').removeClass('edit')
          $('#post-mail-content').addClass('preview')
          $('#toggle-mode').html(WT.Languages.default().edit)

          formData = $('#new_sharing_mail').serializeArray()
          $.ajax
            url: '/share/email/preview'
            data: formData
            dataType: 'script'

      else 
        $('#post-mail-content').removeClass('preview');
        $('#post-mail-content').addClass('edit');
        $('#toggle-mode').html(WT.Languages.default().preview)

