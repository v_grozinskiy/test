@WT = {
  Language: 'en'
  Media: undefined
}


$.extend {
  scrollWin: if navigator.userAgent.match(/webkit/i) then 'body' else 'html'
}

$ ->

  $(document).ready (e)->
    if WT._dev
      if WT._ssl
        $('body').append('<div class="dev_ssl_icon" style="position:fixed; top: 8px; right: 5px; z-index: 1000000; width: 30px; height: 30px; background: #FF0; color: #000; text-align: center; font-size: 20px; line-height: 30px; overflow: hidden; vertical-align: baseline; border-radius: 100%; padding: 0; font-weight: bold;" title="SSL Request"><span class="glyphicon glyphicon-lock">&Oslash;</span></div>')

