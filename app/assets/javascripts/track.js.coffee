WT.Track = {
  parse : (info) ->
    if this[ info['fn'] ]
      this[ info['fn'] ]( info['evt'], info['loc'], info['msg'] )
    else if WT._dev && window.console
      console.error(['ERROR: evt not found.',info])

  page : (evt,loc,msg) ->
    this._track('page', evt, loc, msg)
  
  click : (evt,loc,msg) ->
    this._track('click', evt, loc, msg)
  
  action : (evt,loc,msg) ->
    this._track('action', evt, loc, msg)
  
  abtest : (name,test,action) ->
    this._track('ab_test', name, test, action)

  _current_page : ->
    return window.location.href.replace(window.location.host, "").replace(window.location.protocol+"//", '')
  
  _track : (a,e,l,m) ->
    # Send information to Google Analytics
    ga('send', 'pageview', '/_event/' + [a,e,l,m].join('/').replace(/\/\//m, '/'));
    # ga('send', 'event', e.toLowerCase(), a.toLowerCase(), l.toLowerCase(), m.toLowerCase())
    return true;
};

$(document).ready( ->
  $(document).on('click', '*[data-wt-trk]', (e) ->
    info = $(this).data('wt-trk')
    if (typeof info is 'object')
      WT.Track.parse(info)
  )
)