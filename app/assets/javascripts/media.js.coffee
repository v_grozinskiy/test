WT.Media =
  initialize: ->

  provider: undefined
  mediaId: undefined
  isValid: false

  oembedUrls:
    vimeo:        'https://vimeo.com/api/oembed.json?url=http%3A//vimeo.com/'
    twitter:      'https://api.twitter.com/1/statuses/oembed.json?id='
    hulu:         'http://www.hulu.com/api/oembed.json?url=http://www.hulu.com/watch/'
    funnyordie:   'http://www.funnyordie.com/oembed.json?url=http://www.funnyordie.com/videos/'
    collegehumor: 'http://www.collegehumor.com/oembed.json?url=http://www.collegehumor.com/video/'
    kickstarter:  'https://www.kickstarter.com/services/oembed?url=https://www.kickstarter.com/projects/'
    vine:         'https://api.embed.ly/1/oembed?url=https://vine.co/v/'
    instagram:    'https://api.instagram.com/oembed?beta=true&url=http://instagram.com/p/'

  suffix:
    vimeo:        ''
    twitter:      '/'
    hulu:         'watch/'
    funnyordie:   'videos/'
    collegehumor: 'video/'
    kickstarter:  'projects/'
    vine:         'v/'
    instagram:    'p/'
    facebook:     '/'

  regExs:
    youtube:      /^.*(youtube\.com\/)((v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?\"]*).*/i
    youtube_short:/^.*(youtu\.be\/)([^#\&\?\"]*).*/i
    vimeo:        /^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/)|(video\/))?([0-9]+).*/i
    hulu:         /(http:\/\/)?(www\.)?hulu.com\/watch\/?(\d+)?/
    hulu_em:      /(http:\/\/)?(www\.)?hulu.com\/embed.html\?eid=([\w-]+)?/
    funnyordie:   /(http:\/\/)?(www\.)?funnyordie.com\/(videos|embed)\/?([\w]+)?/
    collegehumor: /(http:\/\/)?(www\.)?collegehumor.com\/(video|e)\/?(\d+)?/
    kickstarter:  /(http(s)?:\/\/)?(www\.)?kickstarter.com\/projects\/?([\w\d]+\/[\w-]+)?/
    twitter:      /(http(s)?\:\/\/).*(twitter\.com\/)([a-z0-9_]{1,15}\/status(es)?\/)([0-9]+)/i
    vine:         /(http(s)?\:\/\/).*(vine\.co\/v\/)([a-z0-9\-]{11})/i
    instagram:    /(http:\/\/)?(www\.)?instagram.com\/p\/?([a-z0-9\-\_]{10})/i
    facebook:     /(https:\/\/)(www\.)facebook.com\/(.*)/
    etc:          /\<(iframe|embed|object|script)/i

  cleanNonUtf8: (str) ->
    return '' unless str?
    r = /[^(\x20-\x7F)]/
    while str.match(r)
      str = str.replace(r, ' ')
    str

  updateMedia: (data) ->
    @isValid = true

    source = if @provider is 'twitter' or @provider is 'facebook'
      data.url
    else
      data.provider_url + (unless data.provider_url.match(/\/$/) then '/' else '') + @suffix[@provider] + @mediaId

    # Some oembed media codes are so whack, we want to make these items loads better or with less crap.
    media = if @provider is 'vine'
      '<iframe class="vine-embed" src="https://vine.co/v/' + @mediaId + '/embed/postcard" width="600" height="600" frameborder="0"></iframe>'
    else if @provider is 'instagram'
      '<iframe src="//instagram.com/p/' + @mediaId + '/embed/" width="612" height="710" frameborder="0" scrolling="no" allowtransparency="true"></iframe>'
    else if @provider is 'twitter' or @provider is 'facebook'
      data.url
    else if data.media
      data.media
    else
      data.html

    imageUrl = data.thumbnail_url

    @updateView
      'video-id': @mediaId
      provider: @provider
      title: @cleanNonUtf8(data.title)
      text: data.description
      source: source
      'image-url': imageUrl
      duration: data.duration
      via: (data.via or data.provider_url)
      media: media
      type: data.type

  getMedia: (opts) ->
    url = @oembedUrls[@provider] + @mediaId
    if opts && opts.proxy && opts.url
      url = '/admin/post_items/oembed.json?url=' + opts.url

    $.ajax
      url: url + '&callback=WT.Media.updateMedia'
      dataType: 'jsonp'
      success: (data, textStatus) =>
        @updateMedia data
      fail: ->
        @isValid = false

  getYoutubeInfo: ->
    $.support.cors = true

    $.ajax
      url: 'https://www.googleapis.com/youtube/v3/videos'
      data: 
        id: @mediaId
        key: 'AIzaSyDwnASvnmc9rqTprmUj0u8x_KcdENLiboo'
        part: 'snippet,contentDetails,statistics' 
      contentType: 'text/plain'
      crossDomain: true
      type: 'GET'
      cache: false
      dataType: 'jsonp'
      success: (data) =>
        unless data.items[0] is undefined
          @isValid = true
          detail = data.items[0]
          source = "http://www.youtube.com/watch?v=" + @mediaId
          @updateView
            'video-id': @mediaId
            provider: 'youtube'
            title: detail.snippet.title
            text: detail.snippet.description
            source: source
            media: source
            via: 'http://www.youtube.com'
            duration: detail.contentDetails.duration
            'image-url': detail.snippet.thumbnails["high"]["url"]
            'like-count': detail.statistics.likeCount
            'comment-count': detail.statistics.commentCount
        else
          @isValid = false
      error: (jqXHR, textStatus) ->
        @isValid = false

  getFacebookInfo: ->

  removeMediaError: ->
    mediaEl = if @referrer is 'post-item'
      $('.post-item-media')

    if mediaEl  
      mediaEl.parent().removeClass 'has-error'
      next = mediaEl.next()
      next.remove() if next.hasClass 'parsley-error-list'

  updateView: (media) ->
    @removeMediaError()
    if media.provider is 'instagram'
      # if media.type is 'photo'
      #   @changeMediaStyle('Image')
      #   $('.upload-from-file').hide()
      #   $('.upload-from-url').show()
      #   $('.upload-from-url p').hide()
      #   imageOnLoad media['image-url']
      if media.type is 'photo'
        @changeMediaStyle('Embed')
        # $('.upload-from-file').hide()
        # $('.upload-from-url').show()
        # $('.upload-from-url p').hide()
        # imageOnLoad media['image-url']
      else
        @changeMediaStyle('Video')
    else if media.provider is 'facebook' or media.provider is 'twitter'
      @changeMediaStyle('Tweet')
    else if !(media.provider is 'twitter' || media.title is undefined)
      @changeMediaStyle('Video')

    $.each media, (key, value) =>
      el = $('.nested-fields').find('.' + @referrer + '-' + key)
      return unless el.length
      origin = el.val()
      return if (key is 'title' || key is 'text') && !( origin is '' || origin is undefined)

      for item in el
        if $(item).is('input') or $(item).is('textarea')
          $(item).val value
        else
          $(item).html value

      if key is 'image-url' # Try both types
        $('.preview-wrapper .img').css 'background-image', (if value then 'url("' + value + '")' else 'none')
        if value
          $('.preview-wrapper img').attr 'src', value
        else
          $('.preview-wrapper img').attr 'src', $('.preview-wrapper img').attr('data-reset-src')


  changeMediaStyle: (style) ->
    $('.nested-fields .item-media').removeClass('text_item image_item tweet_item embed_item video_item').addClass(style.toLowerCase() + '_item')
    $('.nested-fields .post-item-style').val(style.toLowerCase() + '_item')
    $('.nested-fields .post_post_items_media label').html(WT.Languages.default().post[style.toLowerCase() + '_url'])
    $('.nested-fields .heading h4').html(style + ' Item')

  validate: (url) ->
    match = url.match(@regExs[@provider])
    if match && match[match.length - 1]
      @mediaId = match[match.length - 1]
      return true
    else
      return false

  parseMedia: (url, referrer='post-item') ->
    @referrer = referrer
    @provider = undefined
    @isValid = false

    # YouTube
    match = url.match(@regExs['youtube'])
    if match && match[7].length is 11
      @mediaId = match[7]
      @provider = 'youtube'
      return @getYoutubeInfo()

    # YouTube Short URL
    match = url.match(@regExs['youtube_short'])
    if match && match[2].length is 11
      @mediaId = match[2]
      @provider = 'youtube'
      return @getYoutubeInfo()

    providers = ['vimeo', 'hulu', 'twitter', 'hulu_em', 'vine', 'instagram', 'facebook', 'kickstarter', 'funnyordie', 'collegehumor']

    for p in providers
      @provider = p
      if @validate(url)
        if p is 'hulu_em'
          @isValid = true
          return @updateMedia { media: url, via: "http://www.hulu.com/" }
        else if p is 'facebook'
          @isValid = true
          window.ccl = $('<div></div>').append(url);
          postUrl = $('<div></div>').append(url).find('.fb-post').data('href')
          match = postUrl.match(@regExs['facebook'])
          if match[3]?
            @mediaId = match[3]
            return @updateMedia { url: postUrl, via: 'https://www.facebook.com'}
          else
            return false
        else if p is 'kickstarter' or p is 'funnyordie' or p is 'collegehumor'
          # @updateMedia { media: url, provider_url: url.replace(/^(http(s)?\:\/\/([A-Z0-9\-\.]+)\/).*/i, '$1') }
          @getMedia({proxy : true, url : url})
          return
        else
          return @getMedia()

    match = url.match(@regExs['etc'])
    if $('.post-item-style').length && $('.post-item-style').val().toLowerCase() is "embed_item" && match && match.length > 1
      @isValid = true
