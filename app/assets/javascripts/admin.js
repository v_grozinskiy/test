//= require jquery
//= require jquery_ujs
//= require jquery.xdomainrequest.min
//= require jquery.remotipart
//= require jquery.ui.sortable
//= require bootstrap
//= require moment
//= require bootstrap-datetimepicker
//= require select2
// require select2_locale_ca
//= require parsley
//= require jquery.Jcrop.min
//= require jquery.markitup
//= require wt
//= require languages
//= require media
//= require cocoon
//= require_tree ./admin
//= require rails-timeago


$(document).ready(function() {
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  $('[data-loading-text]').not('[data-confirm]').click(function () {
    if ($(this).closest('[data-validate="parsley"]').length && !$(this).closest('[data-validate="parsley"]').parsley('isValid')) return true;
    $(this).button('loading'); return true;
  });
  $('[data-loading-text][data-confirm]').on('confirm:complete', function (e,a) {if (!!a) $(this).button('loading'); return true;});

  $('#dashboard_trends a[data-toggle="tab"], #post_trends a[data-toggle="tab"]').on('shown.bs.tab', function() {
    window.dispatchEvent(new Event('resize'));
  })

  $('[data-dismiss-id]').each(function() {
    var id = $(this).data('dismiss-id');
    if (localStorage[ id ] == 'closed') $(this).hide();
  });

  $('[data-dismiss-id] .close').on('click', function(e) {
    var id = $(this).parents('[data-dismiss-id]').data('dismiss-id');
    if (id) localStorage[ id ] = 'closed';
    return true;
  });

  if (WT._ssl) {
    $('html').addClass('ssl')
    $('iframe[src^="http://"]').after('<div class="after"></div>').parent().css('position', 'relative')
  }

  $(window).resize(function() {
    $('.fixed-col').each(function() {
      var r = $(window).width() - $(this).parent().offset().left - $(this).parent().width(),
          w = $(this).parent().width()-$.map($(this).siblings(), function(v) {return $(v).outerWidth();}).reduce(function(a, b) {return a + b;});
      $(this).css({'right' : r+'px', 'width' : w+'px', 'left' : 'auto'});
    })
  }).resize();
});