@addCommas = (nStr) ->
  nStr += ''
  x = nStr.split('.')
  x1 = x[0]
  x2 = if x.length > 1
    '.' + x[1]
  else
    ''
  rgx = /(\d+)(\d{3})/
  
  while (rgx.test(x1))
    x1 = x1.replace(rgx, '$1' + ',' + '$2')
  return x1 + x2

updateFacebook = (data) ->
  if data.data[0]
    fb_count = data['data'][0]['share_count']
    comments_count = data['data'][0]['commentsbox_count']
    if fb_count && fb_count >= 0
      $('.post-shares .share-platform-facebook .share-count').each ->
        $(this).html addCommas(fb_count)
        if fb_count > 0
          $(this).show()
    if comments_count && comments_count >= 0
      $('.post-comments-count').each ->
        $(this).find('span').html addCommas(comments_count)
        if comments_count > 0
          $(this).addClass(if comments_count > 1 then 'has-comments' else 'has-comment').show()


window.updateTweet = (data) ->
  tw_count = data['count']
  if tw_count && tw_count >= 0
    $('.post-shares .share-platform-twitter .share-count').each ->
      $(this).html addCommas(tw_count)
      $(this).show()

updatePinterest = (data) ->
  pin_count = data.count && parseInt(data.count)
  if pin_count && pin_count >= 0
    $('.post-shares .share-platform-pinterest .share-count').each ->
      $(this).html addCommas(pin_count)
      $(this).show()

updateGooglePlus = (data) ->
  google_count = data.result && data.result.metadata.globalCounts.count
  if google_count && google_count >= 0
    $('.post-shares .share-platform-googleplus .share-count').each ->
      $(this).html addCommas(google_count)
      $(this).show()

window.updateStumbleupon = (data) ->
  return
  stumbleupon_count = data['count']
  if stumbleupon_count && stumbleupon_count >= 0
    $('.post-shares .share-platform-stumbleupon .share-count').each ->
      $(this).html addCommas(stumbleupon_count)
      $(this).show()

window.updateReddit = (data) ->
  if (data && data.data && data.data.children[0] && data.data.children[0].data)
    reddit_count = data.data.children[0].data.num_comments
    if reddit_count && reddit_count >= 0
      $('.post-shares .share-platform-reddit .share-count').each ->
        $(this).html addCommas(reddit_count)
        $(this).show()

window.updateLinkedin = (data) ->
  if data
    linked_count = data.count
    if linked_count  && linked_count  >= 0
      $('.post-shares .share-platform-linkedin .share-count').each ->
        $(this).html addCommas(linked_count)
        $(this).show()

$ ->

  $.support.cors = true

  $('.post-comments-count').click ->
    t = $('#comments')
    if t.size > 0
      $($.scrollWin).animate({scrollTop: t.offset().top - 85 + "px"}, 500);
      return false

  $('.social-btn').click ->
    platform = $(this).parents('li').data('platform')
    postId = $('.post-container').data('id')
    $.ajax
      url: '/posts/' + postId + '/share'
      type: 'PUT'
      dataType: 'json'
      data: { platform: platform }
      success: (returnData) ->
        $('li.social-counts.' + platform + '-counts').html returnData

  $(window).load ->
    # -- FETCH POST COUNTS, ONLY WEB (NOT MOBILE)
    if WT.post_url && $('.post-shares').length && $(window).width() >= 768
      if $('.post-shares .share-platform-facebook .share-count').size()
        try
          $.getJSON 'https://graph.facebook.com/fql', { q: ('select total_count, share_count, like_count, click_count, comment_count, commentsbox_count from link_stat where url="' + WT.post_url + '"') }, 
            (data, status, xhr) ->
              updateFacebook(data)
        catch e

      if $('.post-shares .share-platform-twitter .share-count').size()
        try
          $.ajax
            url: 'https://cdn.api.twitter.com/1/urls/count.json?url=' + WT.post_url + '&callback=updateTweet',
            dataType: 'jsonp'
        catch e

      if $('.post-shares .share-platform-pinterest .share-count').size()
        try
          $.ajax
            url: 'https://widgets.pinterest.com/v1/urls/count.json?url=' + WT.post_url,
            dataType: 'jsonp',
            success: (data) ->
              updatePinterest(data)
        catch e

      ###
      $.ajax 
        url: 'http://www.stumbleupon.com/services/1.01/badge.getinfo?url=' + WT.post_url + '&callback=updateStumbleupon'
        dataType: 'jsonp'
        crossDomain: true
        contentType: 'application/json'
        accepts: 'json'
        success: (data) ->
          alert data
      ###

      if $('.post-shares .share-platform-reddit .share-count').size()
        # Reddit does not support SSL (WTF!?)
        unless WT._ssl
          try
            $.getJSON 'http://buttons.reddit.com/button_info.json?url=' + WT.post_url + '&callback=updateReddit', (data) ->
              updateReddit(data)
          catch e

      if $('.post-shares .share-platform-pinterest .share-count').size()
        try
          $.ajax
            url: 'https://www.linkedin.com/countserv/count/share?url=' + WT.post_url + '&lang=en_US&callback=updateLinkedin', 
            dataType: 'jsonp'
        catch e

      if gapi && $('.post-shares .share-platform-googleplus .share-count').size()
        try
          params = {
            nolog: true
            id: WT.post_url
            source: "widget"
            userId: "@viewer"
            groupId: "@self"
          }

          gapi.client.rpcRequest('pos.plusones.get', 'v1', params).execute (resp) ->
            updateGooglePlus resp
        catch e

