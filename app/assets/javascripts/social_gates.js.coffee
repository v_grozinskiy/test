if not @WT
  @WT = {}


window.WT.SocialGate = (platform)->
  this.available_platforms = ['twitter', 'facebook', 'newsletter']
  this.platform = if platform && platform != '' then platform else 'newsletter'
  this.id = 'social-gate-' + this.platform

  # Check if previously completed, if so, then stop.
  if localStorage[ this.id ] == 'closed' && !window.WT._dev
    this.completed = true
    return false

  _t = this

  $('body').on 'click', 'a[data-dismiss-id="' + this.id + '"]', (e) ->
    _t.cancel()
    return true

  return this

$.extend(true, window.WT.SocialGate.prototype, {
  as : null
  completed : false
  close_intv: null

  # Start!
  load : ->
    return true if this.completed
    _t = this

    $('#social-gate').remove()
    $('body').append('<div id="social-gate"><div id="social-gate-content"></div><a tabindex="1", href="javascript:;" data-dismiss-id="' + _t.id + '">&times;</a></div>')
    if status = _t.load_platform()
      $('body').toggleClass('social-gate', status)

      # close after 15 seconds if nothing done
      _t.close_intv = setTimeout(->
        _t.cancel()
      , 15000)

  # Seconds countdown, do close
  countdown: (s)->
    _t = this
    if s > 0
      $('#social-gate .thanks').addClass('show').find('span').text(s + ' second' + (unless s == 1 then 's' else ''))
      setTimeout(->
        _t.countdown(s-1)
      , 1000)
    else
      _t.close()

  # Complete close
  complete : (s)->
    this.completed = true
    this.countdown(s || 2)
    this.ping('follow')

  # Close the modal, mark in localstorage as closed
  close : ->
    localStorage[ this.id ] = 'closed'
    $('#social-gate').hide()
    $('body').removeClass('social-gate')
    try clearTimeout(this.close_intv) catch e

  # Track ab test, mark as cancel or finish on back-end
  ping : (a)->
    window.WT.Track.abtest('social_gates', this.platform)
    $.ajax {
      url: '/_ab/social_gates/' + a
      type: 'PUT'
      dataType: 'json'
      success: (returnData)-> {}
      error : -> {}
    }

  # Cancel the gate
  cancel : ->
    this.cancelled = true
    this.close()
    this.ping('cancel')

  # Load the requested platform and UX logic.
  load_platform : ->
    _t = this
    return switch _t.platform

      # Twitter overlay
      when 'twitter' 
        $('#social-gate-html-twitter').detach().appendTo('#social-gate-content')

        # On button press, close window (even though this is not a true follow, we do open the web intent)
        $('#follow-twitter-button').on 'click', ->
          _t.complete(2)
        true

      # Facebook overlay
      when 'facebook' 
        $('#social-gate-html-facebook').detach().appendTo('#social-gate-content')
        if window.FB
          # On successful like: track & copete
          window.FB.Event.subscribe 'edge.create', (p,el)->
            window.WT.Track.action('like', 'facebook', p)

            # These are bad hacks for the flyout on FB like.... quick resize resets this.
            $(el).find('span').css({'opacity' : 0, 'height' : 999})
            setTimeout( ->
              $(el).find('span').css({'opacity' : 1, 'height' : 20})
            , 1)

            _t.complete()

          # On successful dislike: track & complete
          window.FB.Event.subscribe 'edge.remove', (p,el)->
            window.WT.Track.action('unlike', 'facebook', p)
            _t.complete(3)
        true

      # Newsletter overlay
      when 'newsletter' 
        $('#social-gate-html-newsletter').detach().appendTo('#social-gate-content')

        # On keypress, validate email and toggle error and icon styling
        $('#social-gate-html-newsletter input[type="email"]').on 'keypress', ->
          $('#social-gate-html-newsletter .error-msg').removeClass('show')
          v = $.trim($(this).val())
          m = (v== '' || v.match(/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i))
          $(this).parents('.input').toggleClass('error', !m)

        # On ajax return toggle thanks message & complete gate
        $('#social-gate-html-newsletter form').on('ajax:error ajax:success', ->
          $(this).hide()
          $(this).parent().find('.error-msg').hide()
          $(this).parent().find('.thanks').addClass('show').show()
          _t.complete(3)

        # On submit, check email is valid and present before submitting. If invalid, show error. If valid, disable button and continue
        ).on('submit', ->
          v = $.trim($('#social-gate-html-newsletter input[type="email"]').val())
          if v != '' && v.match(/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i)
            $('#social-gate-html-newsletter button').attr('disabled', 'disabled')
            true
          else
            $('#social-gate-html-newsletter .error-msg').addClass('show')
            false
        )
        true

      # Unknown gate, don't continue
      else
        false
})