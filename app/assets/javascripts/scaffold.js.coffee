currentCategory = null

showDropdown = (e) ->
  return unless $(window).width() >= 768

  category = $(e.currentTarget).data('category')
  menu = $('#dropdown-menus .category-list[data-category="' + category + '"]')
  top = 0

  if $(window).scrollTop() < 110
    top = 110 - $(window).scrollTop()

  $('#dropdown-menus .category-list:not([data-category="' + category + '"])').hide()
  $('#site-nav li.dropdown').removeClass('active');

  if currentCategory && menu.length
    $('#dropdown-menus').addClass('open')
    menu.show()
    $('#site-nav li.dropdown[data-category="'+ category + '"]').addClass('active');
    $(window).trigger('preload:images')
  else
    $('#dropdown-menus').removeClass('open')
    
hideDropdown = (e) ->
  if !e || !e.relatedTarget
    $('#site-nav li.dropdown').removeClass('active');
    $('#dropdown-menus').removeClass('open')
    return

  $relatedTarget = $(e.relatedTarget)
  unless (($relatedTarget.parents('.dropdown').length + $relatedTarget.parents('#dropdown-menus').length) || $relatedTarget.attr('id') == 'dropdown-menus')
    $('#site-nav li.dropdown').removeClass('active');
    $('#dropdown-menus').removeClass('open')


$(document).ready $ ->
  $.timeago.settings.lang = $('html').attr('lang') if $.timeago

  # --- PRELOAD ---------------------------------------------------------------
  preload_win_width  = null
  preload_win_height = null
  preload_scroll_top = 0
  preload_images     = []
  above_window_frame = (top,height) -> return (top - preload_scroll_top  + height) < 0
  below_window_frame = (top,height) -> return (top - preload_scroll_top ) > preload_win_height 
  in_window_frame    = (top,height) -> return !(below_window_frame(top,height) || above_window_frame(top,height))

  if WT._ssl
    $('html').addClass('ssl')


  # --- CSS CALC AVAILABILITY TEST --------------------------------------------
  $('body').append('<div id="css3-calc-test" style="width:0;width:-webkit-calc(2px + 2px);width:-moz-calc(2px + 2px);width:calc(2px + 2px);overflow:hidden;"></div>');
  unless parseInt( $('#css3-calc-test').width() ) == 4
    $('html').addClass('css-no-calc')
    $(window).resize( (e) ->
      $('.content-wrap ul#posts-list').addClass('calc-fix')
      $('.scalable').css('width', ($(window).width() - $('#sidebar').outerWidth()) + 'px')
      if $('.featured-post-container').size()
        l = $('.trends-list-container').position().left + $('.trends-list-container').width()
        r = $('.post-grid-container').width()
        $('.featured-post-container').css({'left' : l + 'px', 'right' : r + 'px'})
    )
  $('#css3-calc-test').remove()

  # CAROUSEL
  $('.carousel').carousel()
  $('#video-carousel, #video-carousel-mobile').on('slid.bs.carousel', ->
    $(window).trigger('preload:images')
  )

  $.ajaxSetup {
    complete: (e)->
      $('time[data-time-ago]').timeago()
  }

  # TIMEAGO
  setTimeout (e) ->
    $('time[data-time-ago]').timeago()
  , 60000

  # DISMISSAL LOCALSTORE
  $('[data-dismiss-id]').each ->
    id = $(this).data('dismiss-id')
    if localStorage[ id ] == 'closed' && !WT._dev
      $(this).hide()
  $('[data-dismiss-id] .close').on 'click', (e) ->
    id = $(this).parents('[data-dismiss-id]').data('dismiss-id');
    if id
      localStorage[ id ] = 'closed'
    return true;


  # WINDOW RESIZE
  $(window).resize (e) ->
    # PRELOAD IMAGES
    preload_win_width  = $(window).height();
    preload_win_height = $(window).height();
    preload_scroll_top = $($.scrollWin).scrollTop();
    preload_images     = [];
    $('[data-preload-img]').not('[data-preloaded]').not('[data-preload-err]').each( ->
      preload_images.push({
        target: $(this),
        top:    $(this).offset().top,
        left:   $(this).offset().left,
        width:  $(this).width(),
        height: $(this).height()
      })
    )
    # $('.viewport-size').each( ->
    #   $(this).css('padding-top', ((preload_win_height  / preload_win_width) * 100) + '%')
    # );

    # FB COMMENTS REPONSIVE WIDTH FIX
    $(".fb-comments").attr("data-width", $("#fb_comment_area").width());
    if window.FB
      FB.XFBML.parse($("#fb_comment_area")[0]);

  # WINDOW SCROLL
  $(window).scroll (e) ->
    # PRELOAD IMAGES
    preload_scroll_top = $($.scrollWin).scrollTop()
    preload_images = $.grep(preload_images, (item,i) ->
      if !item || typeof item == 'undefined' || !item.target.attr('data-preload-img') || item.target.attr('data-preload-img') == ''
        return false;
      if !!item.target.data('preloaded') || !!item.target.data('preload-err')
        return false;
      if item.target.is(':visible') && in_window_frame(item.top,item.height)
        preload = new Image()
        preload.onload = -> 
          item.target.attr('data-preloaded', true).css('background-image', 'url("' + preload.src + '")')
        preload.onerror = -> 
          item.target.attr('data-preload-err', true)
        preload.src = item.target.attr('data-preload-img')
        return false
      return true
    )

    # DROPDOWNS
    if $('#site-nav').length
      if $(window).scrollTop() > $('#top-ad-wrapper').outerHeight()
        $('#site-nav').addClass('navbar-fixed-top')
        $('#dropdown-menus').addClass('navbar-fixed-top')
      else
        $('#site-nav').removeClass('navbar-fixed-top')
        $('#dropdown-menus').removeClass('navbar-fixed-top')

  $('#dropdown-menus').removeClass('open')
  currentCategory = null

  # --- SHARE POPOVER ---------------------------------------------------------
  sbtn_timeout = null
  sbtn_id = null
  $(document).on('mouseover', 'li.post .post-share', (e) ->
    id = $(this).parents('li.post').data('post-id');
    if sbtn_id == id
      clearTimeout sbtn_timeout
    sbtn_id = id

    if ((b = $(this).parents('li').find('.post-share-overlay')))
      a = $(this).parents('a').position()
      p = $(this).position()
      t = p.top - 45
      l = a.left + 12
      b.css({top: t + 'px', left: l + 'px'}).addClass('open');
      $(this).parents('li').find('a').addClass('hover')
  ).on('mouseout', 'li.post .post-share', (e) ->
    if ((b = $(this).parents('li').find('.post-share-overlay')))
      sbtn_timeout = setTimeout ->
        b.removeClass('open')
        b.parents('li').find('a').removeClass('hover')
      ,150
  ).on('mouseover', 'li.post .post-share-overlay', (e) ->
    id = $(this).parents('li.post').data('post-id');
    if sbtn_id == id
      clearTimeout sbtn_timeout
    sbtn_id = id
  
    if ((b = $(this).parents('li').find('.post-share')))
      a = $(this).parents('a').position()
      p = b.position();
      t = p.top - 45
      l = a.left - 8
      $(this).css({top: t + 'px', left: l + 'px'}).addClass('open');
      $(this).parents('li').find('a').addClass('hover')
  ).on('mouseout', 'li.post .post-share-overlay', (e) ->
    t = this
    sbtn_timeout = setTimeout ->
      $(t).removeClass('open')
      $(t).parents('li').find('a').removeClass('hover')
    ,150
  )

  # --- DROPDOWN FIXES --------------------------------------------------------
  $('#site-nav li.dropdown').mouseout (e) ->
    currentCategory = null
    clearTimeout ddopen_timeout
    ddclose_timeout = setTimeout ->
      hideDropdown e
    , 100
  .mouseenter (e) ->
    currentCategory = $(e.currentTarget).data('category')
    t = if $('#dropdown-menus').hasClass('open') then 0 else 250
    clearTimeout ddclose_timeout
    ddopen_timeout = setTimeout ->
      showDropdown e
    , t

  $('#dropdown-menus').mouseout (e) ->
    currentCategory = null
    ddclose_timeout = setTimeout ->
      hideDropdown e
    , 100

  $('#search-form input[name="q"]').on('blur', (e) ->
    $(this).toggleClass('has-query', ($(this).val() != ''))
  ).trigger('blur')
  $(document).on 'click', '#search-form', (e) ->
    $(this).children('input[name="q"]').focus()

  $(document).on 'mouseover', '#dropdown-menus .tabs a', (e) ->
    tab = $(this).data('tab')
    $(this).parents('li').siblings().children('a').removeClass('active')
    $(this).addClass('active')
    $(this).parents('.tabs').siblings('.tab').removeClass('open')
    $(this).parents('.tabs').siblings('.tab[data-tab="' + tab + '"]').addClass('open')
    $(window).trigger('preload:images')

  ddopen_timeout = null
  ddclose_timeout = null

  $(window).trigger('preload:images')


$(window).on('preload:images', ->
  $(this).trigger('resize').trigger('scroll')
).on('load', ->
  $(this).trigger('preload:images')
)
