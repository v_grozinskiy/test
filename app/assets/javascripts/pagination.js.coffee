$(document).ready $ ->
  
  # --- PAGINATION ------------------------------------------------------------
  # AUTO PAGINATE
  # $(window).scroll (e) ->
  #   if $('.pagination').length
  #     url = $('.pagination a[rel="next"]').attr('href')
  #     if url && $(window).scrollTop() > $('#posts-list').height() - $(window).height() - 50
  #       pageNum = url.match(/page=(\d+)/)[1]
  #       if parseInt(pageNum) < 11
  #         $('.pagination').text("Fetching ...")
  #         $.getScript(url)
  $(document).on 'click', '.pagination a[rel~="next"]', (e) ->
    url = $(this).attr('href')
    if url
      unless $(this).hasClass('loading') || $(this).parents('.pagination').data('loading')
        $(this).addClass('loading')
        $('.pagination').data('loading',true)
        setTimeout (e) -> 
          $.getScript(url)
        , 500
      return false