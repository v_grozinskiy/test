if not @WT
  @WT = {}

window.WT.Share = (el)->
  this.element = $(el)
  this.parent = $(el).parents('.share-buttons:eq(0)')

  this.platform = this.element.data('platform')
  this.type = this.parent.data('item-type')
  this.id = this.parent.data('item-id')
  this.title = this.parent.data('title')
  this.text = this.parent.data('text')
  this.desc = this.parent.data('description')
  this.url = this.parent.data('canonical-url')
  this.image = this.parent.data('image')
  this.placement = this.parent.data('placement')
  this.via = this.parent.data('via')
  this.related = this.parent.data('related')
  this.locale = 'en_US'

  this.element.data('wt_share', this)
  return this

$.extend(true, window.WT.Share.prototype, {
  click : (e)->
    try
      return this[this.platform](e)
    catch e
      if WT._dev && window.console
        window.console.log('Share error:', this.platform, e)
      return true

  facebook : ->
    this._intent('intent')
    if window.FB
      _t = this
      FB.ui({method: 'feed', name: this.title, link: this.url, picture: this.image, caption: this.description}, (r)->
        if r && r.post_id
          _t._incr()
          _t._intent('share', r.post_id)
          _t._success('share')
        else
          _t._intent('cancel')
      )
    else
      this.window('https://www.facebook.com/sharer.php?' + $.param({u:this.url,t:this.title}),626,305)
    return false


  twitter : ->
    this._intent('intent')

    if window.twttr
      obj = {related:this.related,url:this.url,text:(this.text || this.title)}
      obj.via = this.via if this.via
      obj.related = this.related if this.related
      this.element.attr('href', 'https://twitter.com/intent/tweet?' + $.param(obj))
      window.twttr.widgets.load()
      return true
    else
      this.window(this.element.attr('href'))
      return false

  googleplus : ->
    this._intent('intent')
    this.window('https://plus.google.com/share?' + $.param({url:this.url}),500,565)
    return false

  linkedin : ->
    this._intent('intent')
    this.window('https://www.linkedin.com/cws/share?' + $.param({url:this.url,original_referer:document.referrer,_ts:(new Date()).getTime(),token:null,isFramed:true,lang:this.locale}) ,600,305)
    return false

  pinterest : ->
    this._intent('intent')
    this.window('http://www.pinterest.com/pin/create/button/?' + $.param($.extend(
      {},
      if this.image && this.image != undefined then {media: this.image} else {},
      {url:this.url, description:this.title}
    )),750,315)
    return false

  whatsapp : ->
    this._intent('intent')
    window.location.href = 'whatsapp://send?text=' + (this.text || this.title) + "%0A%0A" + this.url
    return false

  email : (e)->
    el = $(e.target)
    p = el.parents('[data-share-email-url]')
    u = null

    if p.size() > 0 && (u = p.attr('data-share-email-url')) != ''
      this._intent('intent')
      this.window(u,600,560)
    return false

  stumbleupon : ->
    this._intent('intent')
    this.window('http://www.stumbleupon.com/submit?' + $.param($.extend(
      {},
      if this.image && this.image != undefined then {media: this.image} else {},
      {url:this.url, title:this.title}
    )),750,550)
    return false

  reddit : ->
    this._intent('intent')
    this.window('http://reddit.com/submit?' + $.param($.extend(
      {}, 
      if this.image && this.image != undefined then {media: this.image} else {},
      {url:this.url, title:this.title}
    )),750,450)
    return false

  window : (u,w,h)->
    l = (window.screen.width / 2) - (w / 2)
    t = (window.screen.height / 2) - ((h / 2) + 50)
    t = 0 if t < 0
    opts = 'status=no,height=' + h + ',width=' + w + ',resizable=no,left=' + l + ',top=' + t + ',screenX=' + l + ',screenY=' + t + ',toolbar=no,menubar=no,scrollbars=no,location=no,directories=no'
    this._window = window.open(u, 'wt_share', opts)
    this._window.focus()

  _intent : (a,m)->
    window.WT.Track.action('share', this.platform, a)

  _success : (a)->
    ga('send', 'social', this.platform, a, this.url)

  _incr : ->
    _t = this
    u = '/' + this.type + '/' + this.id + '/share?' + $.param({platform:this.platform})
    $.ajax({
      url: u, 
      type: 'PUT', 
      dataType: 'json', 
      success: (returnData)->
        n = parseInt(_t.element.contents('.share-count').text()) + 1
        $('.share-buttons[data-item-type="' + _t.type + '"][data-item-id="' + _t.id + '"] a[data-platform="' + _t.platform + '"] .share-count').text(n)
      , error : ->
        #
    });
});

$(document).ready ->
  $(document).on 'click', '.share-buttons a[data-platform]', (e)->
    share = if $(this).data('wt_share') then $(this).data('wt_share') else (new window.WT.Share(this))
    return share.click(e)

$(window).on 'load', ->
  if window.twttr
    window.twttr.ready (twttr)->

      twttr.events.bind('tweet', (e)->
        share = new window.WT.Share(e.target)
        share._incr()
        share._intent('share')
        share._success('tweet')
      )

      twttr.events.bind('follow', (e)->
        if e.type == 'follow'
          window.WT.Track.action('follow', 'twitter')
      )

      twttr.events.bind('retweet', (e)->
        # TODO : TRACK RETWEET
      )

      twttr.events.bind('favorite', (e)->
        # TODO : TRACK FAVORITE
      )

      twttr.widgets.load()

