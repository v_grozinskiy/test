WT.Languages =
  initialize: ->
    window.ParsleyValidator.setLocale(WT.language)

  uninitialize: ->

  default: ->
    if WT.Languages[WT.language]
      WT.Languages[WT.language]
    else
      WT.Languages['en']
  
  en:
    blank:                    'This value is required.'
    invalid:                  'The value seems to be invalid.'
    unsaved_msg:              'You have unsaved changed. Please save changes before adding new.'
    confirm_msg:              'Are you sure?'
    form_error:               'Please review the problems below:'
    image_load_error:         'Error while loading image!'
    invalid_name:             'Please enter your name.'
    invalid_email:            'Please enter a valid email address.'
    invalid_time:             "can't be in the past."
    preview:                  'Preview'
    edit:                     'Edit'

    pagination:
      fetching:               'Fetchig...'

    post:
      search_placeholder:     'Search for a author'
      save_item_form:         'Please save post item first!'
      save:                   'Save'
      video_url:              'Video/Rich Media URL <abbr title="required">*</abbr>'
      image_url:              'Image URL <abbr title="required">*</abbr>'
      save_before_queue:      'You have made changes to this post. You must save this post before queueing.'
    
    trend_item:
      search_placeholder:     'Search for a post'
