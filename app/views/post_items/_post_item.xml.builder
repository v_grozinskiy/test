isDefault ||= false
item_defaults = Proc.new{
  xml.tag!('media:rating', (post_item.nsfw ? 'adult' : 'nonadult'), scheme: 'urn:simple')
  xml.tag!('media:title', post_item.title) unless post_item.title.blank?
  xml.tag!('media:description', type: :html) do
    xml.cdata!(post_item.text)
  end unless post_item.text.blank?
}

case (post_item.style || 'other').downcase.to_sym
  when :image
    xml.tag!('media:content', isDefault: isDefault, medium: :image, url: post_item.image.url(:large), type: post_item.image_content_type, width: nil, height: nil) do
      item_defaults.call(xml,post_item)
    end

  when :video
    xml.tag!('media:content', isDefault: isDefault, medium: :video) do
      item_defaults.call(xml,post_item)
      # xml.tag!('media:player', '') # TODO
    end

  when :embed
  # when :tweet

  else
    xml.tag!('media:content', isDefault: isDefault) do
      item_defaults.call
    end
end
