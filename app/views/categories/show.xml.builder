title = [@category.name, "What's Trending", "Latest Posts"].join(' - ')

xml.instruct! 

cache(['category', @category, 'page', (@_page || 1), 'xml'], expires_in: 15.minutes) do
  xml.rss(
    version: "2.0",
    'xmlns:blogChannel' => "http://backend.userland.com/blogChannelModule",
    'xmlns:media' => "http://search.yahoo.com/mrss/",
    'xmlns:dc' => "http://purl.org/dc/elements/1.1/",
    'xmlns:atom' => "http://www.w3.org/2005/Atom"
  ) do

    xml.channel do
      xml.title(title)
      xml.description
      xml.link(category_url(@category))
      xml.category(@category.name)
      xml.language('en')
      xml.lastBuildDate(Time.now.localtime.strftime('%a, %d %b %Y %H:%M:%S %z'))
      xml.copyright("Copyright #{Date.today.year} What's Trending Inc.")
      xml.managingEditor('editor@whatstrending.com (http://whatstrending.com/about)')
      xml.tag!('atom:link', rel: 'self', href: category_url(@category, format: :xml))

      xml.image do
        xml.title(title)
        xml.url(asset_url('share/rss-logo.png'))
        xml.link(category_url(@category))
      end
  
      @posts.each do |post|
        xml << render(post)
      end
    end
  end
end