items = Proc.new{
  xml.tag!('media:category', post.category.name)
  xml.tag!('media:rating', (post.nsfw ? 'adult' : 'nonadult'), scheme: 'urn:simple')
  post.authors.each do |author|
    xml.tag!('media:credit', author.name, role: 'author', scheme: 'urn:ebu')
  end
  xml.tag!('media:thumbnail', url: post.promo_image.url(:feed), height: nil, width: nil)

  post.post_items.each_with_index do |item,i|
    xml << render(item, locals: {isDefault: (i==0)})
  end
}

xml.item do
  xml.title(post.title)
  xml.description do
    xml.cdata!(post.description)
  end
  xml.category(post.category.name) unless post.category.blank?
  xml.pubDate(post.created_at.localtime.strftime('%a, %d %b %Y %H:%M:%S %z'))
  xml.link(post_url(post))
  xml.guid(post_url(post), isPermaLink: false)
  xml.enclosure(type: 'image/jpeg', url: post.promo_image.url(:feed), length: post.promo_image_feed_file_size) unless post.promo_image.blank?
  
  if post.post_items.count > 1
    xml.tag!('media:group') do
      items.call
    end
  else
    items.call
  end
end
