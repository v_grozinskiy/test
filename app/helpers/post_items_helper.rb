module PostItemsHelper

  def embed_code(post_item, autoplay=false)
    # Use our known adapters to auto-generate known media items and embed codes (newer lookup method)
    html = MediaAdapters.embed_code(post_item.media, prepend: '<div class="post-item-media-wrapper post-item-media-%{adapter_slug}">', append: '</div>')
    return html unless html.blank?

    # Otherwise go through the style and regexp type (older lookup method)
    case post_item.style
      when 'video_item'
        regex = /^.*((youtu.be\/|youtube\.com\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?\"]*).*/
        match = regex.match(post_item.media)
        return '<div class="post-item-media-wrapper post-item-media-youtube"><iframe width="640" height="360" src=//www.youtube.com/embed/' + match[7] + '?feature=player_detailpage"' + (autoplay ? '&autoplay=1' : '') + ' frameborder="0" allowfullscreen></iframe></div>' if match && match[7] && match[7].length == 11

        regex = /^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/)|(video\/))?([0-9]+)/
        match = regex.match(post_item.media)
        return '<div class="post-item-media-wrapper post-item-media-vimeo"><iframe src="//player.vimeo.com/video/' + match[match.length - 1] + (autoplay ? '?autoplay=1' : '') +'" width="640" height="266" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>' if match && match[match.length - 1]

      when 'tweet_item'
        if post_item.media.match(/facebook\.com/)
          return '<div class="post-item-media-wrapper post-item-media-facebook"><div class="fb-post" data-href="' + post_item.media + '" data-width="466"><div class="fb-xfbml-parse-ignore"></div></div></div>'
        elsif post_item.media.match(/twitter\.com/)
          return '<div class="post-item-media-wrapper post-item-media-twitter"><blockquote class="twitter-tweet" lang="en"><a href="' + post_item.media + '"></a></blockquote></div>'
        end
    end

    post_item.media || '' #'<p class="text-danger">' + t(:item_display_error) + '</p>'
  end

  def post_source_via_links(item)
    urls = []
    urls << "<span class='item-attribution-source'><em>" + t(:source) + ":</em> #{link_to Addressable::URI.parse(item.source).host, item.source, target: '_blank', rel: 'nofollow'}</span>" rescue nil unless item.source.blank?
    urls << "<span class='item-attribution-via'><em>" + t(:via) + ":</em> #{link_to Addressable::URI.parse(item.via).host, item.source, target: '_blank', rel: 'nofollow'}</span>" rescue nil unless item.via.blank?
    urls.compact!
    return "" if urls.blank?
    haml_tag :p, urls.join(' / ').html_safe, class: 'text-right small text-grey post-item-attribution'
  end

  def post_item_image(post_item, version=:medium)
    if post_item.image.content_type == 'image/gif'
      post_item.image(:large_gif)
    else 
      post_item.image(version)
    end
  end
end
