module ApplicationHelper

  def post_cache_key
    @_post_preview ? "preview-post-#{@post.id}" : @post
  end

  def post_cache_force
    return true if Rails.env.development?
    @_post_preview ? true : false
  end


  # --- SEO & META CONTENT HELPERS ---
  # Page title, description, meta, robots, canonical url, short URL, etc

  # Site is in English with audience in US.
  def page_lang
    'en'
  end

  def page_title
    @page_title || t(:logo)
  end

  def page_share_title
    return @page_share_title.call if @page_share_title && @page_share_title.respond_to?(:call)
    @page_share_title || page_title
  end

  def page_description
    return @page_description.call if @page_description && @page_description.respond_to?(:call)
    @page_description || t(:meta_description)
  end
  
  def page_keywords
    return @page_keywords.call if @page_keywords && @page_keywords.respond_to?(:call)
    @page_keywords || t(:meta_keywords)
  end

  def page_share_description
    return @page_share_description.call if @page_share_description && @page_share_description.respond_to?(:call)
    @page_share_description || page_description
  end

  def page_canonical_url(u=nil)
    @page_canonical_url = u unless u.blank?
    @page_canonical_url || request.original_url
  end

  def page_short_url(u=nil)
    @page_short_url = u unless u.blank?
    @page_short_url
  end

  def page_robots
    [@page_robots_index || 'index', @page_robots_follow || 'follow'].join(',')
  end

  def body_classes; @body_classes.compact; end


  # --- OTHER USEFUL HELPER METHODS ---

  def helper_popover(t=nil, c=nil, *args)
    opts = {html: false, placement: 'bottom', trigger: 'hover'}.merge(args.extract_options!)

    haml_tag :span, class: 'text-muted', 'data-toggle' => 'popover', 'data-placement' => opts[:placement], 'data-title' => t, 'data-content' => c.html_safe, 'data-html' => opts[:html].to_s, 'data-trigger' => opts[:trigger] do
      haml_tag :span, class: 'glyphicon glyphicon-question-sign'
    end
  end

  # Convert an integer into a duration timestap (e.g. hh:mm:ss)
  def number_to_duration(i)
    s,m,h = i % 60, (i / 60) % 60, i / (60 * 60)
    format("%02d:%02d:%02d",h,m,s).gsub(/^(00\:)/, '')
  end

  # Track event JSON options
  def wt_trk_evt(fn,evt,loc,*args)
    args.extract_options!.merge({fn: fn, evt: evt, loc: loc}).to_json
  end

  # Set the velocity class name based on the velocity
  def velocity_class_name(t=:views,n=0)
    min, max = 1,5
    s = (n.to_f / post_velocity_range[t][:intv].last).ceil rescue [min,max].mean
    "velocity-score-#{[max,[min,s].max].min}"
  end

  # Generate the meta cache key using defined params
  def meta_cache_key
    return @page_meta_cache_key unless @page_meta_cache_key.blank?
    key = "meta/#{controller_name}/#{action_name}"
    key += "/#{params[:id]}" if params[:id]
    key += "-#{@_page}" if @_page
    key
  end

  # Check if WT has toggle the Live button setting
  def wt_is_live?
    Setting.wt_youtube_live_url.strip.present? rescue false
  end

  # Category structure does not change much. Cache this request on post editor page.
  # (It makes multiple category table calls as it builds out the tree. Common sense says cache it.)
  def cached_nested_categories
    @cached_nested_categories ||= Rails.cache.fetch(['categories','nested_set_options'], expires_in: 6.hours) do
      nested_set_options(Category){|i| "#{'-' * i.level} #{i.name}" }
    end
  end

  def cached_tags_list
    @cached_tags_list ||= Rails.cache.fetch(['admin','tags_list'], expires_in: 6.hours) do
      Tag.all.map(&:name)
    end
  end

end
