module PostQueuesHelper

  def round_up(time, seconds = 300)
    Time.at((time.to_f / seconds).ceil * seconds)
  end
end
