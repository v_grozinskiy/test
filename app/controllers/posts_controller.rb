class PostsController < ApplicationController

  before_filter :load_resource,  only: [:show, :share, :comment]
  before_filter :load_latest_paginated_posts, if: :request_is_html, except: [:old, :random]
  before_filter :load_top_trends, if: :request_is_html, except: [:old, :random]
  before_filter :load_latest_wt_video_posts, only: [:show]
  before_filter :set_social_gate, only: [:show]

  respond_to :json, only: [:share, :comment]


  def show
    # Track views, unless referrer is self
    impressionist(@post) unless request_is_self?

    # Attempt to get similar posts for a particular post
    @similar_posts = cache([@post,'similar'], expires_in: 3.hours) do
      @post.category.posts.published.recent.where.not(id: @post.id).includes(:category).limit(10).all.to_a rescue nil
    end

    # Google Adsense disable on NSFW pages or pages before Jan 1 2013
    @_hide_ads = true if @post.nsfw? || @post.published_at.to_i < 1357016400

    render layout: 'post'
  end

  # Track sharing on a post through impressionist
  def share
    platform = params[:platform] || 'unknown'
    impressionist(@post, platform)

    @shares_count = @post.get_total_impression_count(message: platform)

    render json: @shares_count
  end

  # Track commenting on a post through impressionist
  def comment
    impressionist(@post)

    @post.increment!(:comments_count)
    @comments_count = @post.impressions.where(action_name: :comment).count

    render json: @comments_count
  end

  # Randomly get a previously published post
  def random
    @post = Post.published.order('rand()').first
    redirect_to post_url(@post)
  end

  # Since the URL structure for pages has changed with the new launch of WT, this means we need to redirect older posts to their new URL
  def old
    @post = Post.find_old_post(params[:slug], params[:year], params[:month])
    raise WhatstrendingWebApp::PostNotFound if @post.blank?
    redirect_to post_url(@post), status: 301
  end


private

  # Get the published post
  def load_resource
    @post = Post.published.find(params[:id]) rescue nil
    raise WhatstrendingWebApp::PostNotFound if @post.blank?
  end

end
