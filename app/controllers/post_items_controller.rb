class PostItemsController < ApplicationController

  before_filter :load_resource,  only: [:show, :share]
  before_filter :load_latest_paginated_posts, if: :request_is_html, only: [:show]
  before_filter :load_top_trends, if: :request_is_html, only: [:show]
  before_filter :load_latest_wt_video_posts, only: [:show]
  before_filter :set_social_gate, only: [:show]

  respond_to :json, only: [:share]


  def show
    unless request_is_self?
      impressionist(@item)
      impressionist(@post)
    end

    @_item_scroll_to = @item.slug
    render 'posts/show', layout: 'post'
  end

  def share
    platform = params[:platform] || 'unknown'
    impressionist(@item, platform) # track on item
    impressionist(@post, platform) # track on post
    @shares_count = @item.get_total_impression_count(message: platform)
    render json: @shares_count
  end


private

  def load_resource
    # Get the published post
    @post = Post.published.find(params[:post_id])
    raise ActiveRecord::RecordNotFound if @post.blank?

    # Get the specific post item from the post
    @item = @post.post_items.find(params[:id])
    raise ActiveRecord::RecordNotFound if @item.blank?

    # Logic to get first video item
    # if @post.format == 'Video'
    #   @post_item_video = Rails.cache.fetch([@post,'video'], expires_in: 1.hour) { @post.post_items.all.select{|v| v.style == 'Video'}[0] rescue nil }
    # end
  end

end
