class PasswordResetsController < ApplicationController

  force_ssl if: :ssl_configured?

  before_filter :load_user_using_perishable_token, only: [:edit, :update]

  layout 'admin'


  def create
    @user = User.find_by_email(params[:email])  
    if @user
      @user.deliver_password_reset_instructions!  
      redirect_to login_url, notice: t('.instruction_sent')
    else
      flash[:alert] = t('.email_not_found')
      render :new 
    end
  end

  def update
    if @user.update(params.require(:user).permit(:password, :password_confirmation))
      redirect_to login_url, notice: t('.notice')
    else
      render :edit
    end
  end

private

  def load_user_using_perishable_token
    unless (@user = User.find_using_perishable_token(params[:id]))
      redirect_to root_url, alert: t('password_resets.user_not_found')
    end
  end

end
