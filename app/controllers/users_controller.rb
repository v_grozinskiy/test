class UsersController < ApplicationController

  force_ssl if: :ssl_configured?, only: [:edit, :update]

  before_filter :authenticate_user!
  load_and_authorize_resource

  layout 'admin'


  def edit
    render :edit
  end

  def update
    if current_user.update(user_params)
      redirect_to edit_user_url, notice: t('.notice')
    else
      render :edit
    end
  end


private

  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :byline, :password, :password_confirmation, :avatar)
  end

end
