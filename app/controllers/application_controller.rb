class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  before_filter :app_init
  helper_method :current_user

  rescue_from WhatstrendingWebApp::PostNotFound, with: :rescue_from_post_not_foud
  rescue_from Timeout::Error, Redis::TimeoutError, with: :rescue_from_timeout

  helper_method :request_is_ssl?


  def app_init
    # Define some necessary variables
    @body_classes, @_css_include, @_js_include = [],[],[]
    @_page = [1,(params[:page] || 1).to_i].max
    # @_hide_ads = true if Rails.env.development? || Rails.env.test?
    @_use_custom_ad = false

    # Set the variant type by browser type
    # request.variant = :tablet if browser.tablet?
    request.variant = :phone if browser.mobile?

    # Set session information as needed
    set_session if request.session.blank? && request.session[:id].blank?

    # Set the locale if available but different to EN
    # I18n.locale = params[:locale] if User.is_valid_language(params[:locale])

    paperclip_request_options
  end

  def request_is_ssl?
    return false if Rails.env.development? || Rails.env.test?
    return @_is_ssl if defined?(@_is_ssl)
    return (@_is_ssl = false) && @_is_ssl if SSL_URL_OPTIONS.blank?
    SSL_URL_OPTIONS.each{|k,v| return (@_is_ssl = false) && @_is_ssl if request[k] != v }
    (@_is_ssl = true) && @_is_ssl
  end

  # Enable force_ssl where required
  def ssl_configured?
    Rails.env.production?
  end

  # Change Paperclip attachment default option during request if SSL, otherwise ensure revert back to HTTP
  def paperclip_request_options
    if Paperclip::Attachment.default_options[:s3_ssl_host_alias].present? && Paperclip::Attachment.default_options[:s3_http_host_alias].present?
      if request_is_ssl?
        Paperclip::Attachment.default_options[:s3_protocol] = "https"
        Paperclip::Attachment.default_options[:s3_host_alias] = Paperclip::Attachment.default_options[:s3_ssl_host_alias]
      else
        Paperclip::Attachment.default_options[:s3_protocol] = "http"
        Paperclip::Attachment.default_options[:s3_host_alias] = Paperclip::Attachment.default_options[:s3_http_host_alias]
      end
    end
  end

  def rescue_from_post_not_foud(e)
    respond_to do |format|
      format.html { render file: 'public/404', status: 404, layout: false }
      format.any { render status: 404, text: '', layout: false }
    end and return
  end

  def rescue_from_timeout(e)
    notify_airbrake(e) rescue nil
    respond_to do |format|
      format.html { render file: 'public/408', layout: false }
      format.any { render status: 408, text: '', layout: false }
    end and return
  end


  # Caches --------------------------------------------------------------------
  def delete_major_caches
    Category.delete_categories_cache(Category.all.pluck(:id))
    TrendList.regenerate_trend_cache
    Post.delete_global_cache
    Post.delete_homepage_cache
  end


private

  def set_session
    # R1 = first time referrer
    session[:id] = SecureRandom.hex(16)
    session[:r1] = case request.referrer
      when /facebook\.com|fb\.me/i;   "facebook"
      when /twitter\.com|t\.co/i;     "twitter"
      when /youtu\.be|youtube\.com/i; "youtube"
      when /plus\.google\.com/i;      "gplus"
      when /google\.([a-z\.])+|bing\.com|yahoo\.(([a-z\.])+)/; "search"
      when /\w+/;   "web"
      else;         "direct"
    end
  end

  # Set the social gate to use. If referrer was from one of the sites, override the social gate if social gate alternative is not the control
  def set_social_gate
    unless browser.mobile?
      @social_gate = ab_test('social_gates')
    end
  end

  # Is the request not JS or XML? Then must be HTML
  def request_is_html
    !request.xhr? && !request.format.js? && !request.format.xml?
  end

  # Is the request coming from itself? (A few 3rd party tools make requests that originate from self, we don't want to track those.)
  def request_is_self?
    (request.url == request.referer) rescue false
  end

  def store_location(u=nil)
    uri ||= request.get? ? request.request_uri : request.referer
    session[:return_to] = uri
  end

  def redirect_back_or_default(uri, *args)
    opts = args.extract_options!
    redirect_to(session.delete(:return_to) || request.referer || uri, opts)
  end

  def current_user?
    current_user.present?
  end

  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end
  
  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.record
  end

  def after_sign_in_url
    if !session[:referrer].nil?
      referrer = session[:referrer]
      session[:referrer] = nil
      return referrer
    elsif current_user? && current_user.admin?
      return admin_dashboard_index_url
    else
      return root_url
    end
  end

  def authenticate_user!
    unless current_user
      session[:referrer] = request.url
      redirect_to login_url, notice: t(:require_login)
      return false
    else
      return true
    end
  end

  def authenticate_admin_user!
    return false unless authenticate_user!
    unless current_user.admin?
      redirect_to root_url, alert: t(:not_authorized)
      return false
    end
  end

  # Pass controller namespace through Ability to define more granular actions
  def current_ability
    controller_namespace = params[:controller].split('/')[0..-2].join('/').camelize rescue nil
    @current_ability ||= Ability.new(current_user, controller_namespace)
  end

  rescue_from CanCan::AccessDenied do |exception|
    if current_user
      redirect_url = current_user.admin? ? admin_dashboard_index_url : root_url
      redirect_to redirect_url, alert: exception.message
    else
      authenticate_user!
    end
  end


  # Trends & Other Stats Methods/etc ------------------------------------------
  def load_latest_posts(*args)
    opts = args.extract_options!
    key = ['global','latest_posts']
    key += opts.keys unless opts.blank?
    opts[:limit] ||= 12

    @latest_posts = Rails.cache.fetch(key, expires_in: 5.minutes) do
      obj = Post.published.includes(:category).recent
      if opts[:not] && !(ids = opts[:not].map(&:id)).blank?
        obj = obj.where.not(id: ids)
      end
      obj = obj.unrecommended if opts[:unrecommended]
      obj = obj.limit(opts[:limit]) if opts[:limit]

      obj.all.to_a
    end
  end

  def load_latest_paginated_posts
    @latest_posts = Rails.cache.fetch(['global',"latest_posts-#{@_page}"], expires_in: 5.minutes) do
      Post.published.includes(:category).recent.paginate(page: @_page, per_page: 6)
    end
  end

  # TODO : OFFLOAD TO SIDEKIQ
  def load_latest_wt_video_posts
    @latest_video_posts = Rails.cache.fetch(['global','latest_video_posts'], expires_in: 5.minutes) do
      Post.published.recent.where(wt_original_video: true).limit(16)
    end
  end

  def load_top_trends
    return @trend_list if defined?(@trend_list)
    @trend_list ||= Rails.cache.read(['global','trend_list'])
    @trend_list ||= []
    TrendWorker.new.trend_list if @trend_list.blank?
  end

  def fetch_impression_count(m,d=0,t=:day,*args)
    opts = args.extract_options!
    n,o,ss,ee,dd = {}, (opts[:sum] ? '<=' : '='),nil,nil,nil
    
    key = ['impression_count',m,t] + opts.map{|k,v| "#{k}:#{v}"}
    (0..d).each do |i|
      case t
        when :all
          #
        when :minute, :minutes
          ss = (Time.now-(d-i).minutes).strftime('%Y-%m-%d %H:%i:00')
          ee = (Time.now-(d-i).minutes).strftime('%Y-%m-%d %H:%i:59')
          q = "created_at BETWEEN '%start' AND '%end'".gsub('%start', ss).gsub('%end', ee)
        when :hour, :hours
          ss = (Time.now-(d-i).hours).strftime('%Y-%m-%d %H:00:00')
          ee = (Time.now-(d-i).hours).strftime('%Y-%m-%d %H:59:59')
          q = "created_at BETWEEN '%start' AND '%end'".gsub('%start', ss).gsub('%end', ee)
        else
          # q = "DATE(created_at) #{o} ?"
          # dd = Date.today-(d-i).days
          ss = (Time.now-(d-i).days).strftime('%Y-%m-%d 00:00:00')
          ee = (Time.now-(d-i).days).strftime('%Y-%m-%d 23:59:59')
          q = "created_at BETWEEN '%start' AND '%end'".gsub('%start', ss).gsub('%end', ee)
      end

      n[(dd || ss || t).to_s] = cache((key + ['interval',(dd || ss || t).to_s]), expires_in: (i > 0 ? 3.hours : 5.minutes)) do
        obj = Impression.where(impressionable_type: m)
        obj = obj.where(q) unless q.blank?

        [:message, :view_name, :action_name, :impressionable_id].each do |w|
          obj = obj.where(w => opts[w]) if opts[w].present?
        end

        if opts[:filter].present?
          obj = obj.select(opts[:filter]).distinct.count
        else
          obj = obj.count(:impressionable_id)
        end

        obj
      end
    end
    n
  end

  def post_velocity_range
    @post_velocity_ranges ||= Rails.cache.read(['global','post_velocity_range'])
  end
  helper_method :post_velocity_range


end
