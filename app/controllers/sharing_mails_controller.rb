class SharingMailsController < ApplicationController

  layout 'mail_post'

  def new
    case params[:target_type]
      when 'Post'
        @target = Post.published.friendly.find(params[:target_id])
      when 'PostItem'
        @target = PostItem.friendly.find(params[:target_id])
      else
        @target = nil
    end
    raise ActiveRecord::RecordNotFound if @target.blank?

    @sharing_mail = SharingMail.new

    render :new
  end

  def create
    @sharing_mail = SharingMail.new(sharing_mail_parmas.merge({ user_agent: request.user_agent, ip_address: request.env['REMOTE_ADDR'] }))
    if @sharing_mail.save
      target = @sharing_mail.target
      url = target.is_a?(Post) ? url_for(target) : url_for(target.post)
      MailWorker.perform_async(:share_mail, @sharing_mail.id, url)
      impressionist(target, :email, action_name: :share, controller_name: target.class.to_s.downcase.pluralize)
    else
      render :new
    end
  end

  def preview
    @sharing_mail = SharingMail.new(sharing_mail_parmas)
    @target = @sharing_mail.target
  end


private

  def sharing_mail_parmas
    params.require(:sharing_mail).permit(:target_id, :target_type, :sender_name, :sender_email, :receiver_email, :message)
  end
  
end
