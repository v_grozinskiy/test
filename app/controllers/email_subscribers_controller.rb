class EmailSubscribersController < ApplicationController


  def new
    @email_subscriber = EmailSubscriber.new
  end

  def create
    @email_subscriber = EmailSubscriber.where(email_subscriber_params[:email]).first_or_create(email_subscriber) rescue nil
    respond_to do |format|
      format.html { redirect_back_or_default notice: "Thanks for subscribing. You'll begin receiving email updates soon." }
      format.json { render :nothing, status: 200 }
    end
  end


private
  
  def email_subscriber_params
    params.require(:email_subscriber).permit(:email, :first_name, :last_name)
  end

end
