class StaticPagesController < ApplicationController

  before_filter :load_top_trends
  before_filter :load_resource, only: [:show]
  before_filter :load_latest_posts, if: :request_is_html

  layout 'post'

  force_ssl if: :page_requires_ssl, only: [:show]


  def show
    @_static_page_options = {}
    respond_to do |format|
      format.html {
        begin
          send(@page.page_method)
        rescue ActiveRecord::RecordNotFound => err
          raise err
        rescue => err
          nil
        end if respond_to?(@page.page_method)

        @body_classes << 'static-page' << "page-#{@page.page.gsub(/\/|\-/m, '_').gsub(/("|')/m, '')}"
        render @page.file, @_static_page_options
      }
      format.any { render_not_found }
    end
  end

  def about
    users = [
      'shira',
      'damon',
      'jonathon'
    ]
    skip_users = [ # XOlator dev accounts
      'gleuch',
      'xolator'
    ]
    @staff = User.admins.where.not(login: skip_users).order("FIELD(login,#{users.reverse.map{|v| v.to_s.inspect}.join(',')}) DESC, name ASC")
  end

  def tubeathon
    @_hide_ads = true
    @_static_page_options[:layout] = false

    @countdown_videos = [
      {date: '2014-12-01', youtube_id: 'jbmDWfnj4jM', title: "TUBEATHON 2014 by Song A Day's JONATHAN MANN!"},
      {date: '2014-12-02', youtube_id: 'dbJIldtInVs', title: "TIPSY BARTENDER'S Holiday Drink Featuring TARYN SOUTHERN!"},
      {date: '2014-12-03', youtube_id: 'VMNkL1pUpvo', title: "ToyCollectorFun Opens 2014's HOTTEST Holiday Gifts"},
      {date: '2014-12-04', youtube_id: 'r0lj35nMDlA', title: "What YOUTUBERS Want For Christmas - Feat. Connor Franta, Rebecca Black & More!"},
      {date: '2014-12-05', youtube_id: '0cshwqpgW9I', title: "Top 5 INSPIRING Viral Videos Of 2014"},
      {date: '2014-12-06', youtube_id: 'aNsU1hgEfo8', title: "TONY HORTON'S Holiday Themed WORKOUT"},
      {date: '2014-12-07', youtube_id: 'qEMwoSZcjq0', title: "Holiday Tree Decorating Tips From AFabLife!"},
      {date: '2014-12-09', youtube_id: 'uD559X4SavY', title: "The Buried Life & Destorm Bring The Holidays To COVENANT HOUSE"},
      {date: '2014-12-10', youtube_id: 'Wy9qsHxeqZQ', title: "Holiday Carolers SURPRISE People In LA"},
    ].reverse
  end

  def escapetobonnaroo
    @_hide_ads = true
    @page_title = t('contest.escape.page_title')
    @page_meta_images = [view_context.image_url('contest/escape/share.jpg')]

    @on_the_road = [
      {title: 'Road Trip Day 1: Leaving LA', youtube_id: 'sP58NuR_dLQ'},
      {title: 'Road Trip Day 2: Bonnaroo Makeup Tips with Kandee Johnson and Glozell',  youtube_id: 'JfZd2jzBcG0'},
      {title: 'Road Trip Day 3: Dance Party!', youtube_id: '0h5bQzOp6EQ'},
    ].reverse
    @live_videos = [
      {title: 'Meghan Tonjes', youtube_id: '0xcx2cLmfPk', winner: true},
      {title: 'Bad Wolf',  youtube_id: 'yhJQaY-ufYU'},
      {title: 'David Choi',  youtube_id: 'TBzIrWB8agY'},
      {title: 'The Vibe',  youtube_id: 'SC0fJTWEcz8'},
      {title: 'Lauren Mayhew',  youtube_id: 'Y1Bisjz73Ko'},
    ]
    @_static_page_options[:layout] = 'layouts/contest/escape'
  end
  alias_method :escapetobonnaroo_rules, :escapetobonnaroo

  # Livestream page for Samsung + WT contest.
  def hope4children
    @_static_page_options[:layout] = false
    @_hide_ads = true
  end


protected

  def load_resource
    @page = StaticPage.new(params)
    raise ActiveRecord::RecordNotFound unless @page.exists?
  end

  def page_requires_ssl
    # ssl_configured? && ['tubeathon'].include?(@page.page_method.to_s)
  end

end