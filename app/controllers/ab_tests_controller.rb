class AbTestsController < ApplicationController

  before_filter :load_resource, only: [:finish]


  def finish
    finished('social_gates' => params[:goal], reset: false)
    render nothing: true, status: 200
  end


private

  def load_resource
    @ab_test = Split::Experiment.find(params[:id])
    raise ActiveRecord::RecordNotFound if @ab_test.blank? || !(@ab_test.goals.present? && @ab_test.goals.include?(params[:goal]))
  end

end
