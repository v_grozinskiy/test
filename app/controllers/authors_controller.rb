class AuthorsController < ApplicationController

  before_filter :load_top_trends
  before_filter :load_resource

  layout 'post'


  def show
    @_per_page = 10

    results = Proc.new {
      @posts = @author.posts.includes(:category).published.recent.paginate(page: @_page, per_page: @_per_page)
    }

    respond_to do |format|
      format.html { results.call }
      format.js { results.call }
      format.xml {
        @_per_page = 20
        results.call
      }
    end
  end


protected

  def load_resource
    @author = User.find(params[:id])
    raise ActiveRecord::RecordNotFound if @author.blank? || !@author.author?
  end

end
