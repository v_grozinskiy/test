module Admin
  class PostsController < BaseController

    newrelic_ignore_enduser
    before_filter :load_resource, only: [:show, :edit, :postition, :publish, :unpublish, :preview, :fetch_authors, :stats, :queue, :preview]
    before_filter :load_new_resource, only: [:new,:create]
    after_filter :delete_cache, only: [:update, :publish, :unpublish, :destroy, :preview]
    load_and_authorize_resource
    respond_to :html, :json

    def index
      # Editors can see all posts. Other users can see their own.
      obj = can?(:manage, Post) ? Post : current_user.posts
      
      case params[:filter]
        when 'drafts';  obj = obj.not_published
        when 'queued';  obj = obj.queued
        when 'all';     nil
        else;           obj = obj.published
      end

      # Drafts should filter by created_at, others by recent
      case params[:filter]
        when 'drafts';  obj = obj.created
        else;           obj = obj.recent
      end

      obj = obj.search(params[:q]) unless params[:q].blank?

      @posts = obj.paginate(page: @_page, per_page: 20)
    end

    def show
      if @post.published?
        @share_counts = Rails.cache.fetch(['admin', 'post', @post, 'share_counts'], expires_in: 5.minutes) do
          {
            facebook:   { name: t(:facebook),   data: @post.get_total_impression_count(action_name: :share, message: 'facebook') },
            twitter:    { name: t(:twitter),    data: @post.get_total_impression_count(action_name: :share, message: 'twitter') },
            pinterest:  { name: t(:pinterest),  data: @post.get_total_impression_count(action_name: :share, message: 'pinterest') },
            googleplus: { name: t(:google),     data: @post.get_total_impression_count(action_name: :share, message: 'gplus') },
            whatsapp:   { name: t(:whatsapp),   data: @post.get_total_impression_count(action_name: :share, message: 'whatsapp') },
            linkedin:   { name: t(:linked_in),  data: @post.get_total_impression_count(action_name: :share, message: 'linkedin') },
            stumbleupon:{ name: t(:stumbleupon),data: @post.get_total_impression_count(action_name: :share, message: 'stumbleupon') },
            reddit:     { name: t(:reddit),     data: @post.get_total_impression_count(action_name: :share, message: 'reddit') },
            email:      { name: t(:email),      data: @post.get_total_impression_count(action_name: :create, message: 'email') },
          }
        end

        @graph_stats = {
          views: {
            library:    { colors: ['bb0000','aad450']}
          },
          shares: {
            library:    { colors: ['3b5998','00aced','cb2027','dd4b39','34af23','007bb6','ff5700','eb4924','555555']}
          },
          comments: {
            library:    { colors: ['bb0000']}
          }
        }

        @post_views = @post.shows_count
        @post_unique_views = Rails.cache.fetch(['admin','post',@post,'unique_views'], expires_in: 1.minute) do
          @post.get_total_impression_count(action_name: :show, filter: :session_hash)
        end
        @post_comments = @post.comments_count
      end
    end

    def new
      render :edit # hack
    end

    def create
      results = Proc.new{
        @post.submit ? t('.post_save_success') : false
      }

      respond_to do |format|
        if status = results.call
          format.html { redirect_to edit_admin_post_url(@post), notice: status }
          format.json { render nothing: true }
        else
          format.html { flash.now[:error] = t('.post_save_error'); render :edit }
          format.json { render json: { error: @post.errors } }
        end
      end
    end

    def update
      results = Proc.new{
        status = false

        if @post.update(post_params)
          if params[:publish]
            if @post.publish
              status = t('.post_publish_success') 
            end
          elsif params[:commit] && params[:commit].match(/Arrange/i)
            status = true
          elsif @post.submit
            status = t('.post_save_success')
          end
        end
        
        status
      }

      respond_to do |format|
        if status = results.call
          format.html { redirect_to edit_admin_post_url(@post), notice: status }
          format.json { render nothing: true }
        else
          format.html { flash.now[:error] = t('.post_save_error'); render :edit }
          format.json { render json: { error: @post.errors } }
        end
      end
    end

    def publish
      if @post.publish
        redirect_to :back, notice: t('.post_publish_success')
      else
        flash.now[:error] = t('.post_publish_error')
        render :edit
      end
    end

    def unpublish
      if @post.unpublish
        redirect_to :back, notice: t('.post_unpublish_success')
      else
        flash.now[:error] = t('.post_unpublish_error')
        render :edit
      end
    end

    def destroy
      @post.destroy
      redirect_to admin_posts_url, notice: t('.notice')
    end

    def fetch_authors
      @post_authors = @post.post_authors.includes(:user)
    end

    def stats
      @graph_stats = Rails.cache.fetch(['admin','post', @post, 'graph_stats',params[:filter]], expires_in: 10.minutes) do
        case params[:filter]
          when 'views'
            [
              { name: t(:views),        data: fetch_impression_count('Post', 72, :hour, sum: true, impressionable_id: @post.id, action_name: :show) },
              { name: t(:unique_views), data: fetch_impression_count('Post', 72, :hour, sum: true, impressionable_id: @post.id, action_name: :show, filter: :session_hash) }
            ]
          when 'shares'
            [
              { name: t(:facebook),     data: fetch_impression_count('Post', 72, :hour, impressionable_id: @post.id, action_name: :share, message: 'facebook') },
              { name: t(:twitter),      data: fetch_impression_count('Post', 72, :hour, impressionable_id: @post.id, action_name: :share, message: 'twitter') },
              { name: t(:pinterest),    data: fetch_impression_count('Post', 72, :hour, impressionable_id: @post.id, action_name: :share, message: 'pinterest') },
              { name: t(:google),       data: fetch_impression_count('Post', 72, :hour, impressionable_id: @post.id, action_name: :share, message: 'gplus') },
              { name: t(:whatsapp),     data: fetch_impression_count('Post', 72, :hour, impressionable_id: @post.id, action_name: :share, message: 'whatsapp') },
              { name: t(:linked_in),    data: fetch_impression_count('Post', 72, :hour, impressionable_id: @post.id, action_name: :share, message: 'linkedin') },
              { name: t(:reddit),       data: fetch_impression_count('Post', 72, :hour, impressionable_id: @post.id, action_name: :share, message: 'reddit') },
              { name: t(:stumbleupon),  data: fetch_impression_count('Post', 72, :hour, impressionable_id: @post.id, action_name: :share, message: 'stumbleupon') },
              { name: t(:email),        data: fetch_impression_count('Post', 72, :hour, impressionable_id: @post.id, action_name: :create, message: 'email') }
            ]
          when 'comments'
            [
              { name: t(:comments),     data: fetch_impression_count('Post', 72, :hour, impressionable_id: @post.id, action_name: :comment) }
            ]
        end
      end
      render json: @graph_stats
    end

    def preview
      respond_to do |format|
        format.html {
          @_post_preview, @_hide_ads = true, true
          @post.assign_attributes(post_params || {})
          unless @post.changed.blank?
            flash.now[:danger] = t('.unsaved')
          else
            flash.now[:warning] = t('.preview')
          end
          render 'posts/show', layout: 'post'
        }
      end
    end

  private

    def load_resource
      @_require_social_js = true
      @post = Post.find(params[:id])
    end

    def load_new_resource
      @_require_social_js = true
      @post = current_user.created_posts.build(post_params)
    end

    def post_params
      params.require(:post).permit(:slug, :title, :description, :promo_title, 
        :promo_description, :promo_image, :category_id, :style, :format,
        :visible_homepage, :visible_stats, :nsfw, :wt_original_video, :tag_list, :allow_comments, :google_news,
        post_authors_attributes: [:user_id, :position, :id, :_destroy],
        post_items_attributes: [:id, :post_id, :style, :media, :title, :crop_x, :crop_y, :crop_w, :crop_h, 
          :image, :text, :source, :via, :bulleted, :visible, :position, :image_url, :duration, :_destroy], 
          recommended_post_ids: []) rescue nil
    end

    def delete_cache
      Post.delay_for(5.seconds).delete_global_cache
      Post.delay_for(5.seconds).delete_homepage_cache
      Category.delay_for(5.seconds).delete_categories_cache(@post.category.self_and_ancestors.pluck(:id))
    end

  end
end
