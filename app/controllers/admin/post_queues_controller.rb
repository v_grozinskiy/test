module Admin
  class PostQueuesController < BaseController

    newrelic_ignore_enduser
    load_and_authorize_resource

    def create
      @post_queue = PostQueue.new(post_queue_params)
      @post_queue.save
    end

    def update
      @post_queue = PostQueue.find(params[:id])
      @post_queue.update(post_queue_params)
      render :create
    end

    private

    def post_queue_params
      params.require(:post_queue).permit(:post_id, :publish_at)
    end
  end
end
