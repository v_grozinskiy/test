module Admin
  class BadgesController < BaseController

    newrelic_ignore_enduser
    load_and_authorize_resource

    def index
      @badges = Badge.paginate(page: @_page, per_page: 20)
    end

    def new
      @badge = Badge.new
    end

    def create
      @badge = Badge.new(badge_params)

      if @badge.save
        redirect_to admin_badge_url(@badge), notice: "Badge was successfully created!"
      else
        render :new
      end
    end

    def update
      if @badge.update(badge_params)
        redirect_to admin_badge_url(@badge), notice: "Badge was successfully updated!"
      else
        render :edit
      end
    end

    def destroy
      @badge.destroy
      redirect_to admin_badges_url, notice: "Badge was successfully deleted!"
    end


    private

    def badge_params
      params.require(:badge).permit(:name, :description, :icon, :nsfw, :active)
    end
  end
end
