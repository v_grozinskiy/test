module Admin
  class SettingsController < BaseController

    newrelic_ignore_enduser

    def index
      authorize! :index, :settings
      @settings = Setting.get_all
    end

    def create
      authorize! :create, :settings

      if params[:setting][:key].present? && !params[:setting][:key].blank?
        val = params[:setting][:value]
        if (val_array = params[:setting][:value].split(',')).any? && val_array.length > 1
          val = val_array.reject(&:blank?).map(&:strip).map{|v| v.is_bool? ? v.to_bool : v}
        elsif val.is_bool?
          val = val.to_bool
        end
        Setting.send("#{params[:setting][:key]}=", val)
        notice = t('.notice')
        expire_fragment('featured_posts') if params[:setting][:key] == 'recommended_post_ids'
      end
      redirect_to :back, notice: notice
    end

    def destroy
      authorize! :destroy, :settings
      Setting.destroy params[:id]
      redirect_to admin_settings_url, notice: t('.notice')
    end
  end
end

