module Admin
  class CategoriesController < BaseController

    newrelic_ignore_enduser
    load_and_authorize_resource
    before_filter :load_resources, only: [:index]
    respond_to :html, :json


    def index
    end

    def show
    end

    def new
      @category = Category.new
    end

    def create
      @category = Category.new(category_params)

      if @category.save
        redirect_to admin_categories_url, notice: "Category was successfully created!"
      else
        render :new
      end
    end

    def edit
    end

    def update
      if @category.update(category_params)
        redirect_to admin_categories_url, notice: "Category was successfully updated!"
      else
        render :edit
      end
    end

    def destroy
      @category.destroy
      redirect_to admin_categories_url, notice: "Category was successfully deleted!"
    end


  private

    def category_params
      params.require(:category).permit(:name, :description, :parent_id, :banner_image, :promotional, :nsfw, :active)
    end

    def load_resources
      @categories = []
      Category.roots.each do |root|
        @categories += root.class.associate_parents(root.self_and_descendants).compact
      end
    end

  end
end
