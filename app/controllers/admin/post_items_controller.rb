module Admin
  class PostItemsController < BaseController

    newrelic_ignore_enduser
    load_and_authorize_resource
    skip_before_action :verify_authenticity_token, only: [:oembed]
    respond_to :js

    def destroy
      @post_item.destroy
    end

    # Return oemed result
    def oembed
      url = Sanitize.fragment(params[:url], Sanitize::Config.merge(Sanitize::Config::BASIC,
        remove_contents: true, 
        elements: %w(iframe), 
        attributes: {all: %w(src)}
      )).gsub(/\<iframe\ [^>]*src="([^"]+)"[^>]*>(<\/iframe>)?/i, '\1')

      oembed_url = case params[:url]
        when /funnyordie\.com/
          id = url.gsub(/(http(s)?\:\/\/(www\.)?funnyordie\.com\/(videos|embed)\/)([a-z0-9]+)/i, '\5')
          'http://www.funnyordie.com/oembed.json?url=http://www.funnyordie.com/videos/' + id
        when /kickstarter\.com/
          id = url.gsub(/(http(s)?\:\/\/(www\.)?kickstarter\.com\/projects\/)(\d+|([a-z0-9\-]+\/[a-z0-9\-]+))/i, '\4')
          'https://www.kickstarter.com/services/oembed?url=http://www.kickstarter.com/projects/' + id
        when /collegehumor\.com/
          id = url.gsub(/(http(s)?\:\/\/(www\.)?collegehumor\.com\/(video|e)\/)(\d+)/i, '\5')
          'http://www.collegehumor.com/oembed.json?url=http://www.collegehumor.com/video/' + id
        else
          nil
      end

      Rails.logger.info oembed_url
      result = RestClient.get(oembed_url) unless oembed_url.blank?
      result ||= {error: true}
      
      json = JSON.parse(result)
      json['html'] = Sanitize.fragment(json['html'], Sanitize::Config.merge(Sanitize::Config::BASIC,
        remove_contents: true, 
        elements: %w(iframe embed object param),
        attributes: {
          all: %w(src type width height name value id allowfullscreen allowfullscreen webkitallowfullscreen mozallowfullscreen)
        }
      )) rescue json['html'] unless json['html'].blank?

      render json: json, callback: params[:callback]
    end
  end
end
