module Admin
  class LayoutsController < BaseController

    newrelic_ignore_enduser

    def index
      authorize! :index, :layouts
      @recommended_posts = Post.recommended
      @posts = Post.unrecommended_all.published.recent.limit(75)
    end

    def create
      authorize! :create, :layouts

      Setting.recommended_post_ids = params[:post_ids]
      Post.where(id: Setting.recommended_post_ids).find_each do |post|
        post.update_column(:visible_homepage, true)
      end
      expire_fragment('featured_posts')
      
      Post.delete_homepage_cache
      # Homepage popular will update shortly...

      redirect_to admin_layouts_url, notice: t('.notice')
    end
  end
end
