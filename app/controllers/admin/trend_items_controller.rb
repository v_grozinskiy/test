module Admin
  class TrendItemsController < BaseController

    newrelic_ignore_enduser
    after_filter :delete_cache, only: [:update, :publish, :unpublish]
    load_and_authorize_resource
    before_filter :load_trend_list
    before_filter :load_trend_list_item
    before_filter :load_trend_item, only: [:edit, :update]
    respond_to :html, :json

    def edit
      render :edit
    end

    def update
      respond_to do |format|
        if @trend_item.update(trend_item_params)
          format.html { redirect_to admin_trends_url, notice: "Trend item was updated!" }
          format.json { render :nothing }
        else
          format.html {
            flash.now[:notice] = 'There was an error saving this trend item.'
            render :edit
          }
          format.json { render error: @trend_item.errors }
        end
      end
    end

    def publish
      @trend_list_item.update_attribute(:active, true)
      redirect_to admin_trends_url, notice: "Trend item was made visible!"
    end

    def unpublish
      @trend_list_item.update_attribute(:active, false)
      redirect_to admin_trends_url, notice: "Trend item was hidden!"
    end


  protected

    def load_trend_list
      @trend_list = TrendList.find(params[:trend_id])
      raise ActiveRecord::RecordNotFound if @trend_list.blank?
    end

    def load_trend_list_item
      @trend_list_item = @trend_list.list_items.find(params[:list_id])
      raise ActiveRecord::RecordNotFound if @trend_list_item.blank?
    end

    def load_trend_item
      @trend_item = @trend_list_item.item
      raise ActiveRecord::RecordNotFound if @trend_item.blank? || @trend_item.id.to_s != params[:id]
    end

    def trend_item_params
      params.require(:trend_item).permit(:display_keyword, :source_url, :image, :post_id)
    end

    def delete_cache
      TrendList.regenerate_trend_cache
    end
  end
end
