module Admin
  class UsersController < BaseController

    newrelic_ignore_enduser
    load_and_authorize_resource except: [:create, :search]
    respond_to :html, :json


    def index
      obj = User

      unless params[:q].blank?
        obj = obj.search(params[:q]).order('name desc')
      else
        obj = obj.order('created_at desc')
      end

      case params[:filter]
        when 'authors'
          obj = obj.authors
        when 'community'
          obj = obj.community
      end

      @users = obj.paginate(page: @_page, per_page: 20)
    end

    def create
      password = SecureRandom.hex
      @user = User.new(user_params.merge(password: password, password_confirmation: password))

      if @user.save
        redirect_to admin_user_url(@user), notice: t('.notice', password: password)
      else
        render :new
      end
    end

    def update
      if @user.update(user_params)
        redirect_to admin_user_url(@user), notice: t('.notice')
      else
        render :edit
      end
    end

    def destroy
      @user.destroy
      redirect_to admin_users_url, notice: t('.notice')
    end

    def search
      @users = User.where("lower(first_name) LIKE ? OR lower(last_name) LIKE ? OR lower(login) LIKE ?", "%#{params[:q].downcase}%", "%#{params[:q].downcase}%", "%#{params[:q].downcase}%").limit(params[:page_limit])
    end


  private

    def user_params
      params[:user_tmp] ||= params[:user]
      params.require(:user)
        .permit(:email, :login, :first_name, :last_name, :avatar, :title, :byline, 
          :twitter_handle, :facebook_handle, :facebook_id, :twitter_id, :password, :password_confirmation)
        .merge(roles: ((params[:user_tmp][:admin_roles] || []) + (params[:user_tmp][:access_roles] || [])))
    end
  end
end
