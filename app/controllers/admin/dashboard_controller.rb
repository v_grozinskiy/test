module Admin
  class DashboardController < BaseController

    newrelic_ignore_enduser

    def index
      if can?(:stats, Post)
        @top_posts = Rails.cache.read(['admin','dashboard','top_posts']) || {}
        @graph_stats = Rails.cache.read(['admin', 'dashboard', 'graph_stats']) || {}
      end

      render :index
    end

    def stats

    end
  end
end
