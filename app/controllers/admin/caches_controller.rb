module Admin
  class CachesController < BaseController

    newrelic_ignore_enduser
    before_filter :load_resources
    before_filter :load_resource, only: [:update]

    def index
      authorize! :index, :caches
    end

    def update
      authorize! :create, :caches

      status = case @cache
        when :homepage
          Post.delete_homepage_cache
        when :trend_list
          TrendList.regenerate_trend_cache
        when :rss_feed
          Rails.cache.delete_matched('*/xml*')
        when :static_pages
          Rails.cache.delete_matched('*/static_pages/*')
        else
          nil
      end

      name = t(:name, scope: [:admin, :caches, :names, @cache.to_s], default: @cache.to_s.humanize.capitalize)

      if status
        flash[:success] = t('.success', name: name)
      else
        flash[:danger] = t('.error', name: name)
      end

      redirect_back_or_default admin_caches_url
    end

  private

    def load_resources
      @caches = [
        :homepage,
        :trend_list,
        :rss_feed,
        :static_pages,
      ]
    end

    def load_resource
      @cache = params[:id].to_sym
      raise ActiveRecord::RecordNotFound unless @caches.include?(@cache)
    end
  end
end

