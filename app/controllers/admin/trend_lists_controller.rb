module Admin
  class TrendListsController < BaseController

    newrelic_ignore_enduser
    load_and_authorize_resource
    before_filter :load_resource, only: [:publish]

    respond_to :html, :json

    def index
      @draft_list = TrendList.draft.recent.first
      @current_list = TrendList.published.recent.first
    end

    def publish
      if @trend_list.publish
        TrendList.regenerate_trend_cache
        redirect_to admin_trends_url, notice: "Trend list was successfully published!"
      else
        redirect_to admin_trends_url, error: "Trend list could not be published."
      end
    end


  protected

    def load_resource
      @trend_list = TrendList.find(params[:id])
    end
  end
end
