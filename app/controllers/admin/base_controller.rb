module Admin
  class BaseController < ApplicationController

    before_filter do
      resource = controller_name.singularize.to_sym
      method = "#{resource}_params"
      params[resource] &&= send(method) if respond_to?(method, true)
    end

    force_ssl if: lambda {
      # There are a few routes where non-ssl is ok
      case [controller_name, action_name].join('_')
        when 'posts_preview'
          false
        else
          ssl_configured?
      end
    }

    newrelic_ignore_enduser
    before_filter :authenticate_admin_user!

    layout 'admin'


    def current_ability
      @current_ability ||= AdminAbility.new(current_user)
    end

  end
end