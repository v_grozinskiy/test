module Admin
  class SearchTermsController < BaseController

    newrelic_ignore_enduser
    load_and_authorize_resource

    def index
      @search_terms = SearchTerm.includes(:impressions).recent.paginate(page: @_page, per_page: 20)
    end

    def show
      @impressions = @search_term.impressions.order(updated_at: :desc).paginate(page: @_page, per_page: 20)
    end
  end
end
