class CategoriesController < ApplicationController

  before_filter :load_latest_paginated_posts, if: :request_is_html
  before_filter :load_latest_wt_video_posts, only: [:index], if: :request_is_html
  before_filter :load_top_trends, if: :request_is_html


  def index
    @_per_page = request.format.xml? ? 20 : 6
    #@_offset = [(@_per_page * (@_page-1)) - 3, 0].max if %w(footer).include?(params[:place]) && request.xhr?
    #@_offset ||= nil

    results = Proc.new{|sorted|
      obj = Post.published.includes(:category).recent
      obj = obj.includes(:post_items).includes(:authors) if request.format == :xml

      # Normal search query for post
      if params[:q].present?
        obj = obj.search(params[:q])
        unless params[:q].blank?
          @search_term = SearchTerm.find_or_create_by term: params[:q].downcase
          impressionist @search_term, nil, unique: [:session_hash] rescue nil # sometimes bots spam this
        end

      # HTML / JS removes recommended items from the list
      elsif sorted
        obj = obj.unrecommended

      # XML/RSS does not rank recommended posts
      else
        obj = obj.visible_on_homepage
      end

      @posts = obj.paginate(page: @_page, per_page: @_per_page)#, offset: @_offset)
    }

    respond_to do |format|
      format.html {
        results.call(true)

        # Homepage functionality, if not a search
        if !params[:search] && @_page == 1
          load_featured_homepage_posts
          @popular_posts = Rails.cache.read(['homepage', 'popular_posts']) || []
          load_latest_posts(unrecommended: true, not: @popular_posts)

          render :home

        # Search/post pagination functionality
        else
          render :index
        end
      }
      format.js   { results.call(true) }
      format.xml  { results.call }
    end
  end

  # Individual category pages
  def show
    @category = Category.find(params[:id])
    raise ActiveRecord::RecordNotFound if @category.blank?

    @_per_page = 12

    results = Proc.new {
      @posts = Post.nested_category(@category).includes(:category).published.recent.paginate(page: @_page, per_page: @_per_page)
    }

    respond_to do |format|
      format.html { results.call }
      format.js   { results.call }
      format.xml  {
        @_per_page = 20
        results.call
      }
    end
  end


private
  
  def load_featured_homepage_posts
    @featured_posts = cache(['homepage','featured_homepage_posts'], expires_in: 5.minutes) do
      Post.published.includes(:category).recommended.weighted_homepage.all.to_a
    end
  end

end
