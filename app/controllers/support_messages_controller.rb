class SupportMessagesController < ApplicationController

  before_filter :load_top_trends
  before_filter :load_new_resource
  before_filter :load_latest_posts, if: :request_is_html

  layout 'post'


  def index
    render :index
  end

  def new
    render :index
  end

  def create
    @support_message.assign_attributes(support_message_params)
    if @support_message.save
      flash[:warning] = t('.success')
      redirect_to support_messages_url
    else
      render :index
    end
  end


private

  def load_new_resource
    @support_message = SupportMessage.new(referrer: request.referrer)
  end

  def support_message_params
    params.require(:support_message).permit(:name, :email, :phone, :message, :referrer).merge(
      user_id:    (current_user ? current_user.id : nil),
      ip_address: request.remote_ip,
      browser:    request.user_agent,
      session_id: session[:id]
    )
  end

end
