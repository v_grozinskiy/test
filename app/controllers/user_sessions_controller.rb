class UserSessionsController < ApplicationController

  force_ssl if: :ssl_configured?

  layout 'admin'


  def new
    @user_session = UserSession.new
  end
  
  def create
    @user_session = UserSession.new(params[:user_session])
    if @user_session.save
      redirect_to after_sign_in_url, notice: t('.welcome_message', user_name: @user_session.user.name)
    else
      flash[:danger] = t('.invalid_login')
      render :new
    end
  end
  
  def destroy
    @user_session = UserSession.find
    @user_session.destroy
    redirect_to root_url#, notice: t('.logout_message')
  end

end
