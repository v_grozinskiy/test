class PostQueueWorker
  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(post_id)
    post = Post.find(post_id)
    post.publish if post
    PostQueue.where(post_id: post_id).destroy_all
  end
end