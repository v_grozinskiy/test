class MailWorker
  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform(mail_type, id, url=nil)
    case mail_type.to_sym
      when :share_mail
        @sharing_mail = SharingMail.find(id)
        @sharing_mail.receiver_email.split(',').each_with_index do |address, i|
          address = address.strip
          UserMailer.share_mail(@sharing_mail, address, url).deliver unless address.blank?
        end
    end
  end
end