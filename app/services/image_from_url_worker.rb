class ImageFromUrlWorker
  include Sidekiq::Worker

  sidekiq_options queue: "high"


  def perform(klass,id,img_attr=:thumbnail,url=:thumbnail_url)
    begin
      obj = klass.constantize.find(id) rescue nil
      url = obj.send(url) 
      return false if url.nil? || !url.match(/^(ftp|http(s)?)/i)
      uri = Addressable::URI.parse(url)

      Timeout::timeout(30) do # 30 seconds
        io = open(uri, read_timeout: 30, "User-Agent" => WT_USER_AGENT, allow_redirections: :all)
        io.class_eval { attr_accessor :original_filename }
        raise "invalid content-type" unless io.content_type.match(/^image\//i)
        io.original_filename = File.basename(uri.path || obj.id || Time.now.to_i)
        io.original_filename << ".#{io.content_type.gsub(/^(image\/)(.*)$/i, '\2')}" unless io.original_filename.match(/\.(jpg|jpeg|png|tif|gif)$/)
        obj.send("#{img_attr}=", io)
        obj.save
      end
    rescue OpenURI::HTTPError => err
      puts "Error 3: #{err}"
      # obj.errors.add(img_attr, "unable to retrieve from URL [e:1]")
      true
    rescue Timeout::Error => err
      puts "Error 2: #{err}"
      # obj.errors.add(img_attr, "unable to retrieve from URL [e:2]")
      false
    rescue => err
      puts "Error 1: #{err}"
      # obj.errors.add(img_attr, "unable to retrieve from URL (#{err})")
      false
    end
  end

end