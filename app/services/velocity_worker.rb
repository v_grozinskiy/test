class VelocityWorker
  include Sidekiq::Worker
  sidekiq_options queue: "stats"

  def perform(action=nil,again=nil,*args)
    send action.to_s
    VelocityWorker.perform_at(again.from_now, action, again) unless again.blank?
  end

  # ---------------------------------------------------------------------------

  def self.start
    perform_async(:post_velocity_range, 5.minutes)
    perform_async(:per_5_min, 5.minutes)
    perform_async(:per_15_min, 15.minutes)
    perform_async(:per_1_hour, 1.hour)
    perform_async(:per_3_hours, 3.hour)
    perform_async(:per_1_day, 1.day)
  end

  # ---------------------------------------------------------------------------

  def post_velocity_range
    Rails.cache.fetch(['global','post_velocity_range'], force: true) do
      r = Time.now-7.days
      post = Post.published.where('published_at > ?', r).where('shows_velocity_score > 0')

      obj = {
        views: {
          ct: post.count, 
          avg: post.average(:shows_velocity_score), 
          # min: post.minimum(:shows_velocity_score), 
          max: post.maximum(:shows_velocity_score)
        },
        # shares: {
        #   ct: Post.published.where('shares_velocity_score > 0').where('published_at > ?', r).count, 
        #   avg: Post.published.where('published_at > ?', r).where('shares_velocity_score > 0').average(:shares_velocity_score), 
        #   # min: Post.published.where('published_at > ?', r).minimum(:shares_velocity_score), 
        #   max: Post.published.where('published_at > ?', r).maximum(:shares_velocity_score)
        # },
        # comments: {
        #   ct: Post.published.where('comments_velocity_score > 0').where('published_at > ?', r).count, 
        #   avg: Post.published.where('published_at > ?', r).where('comments_velocity_score > 0').average(:comments_velocity_score), 
        #   # min: Post.published.where('published_at > ?', r).minimum(:comments_velocity_score), 
        #   max: Post.published.where('published_at > ?', r).maximum(:comments_velocity_score)
        # }
      }

      obj.each{|k,v| obj[k][:intv] = WilsonScore.rating_interval(v[:avg], v[:ct], 0..v[:max], confidence: 0.90) rescue 0..5 }
      obj
    end
  end

  # Get delta and score of posts in last 1 day
  def per_5_min
    Post.published.where('published_at > ?', (Time.now - 24.hours)).find_each { |p| update_post(p) }
  end

  # Get delta and score of posts in last 2 days
  def per_15_min
    Post.published.where(published_at: ((Time.now - 48.hours)..(Time.now - 24.hours))).find_each { |p| update_post(p) }
  end

  # Get delta and score of posts in last 3 days
  def per_1_hour
    Post.published.where(published_at: ((Time.now - 72.hours)..(Time.now - 48.hours))).find_each { |p| update_post(p) }
  end

  # Get delta and score of posts in last 1 week
  def per_3_hours
    Post.published.where(published_at: ((Time.now - 168.hours)..(Time.now - 72.hours))).find_each { |p| update_post(p) }
  end

  # "Get delta and score of all posts"
  def per_1_day
    Post.published.find_each { |p| update_post(p) }
  end

  # ---------------------------------------------------------------------------

  def update_post(p)
    tmp = {}
    [:shows, :comments, :shares].each do |v|
      tmp[(v.to_s + '_velocity_delta').to_sym] = p.velocity_delta(v, 1.day)
      tmp[(v.to_s + '_velocity_score').to_sym] = p.velocity_score(v, 1.day)
    end

    unless p.update_columns(tmp)
      logger.info "Error: " << p.errors.inspect
    end
  end

end