require 'render_anywhere'

class CategoryWorker
  include RenderAnywhere
  include Sidekiq::Worker

  sidekiq_options queue: "stats"

  def perform(action=nil,again=nil)
    send action.to_s
    CategoryWorker.perform_at(again.from_now, action, again) unless again.blank?
  end

  # ---------------------------------------------------------------------------

  def self.start; perform_async(:dropdowns, 5.minutes); end

  # ---------------------------------------------------------------------------

  # Generate the main category dropdown section
  def dropdowns
    nav = ['video','trending-now','originals','funny','inspiring','music','pop','news']
    categories = Category.where(slug: nav).order("FIELD(categories.slug,#{nav.map{|v| v.inspect}.join(',')})")

    Rails.cache.fetch(['categories','list'], force: true) do
      render(partial: 'categories/nav_category', layout: false, collection: categories)
    end

    Rails.cache.fetch(['categories','list','dropdown'], force: true) do
      render(partial: 'categories/category', layout: false, collection: categories)
    end
  end

end