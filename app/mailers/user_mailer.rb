class UserMailer < ActionMailer::Base

  default from: "What's Trending <no-reply@whatstrending.com>"
  default to: "dev@xolator.com"

  layout 'layouts/mail'
  helper MailerHelper


  def password_reset_instructions(user)
    @user = user

    address = @user.email
    address = 'dev@xolator.com' unless Rails.env.production?

    mail(to: @user.email, subject: "Password Reset Instructions")
  end

  def share_mail(sharing_mail, address, url)
    @sharing_mail = sharing_mail
    @target = @sharing_mail.target

    @nice_url = url.gsub(/^http(s)?\:\/\/(www\.|app\.[A-Z0-9]+\.)?/i, '')

    address = 'dev@xolator.com' unless Rails.env.production?

    uri = URI.parse(url)
    uri.query = [uri.query, 'via=email',"tid=#{CGI.escape Base64.encode64(@sharing_mail.id.to_s)}","eid=#{CGI.escape Digest::MD5.hexdigest(address)}"].compact.join('&')
    @url = uri.to_s

    mail(to: address, subject: "#{@sharing_mail.sender_name} wants to share this with you.")
  end

  def admin_support_message(support_message)
    @support_message = support_message

    to = []
    if Rails.env.production?
      to << 'Jonathan Harris <jonathan@whatstrending.com>'
      to << 'Damon Berger <damon@whatstrending.com>'
    else
      to << 'XOlator <dev@xolator.com>'
    end
    
    mail(to: to.join(', '), from: 'no-reply@whatstrending.com', reply_to: support_message.email, subject: "[WT.com] Contact Message (Id: #{support_message.uuid})")
  end

end
