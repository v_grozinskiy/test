# imp = Impression.order('created_at DESC').first
# id = imp.id
# puts "","IMP ID: #{id}","-"*80
# (0..1000000).each do |i|
#   p = Post.published.where('published_at > ?', Time.now-3.months).order('RAND()').first
#   Impression.create(
#     impressionable_type: 'Post',
#     impressionable_id: p.id,
#     :controller_name => 'posts',
#     :action_name => 'show',
#     :user_id => nil,
#     :request_hash => 'abc',
#     :session_hash => "".random(32),
#     :ip_address => '127.0.0.1',
#     :referrer => nil,
#     created_at: rand(1.year.ago..Time.now)
#   )
#   puts i if i % 1000 == 0
# end

# Impression.where('id > ?', id).find_each do |i|
#   puts i.inspect
#   puts i.update_column(:created_at, rand(1.year.ago..Time.now)).inspect
#   i.reload
#   puts i.inspect
#   break
# end

# 50.times do
#   first_name = Faker::Name.first_name
#   last_name = Faker::Name.last_name
#   name = "#{first_name} #{last_name}"
# 
#   User.create(
#     first_name: first_name,
#     last_name: last_name,
#     name: name, 
#     password: "password", 
#     password_confirmation: "password", 
#     email: Faker::Internet.email, 
#     login: name.gsub(/\W+/, '').underscore,
#     avatar: File.open(Rails.root.join("db", "sample.jpg"), "r")
#     )
# end

%w(funny inspiring viral news pop music shows).each do |name|
  Category.where(name: name).first_or_create{|n| n.description = Faker::Lorem.sentence}
end

# 20.times do
#   Tag.create(name: Faker::Lorem.word)
# end

puts "","Run posts..."
10.times do |i|
  puts "...adding ##{i+1}"

  categories = Category.all
  users = User.all
  tags = Tag.all

  begin
    t = rand(1.month.ago..Time.now)
    post = Post.new(
      title: Faker::Lorem.word,
      description: Faker::Lorem.sentence,
      promo_title: Faker::Lorem.word,
      promo_description: Faker::Lorem.sentence,
      promo_image_url: "http://lorempixel.com/1200/800/",
      style: Faker::Lorem.word,
      format: Post.formats.keys.sample,
      created_at: t,
      published_at: t,
      state: 'published',
      category: categories.sample, 
      creator: users.sample
    )

    (1+rand(2)).times do |i|
      item = post.post_items.build(
        style: PostItem::styles.keys.sample,
        title: Faker::Lorem.word,
        text: Faker::Lorem.word,
        image_url: "http://lorempixel.com/1200/800/",
        media: '<iframe src="//player.vimeo.com/video/84880847" width="500" height="208" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="http://vimeo.com/84880847">Ace Reporter - Stick To</a> from <a href="http://vimeo.com/petersimonite">peter simonite</a> on <a href="https://vimeo.com">Vimeo</a>.</p>',
        source: Faker::Lorem.word,
        via: Faker::Lorem.word,
        position: i
      )
    end

    post.save

    # hacky hack ugh.
    post.authors << User.authors.order('rand()').first
    post.tags << [tags.sample]
    post.save

    # Seed with fake views
    (0..rand(200)+100).each do |i|
      Impression.create(impressionable_type: 'Post', impressionable_id: post.id, controller_name: 'posts', action_name: 'show', user_id: nil, request_hash:'abc', session_hash: "".random(32), ip_address: '127.0.0.1', referrer: nil, created_at: rand(1.month.ago..Time.now))
    end

  rescue => err
    puts "ERR: #{err}"
  end
end

puts "","Run workers...","...impresisons"
PostWorker.new.impressions
PostWorker.new.all_impressions
puts "...velocity range"
VelocityWorker.new.post_velocity_range
VelocityWorker.new.per_5_min
VelocityWorker.new.per_15_min
VelocityWorker.new.per_1_hour
VelocityWorker.new.per_3_hours
# VelocityWorker.new.per_1_day
puts "...popularity"
PostWorker.new.popularity
puts "...dropdowns"
CategoryWorker.new.dropdowns




# end
# exit
# # ----
# 
# 
# TrendList.draft.each{|v| v.destroy }
# list = TrendList.create(list_type: :global, total_score: 100, state: 'published')
# posts = Post.all
# 50.times do |i|
#   v = list.list_items.create(
#     rank: i+1,
#     rank_delta_last_hour: rand(20)-10,
#     rank_delta_last_day: rand(20)-10,
#     avg_score: rand,
#     score: (rand(1000) / 100)
#   )
# 
#   k = TrendItem.create(
#     post: posts.sample,
#     keyword_uuid: rand(10000000),
#     keyword: Faker::Lorem.word,
#     display_keyword: Faker::Lorem.word,
#     format: :video,
#     grouping: :you_tube,
#     active: true,
#     image: File.open(Rails.root.join("db", "sample.jpg"), "r")
#   )
#   k.list_items << v
#   k.save
# end
