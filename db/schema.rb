# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141216214144) do

  create_table "badges", force: true do |t|
    t.string   "slug",                                               null: false
    t.string   "name",                                               null: false
    t.text     "description",       limit: 16777215
    t.string   "icon_file_name"
    t.string   "icon_content_type"
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.integer  "posts_count",                        default: 0
    t.boolean  "nsfw",                               default: false
    t.boolean  "active",                             default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "badges", ["slug"], name: "index_badges_on_slug", using: :btree

  create_table "categories", force: true do |t|
    t.string   "slug",                                                       null: false
    t.string   "name",                                                       null: false
    t.text     "description",               limit: 16777215
    t.string   "banner_image_file_name"
    t.string   "banner_image_content_type"
    t.integer  "banner_image_file_size"
    t.datetime "banner_image_updated_at"
    t.integer  "posts_count",                                default: 0
    t.boolean  "promotional",                                default: false
    t.boolean  "nsfw",                                       default: false
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.boolean  "active",                                     default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "categories", ["slug"], name: "index_categories_on_slug", using: :btree

  create_table "comments", force: true do |t|
    t.string   "uuid"
    t.integer  "post_id"
    t.integer  "user_id"
    t.integer  "status",            default: 0
    t.boolean  "nsfw",              default: false
    t.text     "message"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.string   "request_ip"
    t.string   "request_useragent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["lft"], name: "index_comments_on_lft", using: :btree
  add_index "comments", ["parent_id"], name: "index_comments_on_parent_id", using: :btree
  add_index "comments", ["post_id"], name: "index_comments_on_post_id", using: :btree
  add_index "comments", ["rgt"], name: "index_comments_on_rgt", using: :btree
  add_index "comments", ["uuid"], name: "index_comments_on_uuid", unique: true, using: :btree

  create_table "contest_escape_authentications", force: true do |t|
    t.string   "submission_id"
    t.string   "uid"
    t.string   "provider"
    t.string   "name"
    t.string   "username"
    t.string   "token"
    t.string   "secret"
    t.datetime "token_expire_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "profile_image_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email"
  end

  add_index "contest_escape_authentications", ["submission_id"], name: "index_contest_escape_authentications_on_submission_id", using: :btree
  add_index "contest_escape_authentications", ["uid"], name: "index_contest_escape_authentications_on_uid", using: :btree

  create_table "contest_escape_submissions", force: true do |t|
    t.string   "uuid"
    t.string   "hashtag"
    t.string   "video_id"
    t.string   "video_type"
    t.string   "title"
    t.string   "band_name"
    t.string   "song_title"
    t.text     "description"
    t.string   "thumbnail_file_name"
    t.string   "thumbnail_content_type"
    t.integer  "thumbnail_file_size"
    t.datetime "thumbnail_updated_at"
    t.string   "thumbnail_url"
    t.string   "duration"
    t.integer  "user_id"
    t.string   "submitter_name"
    t.string   "submitter_email"
    t.integer  "submitter_relationship",            default: 0
    t.integer  "status",                            default: 0
    t.integer  "winner_status",                     default: 0
    t.text     "winner_details"
    t.integer  "total_vote_count",                  default: 0
    t.integer  "rank"
    t.integer  "twitter_url_count",                 default: 0
    t.integer  "twitter_hashtag_count",             default: 0
    t.integer  "twitter_url_and_hashtag_count",     default: 0
    t.integer  "twitter_total_count",               default: 0
    t.string   "twitter_search_since_id"
    t.datetime "twitter_search_scraped_at"
    t.datetime "twitter_share_scraped_at"
    t.integer  "facebook_url_count",                default: 0
    t.integer  "facebook_hashtag_count",            default: 0
    t.integer  "facebook_url_and_hashtag_count",    default: 0
    t.integer  "facebook_total_count",              default: 0
    t.string   "facebook_search_since_id"
    t.datetime "facebook_search_scraped_at"
    t.datetime "facebook_share_scraped_at"
    t.integer  "google_plus_url_count",             default: 0
    t.integer  "google_plus_hashtag_count",         default: 0
    t.integer  "google_plus_url_and_hashtag_count", default: 0
    t.integer  "google_plus_total_count",           default: 0
    t.integer  "google_plus_search_since_id",       default: 0
    t.datetime "google_plus_search_scraped_at"
    t.datetime "google_plus_share_scraped_at"
    t.integer  "youtube_initial_comments_count",    default: 0
    t.integer  "youtube_comments_count",            default: 0
    t.integer  "youtube_initial_likes_count",       default: 0
    t.integer  "youtube_likes_count",               default: 0
    t.integer  "youtube_total_count",               default: 0
    t.datetime "youtube_scraped_at"
    t.string   "ip_address"
    t.string   "user_agent"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone"
  end

  add_index "contest_escape_submissions", ["uuid"], name: "index_contest_escape_submissions_on_uuid", using: :btree
  add_index "contest_escape_submissions", ["video_id", "video_type"], name: "index_contest_escape_submissions_on_video_id_and_video_type", using: :btree

  create_table "email_subscribers", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "impression_rollups", force: true do |t|
    t.date     "rollup_date",                                          null: false
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.text     "message",                 limit: 16777215
    t.integer  "impression_count",                         default: 0, null: false
    t.integer  "unique_impression_count",                  default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "impression_rollups", ["impressionable_type", "impressionable_id"], name: "impressionable_rollup_type_index", using: :btree
  add_index "impression_rollups", ["impressionable_type", "message", "impressionable_id"], name: "impressionable_rollup_type_message_index", length: {"impressionable_type"=>nil, "message"=>255, "impressionable_id"=>nil}, using: :btree

  create_table "impressions", force: true do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message",             limit: 16777215
    t.text     "referrer",            limit: 16777215
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "impressions", ["action_name", "created_at"], name: "index_impressions_on_action_name_and_created_at", using: :btree
  add_index "impressions", ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id"], name: "index_impressions_on_impressionable_type_and_impressionable_id", using: :btree
  add_index "impressions", ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", length: {"impressionable_type"=>nil, "message"=>255, "impressionable_id"=>nil}, using: :btree
  add_index "impressions", ["user_id"], name: "index_impressions_on_user_id", using: :btree

  create_table "post_authors", force: true do |t|
    t.integer "post_id",              null: false
    t.integer "user_id",              null: false
    t.integer "position", default: 0, null: false
  end

  add_index "post_authors", ["post_id"], name: "index_post_authors_on_post_id", using: :btree
  add_index "post_authors", ["user_id"], name: "index_post_authors_on_user_id", using: :btree

  create_table "post_badges", force: true do |t|
    t.integer "post_id"
    t.integer "badge_id"
  end

  add_index "post_badges", ["badge_id"], name: "index_post_badges_on_badge_id", using: :btree
  add_index "post_badges", ["post_id"], name: "index_post_badges_on_post_id", using: :btree

  create_table "post_items", force: true do |t|
    t.integer  "post_id",                                             null: false
    t.string   "slug",                                                null: false
    t.string   "old_style"
    t.integer  "style",                               default: 0
    t.string   "title",                                               null: false
    t.text     "text",               limit: 16777215
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.text     "media",              limit: 16777215
    t.string   "source"
    t.string   "via"
    t.boolean  "bulleted",                            default: true
    t.boolean  "visible",                             default: true
    t.integer  "position",                            default: 0
    t.boolean  "nsfw",                                default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "stat_views",                          default: 0
    t.datetime "stat_updated_at"
    t.integer  "duration",                            default: 0
    t.integer  "image_width",                         default: 0
    t.integer  "image_height",                        default: 0
    t.boolean  "image_animated",                      default: false
  end

  add_index "post_items", ["post_id"], name: "index_post_items_on_post_id", using: :btree
  add_index "post_items", ["slug"], name: "index_post_items_on_slug", using: :btree

  create_table "post_queues", force: true do |t|
    t.integer  "post_id"
    t.string   "jid"
    t.datetime "publish_at"
    t.boolean  "processed"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "post_queues", ["post_id"], name: "index_post_queues_on_post_id", using: :btree

  create_table "post_recommendations", force: true do |t|
    t.integer "post_id"
    t.integer "recommended_post_id"
  end

  add_index "post_recommendations", ["post_id"], name: "index_post_recommendations_on_post_id", using: :btree
  add_index "post_recommendations", ["recommended_post_id"], name: "index_post_recommendations_on_recommended_post_id", using: :btree

  create_table "post_tags", force: true do |t|
    t.integer "post_id"
    t.integer "tag_id"
  end

  add_index "post_tags", ["post_id"], name: "index_post_tags_on_post_id", using: :btree
  add_index "post_tags", ["tag_id"], name: "index_post_tags_on_tag_id", using: :btree

  create_table "posts", force: true do |t|
    t.string   "slug"
    t.string   "title"
    t.text     "description",                limit: 16777215
    t.string   "promo_title"
    t.text     "promo_description",          limit: 16777215
    t.string   "promo_image_file_name"
    t.string   "promo_image_content_type"
    t.integer  "promo_image_file_size"
    t.datetime "promo_image_updated_at"
    t.integer  "promo_image_feed_file_size"
    t.integer  "category_id"
    t.string   "style"
    t.string   "old_format"
    t.integer  "format",                                      default: 0
    t.integer  "creator_id",                                                  null: false
    t.boolean  "visible_homepage",                            default: true
    t.boolean  "visible_stats",                               default: true
    t.boolean  "nsfw",                                        default: false
    t.boolean  "wt_original_video",                           default: false
    t.boolean  "autogenerated",                               default: false
    t.datetime "published_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "shows_count",                                 default: 0
    t.integer  "shares_count",                                default: 0
    t.integer  "comments_count",                              default: 0
    t.string   "state"
    t.boolean  "allow_comments",                              default: true
    t.boolean  "google_news",                                 default: false
    t.integer  "shows_velocity_delta",                        default: 0
    t.float    "shows_velocity_score",       limit: 24,       default: 0.0
    t.integer  "shares_velocity_delta",                       default: 0
    t.float    "shares_velocity_score",      limit: 24,       default: 0.0
    t.integer  "comments_velocity_delta",                     default: 0
    t.float    "comments_velocity_score",    limit: 24,       default: 0.0
  end

  add_index "posts", ["category_id"], name: "index_posts_on_category_id", using: :btree
  add_index "posts", ["creator_id"], name: "index_posts_on_creator_id", using: :btree
  add_index "posts", ["slug"], name: "index_posts_on_slug", using: :btree

  create_table "search_terms", force: true do |t|
    t.string   "term"
    t.integer  "impressions_count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  add_index "search_terms", ["slug"], name: "index_search_terms_on_slug", using: :btree
  add_index "search_terms", ["term"], name: "index_search_terms_on_term", using: :btree

  create_table "settings", force: true do |t|
    t.string   "var",                         null: false
    t.text     "value",      limit: 16777215
    t.integer  "thing_id"
    t.string   "thing_type", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true, using: :btree

  create_table "sharing_mails", force: true do |t|
    t.string   "sender_name"
    t.string   "sender_email"
    t.string   "receiver_email"
    t.text     "message"
    t.integer  "target_id"
    t.string   "target_type"
    t.string   "user_agent"
    t.string   "ip_address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sharing_mails", ["target_id", "target_type"], name: "index_sharing_mails_on_target_id_and_target_type", using: :btree

  create_table "support_messages", force: true do |t|
    t.string   "uuid"
    t.integer  "user_id"
    t.string   "session_id"
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "message"
    t.string   "ip_address"
    t.string   "browser"
    t.string   "referrer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "support_messages", ["uuid"], name: "index_support_messages_on_uuid", unique: true, using: :btree

  create_table "tags", force: true do |t|
    t.string   "slug",                        null: false
    t.string   "name",                        null: false
    t.integer  "posts_count", default: 0
    t.boolean  "nsfw",        default: false
    t.boolean  "active",      default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tags", ["slug"], name: "index_tags_on_slug", using: :btree

  create_table "trend_items", force: true do |t|
    t.integer  "keyword_uuid"
    t.integer  "post_id"
    t.string   "keyword"
    t.string   "display_keyword"
    t.string   "format"
    t.string   "grouping"
    t.string   "source"
    t.string   "source_url"
    t.string   "via_url"
    t.string   "image_url"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "active",             default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_list_items", force: true do |t|
    t.integer  "trend_list_id"
    t.integer  "trend_item_id"
    t.integer  "rank",                            default: 100
    t.integer  "rank_delta_last_hour"
    t.integer  "rank_delta_last_day"
    t.float    "avg_score",            limit: 24
    t.float    "score",                limit: 24
    t.boolean  "active",                          default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trend_lists", force: true do |t|
    t.string   "list_type"
    t.integer  "total_score"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_activities", force: true do |t|
    t.integer  "user_id",                                        null: false
    t.integer  "item_id"
    t.string   "item_type"
    t.string   "action"
    t.text     "action_object", limit: 16777215
    t.boolean  "automatic",                      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_activities", ["item_id", "item_type"], name: "index_user_activities_on_item_id_and_item_type", using: :btree

  create_table "users", force: true do |t|
    t.string   "name",                                             null: false
    t.string   "first_name",                                       null: false
    t.string   "last_name",                                        null: false
    t.string   "email"
    t.string   "slug",                                             null: false
    t.string   "login",                                            null: false
    t.string   "title"
    t.text     "byline",              limit: 16777215
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "facebook_id"
    t.string   "facebook_handle"
    t.string   "twitter_id"
    t.string   "twitter_handle"
    t.string   "google_plus_id"
    t.string   "google_plus_handle"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token",                                null: false
    t.string   "perishable_token",                                 null: false
    t.string   "single_access_token",                              null: false
    t.integer  "login_count",                          default: 0, null: false
    t.integer  "failed_login_count",                   default: 0, null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.integer  "roles_mask"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["last_request_at"], name: "index_users_on_last_request_at", using: :btree
  add_index "users", ["perishable_token"], name: "index_users_on_perishable_token", using: :btree
  add_index "users", ["persistence_token"], name: "index_users_on_persistence_token", using: :btree
  add_index "users", ["single_access_token"], name: "index_users_on_single_access_token", using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", using: :btree

end
