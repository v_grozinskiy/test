class AddRolesMaskToUsers < ActiveRecord::Migration

  def up
    add_column :users, :roles_mask, :integer, after: :last_login_ip
    drop_table :user_roles
  end

  # for posterity
  def down
    remove_column :users, :roles_mask

    create_table :user_roles do |t|
      t.integer :user_id,       null: false
      t.string :role_type,      null: false
      t.integer :created_by,    null: false

      t.timestamps
    end

    add_index :user_roles, :user_id
  end

end
