class AddImageUrlToTrendItems < ActiveRecord::Migration

  def change
    add_column :trend_items, :image_url, :string, after: :source_url
  end

end
