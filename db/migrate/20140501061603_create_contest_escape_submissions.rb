class CreateContestEscapeSubmissions < ActiveRecord::Migration
  def change
    create_table :contest_escape_submissions do |t|
      t.string :uuid
      t.string :hashtag
      t.string :video_id
      t.string :video_type
      t.string :title
      t.string :band_name
      t.text :description
      t.attachment :thumbnail
      t.string :duration
      t.integer :user_id
      t.string :submitter_name
      t.string :submitter_email
      t.string :submitter_relationship,                 default: 0
      t.integer :status,                                default: 0
      t.integer :total_vote_count,                      default: 0
      t.integer :twitter_url_count,                     default: 0
      t.integer :twitter_hashtag_count,                 default: 0
      t.integer :twitter_url_and_hashtag_count,         default: 0
      t.integer :twitter_total_count,                   default: 0
      t.string :twitter_search_since_id
      t.timestamp :twitter_search_scraped_at
      t.timestamp :twitter_share_scraped_at
      t.integer :facebook_url_count,                    default: 0
      t.integer :facebook_hashtag_count,                default: 0
      t.integer :facebook_url_and_hashtag_count,        default: 0
      t.integer :facebook_total_count,                  default: 0
      t.string :facebook_search_since_id
      t.timestamp :facebook_search_scraped_at
      t.timestamp :facebook_share_scraped_at
      t.integer :google_plus_url_count,                 default: 0
      t.integer :google_plus_hashtag_count,             default: 0
      t.integer :google_plus_url_and_hashtag_count,     default: 0
      t.integer :google_plus_total_count,               default: 0
      t.string :google_plus_search_since_id
      t.timestamp :google_plus_search_scraped_at
      t.timestamp :google_plus_share_scraped_at
      t.integer :youtube_initial_comments_count,        default: 0
      t.integer :youtube_comments_count,                default: 0
      t.integer :youtube_initial_likes_count,           default: 0
      t.integer :youtube_likes_count,                   default: 0
      t.integer :youtube_total_count,                   default: 0
      t.timestamp :youtube_scraped_at
      t.string :ip_address
      t.string :user_agent

      t.timestamps
    end

    add_index :contest_escape_submissions, :uuid
  end
end
