class CreateUserRoles < ActiveRecord::Migration
  def change
    create_table :user_roles do |t|
      t.integer :user_id,       null: false
      t.string :role_type,      null: false
      t.integer :created_by,    null: false

      t.timestamps
    end

    add_index :user_roles, :user_id
  end
end
