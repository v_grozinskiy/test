class CreatePostRecommendations < ActiveRecord::Migration
  def change
    create_table :post_recommendations do |t|
      t.integer :post_id
      t.integer :recommended_post_id
    end

    add_index :post_recommendations, :post_id
    add_index :post_recommendations, :recommended_post_id
  end
end
