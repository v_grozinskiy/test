class AddNestedSetToCategories < ActiveRecord::Migration

  def change
    add_column :categories, :parent_id, :integer, after: :nsfw
    add_column :categories, :lft, :integer, after: :parent_id
    add_column :categories, :rgt, :integer, after: :lft
    add_column :categories, :depth, :integer, after: :rgt
  end

end
