class AddGoogleNewsBooleanToPosts < ActiveRecord::Migration

  def change
    add_column :posts, :google_news, :boolean, default: true, after: :allow_comments
  end

end
