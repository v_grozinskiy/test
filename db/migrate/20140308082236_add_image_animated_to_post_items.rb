class AddImageAnimatedToPostItems < ActiveRecord::Migration
  def change
    add_column :post_items, :image_animated, :boolean, default: false
  end
end
