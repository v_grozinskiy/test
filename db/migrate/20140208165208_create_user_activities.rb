class CreateUserActivities < ActiveRecord::Migration
  def change
    create_table :user_activities do |t|
      t.integer :user_id,       null: false
      t.references :item,       polymorphic: true
      t.string :action
      t.text :action_object
      t.boolean :automatic,     default: false

      t.timestamps
    end

    add_index :user_activities, [:item_id, :item_type]
  end
end
