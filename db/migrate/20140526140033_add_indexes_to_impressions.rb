class AddIndexesToImpressions < ActiveRecord::Migration
  def change
    add_index :impressions, [:impressionable_type, :impressionable_id]
    add_index :impressions, [:action_name, :created_at]
  end
end
