class AddRankToContestEscapeSubmissions < ActiveRecord::Migration

  def change
    add_column :contest_escape_submissions, :rank, :integer, after: :total_vote_count
  end

end
