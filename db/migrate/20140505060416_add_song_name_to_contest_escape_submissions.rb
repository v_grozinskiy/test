class AddSongNameToContestEscapeSubmissions < ActiveRecord::Migration
  def change
    add_column :contest_escape_submissions, :song_title, :string, after: :band_name
  end
end
