class CreateSharingMails < ActiveRecord::Migration
  def change
    create_table :sharing_mails do |t|
      t.string :sender_name
      t.string :sender_email
      t.string :receiver_email
      t.text :message
      t.references :target,       polymorphic: true
      t.string :user_agent
      t.string :ip_address

      t.timestamps
    end

    add_index :sharing_mails, [:target_id, :target_type]
  end
end
