class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :slug
      t.string :title
      t.text :description
      t.string :promo_title
      t.text :promo_description
      t.attachment :promo_image
      t.integer :category_id
      t.string :style
      t.string :format
      t.integer :creator_id,          null: false
      t.boolean :visible_homepage,    default: true
      t.boolean :visible_stats,       default: true
      t.boolean :nsfw,                default: false
      t.timestamp :published_at

      t.timestamps
    end

    add_index :posts, :slug
    add_index :posts, :category_id
    add_index :posts, :creator_id
  end
end
