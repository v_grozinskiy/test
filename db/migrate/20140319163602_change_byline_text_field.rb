class ChangeBylineTextField < ActiveRecord::Migration

  def up
    change_column :users, :byline, :mediumtext
  end

  def down
    change_column :users, :byline, :string
  end

end
