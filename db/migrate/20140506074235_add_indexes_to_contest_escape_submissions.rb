class AddIndexesToContestEscapeSubmissions < ActiveRecord::Migration
  def self.up
    add_index :contest_escape_submissions, [:video_id, :video_type]
  end

  def self.down
    remove_index :contest_escape_submissions, [:video_id, :video_type]
  end
end
