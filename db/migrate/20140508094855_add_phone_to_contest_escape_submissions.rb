class AddPhoneToContestEscapeSubmissions < ActiveRecord::Migration
  def change
    add_column :contest_escape_submissions, :phone, :string
  end
end
