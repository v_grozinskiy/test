class AddPaperclipToUsers < ActiveRecord::Migration

  def change
    add_column :users, :avatar_file_name, :string, after: :byline
    add_column :users, :avatar_content_type, :string, after: :avatar_file_name
    add_column :users, :avatar_file_size, :integer, after: :avatar_content_type
    add_column :users, :avatar_updated_at, :datetime, after: :avatar_file_size
  end

end
