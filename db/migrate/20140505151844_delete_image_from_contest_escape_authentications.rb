class DeleteImageFromContestEscapeAuthentications < ActiveRecord::Migration

  def up
    remove_column :contest_escape_authentications, :image
  end

  def down
    add_column :contest_escape_authentications, :image, :stirng, after: :username
  end

end
