class CreateSearchTerms < ActiveRecord::Migration
  def change
    create_table :search_terms do |t|
      t.string :term
      t.integer :impressions_count

      t.timestamps
    end

    add_index :search_terms, :term
  end
end
