class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :slug,           null: false
      t.string :name,           null: false
      t.integer :posts_count,   default: 0
      t.boolean :nsfw,          default: false
      t.boolean :active,        default: true

      t.timestamps
    end

    add_index :tags, :slug
  end
end
