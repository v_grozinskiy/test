class AddScoresAndDeltasToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :shows_velocity_delta,     :integer, default: 0
    add_column :posts, :shows_velocity_score,     :float,   default: 0
    add_column :posts, :shares_velocity_delta,    :integer, default: 0
    add_column :posts, :shares_velocity_score,    :float,   default: 0
    add_column :posts, :comments_velocity_delta,  :integer, default: 0
    add_column :posts, :comments_velocity_score,  :float,   default: 0
  end
end
