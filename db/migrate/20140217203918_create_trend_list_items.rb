class CreateTrendListItems < ActiveRecord::Migration

  def change
    create_table :trend_list_items do |t|
      t.integer     :trend_list_id
      t.integer     :trend_item_id
      t.integer     :rank,                  default: 100
      t.integer     :rank_delta_last_hour
      t.integer     :rank_delta_last_day
      t.float       :avg_score,             precision: 10,   scale: 8
      t.float       :score,                 precision: 5,    scale: 3
      t.boolean     :active,                default: true
      t.timestamps
    end
  end

end
