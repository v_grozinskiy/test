class AddWinnerStatusToContest < ActiveRecord::Migration
  def change
    add_column :contest_escape_submissions, :winner_status, :integer, default: 0, after: :status
    add_column :contest_escape_submissions, :winner_details, :text, after: :winner_status
  end
end
