class CreateTrendItems < ActiveRecord::Migration

  def change
    create_table :trend_items do |t|
      t.integer     :keyword_uuid
      t.integer     :post_id
      t.string      :keyword
      t.string      :display_keyword
      t.string      :format
      t.string      :grouping
      t.string      :source
      t.string      :source_url
      t.string      :image_file_name
      t.string      :image_content_type
      t.integer     :image_file_size
      t.datetime    :image_updated_at
      t.boolean     :active,                default: false
      t.timestamps
    end
  end

end
