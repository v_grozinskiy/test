class CreateBadges < ActiveRecord::Migration
  def change
    create_table :badges do |t|
      t.string :slug,           null: false
      t.string :name,           null: false
      t.text :description
      t.attachment :icon
      t.integer :posts_count,   default: 0
      t.boolean :nsfw,          default: false
      t.boolean :active,        default: true

      t.timestamps
    end

    add_index :badges, :slug
  end
end
