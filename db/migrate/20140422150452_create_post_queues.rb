class CreatePostQueues < ActiveRecord::Migration
  def change
    create_table :post_queues do |t|
      t.integer :post_id
      t.string :jid
      t.timestamp :publish_at
      t.boolean :processed

      t.timestamps
    end

    add_index :post_queues, :post_id
  end
end
