class AddWtOriginalVideoBooleanToPosts < ActiveRecord::Migration

  def up
    add_column :posts, :wt_original_video, :boolean, default: false, after: :nsfw

    # Update all known posts
    Post.published.includes(:category).recent.where(category_id: Category.where(slug: ['originals','whats-trending-live']).pluck(:id) ).update_all(wt_original_video: true)
  end

  def down
    remove_column :posts, :wt_original_video
  end

end
