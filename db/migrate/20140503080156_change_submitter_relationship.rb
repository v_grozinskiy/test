class ChangeSubmitterRelationship < ActiveRecord::Migration
  def self.up
    change_column :contest_escape_submissions, :submitter_relationship, :integer, default: 0
  end

  def self.down
    change_column :contest_escape_submissions, :submitter_relationship, :string, default: '0'
  end
end
