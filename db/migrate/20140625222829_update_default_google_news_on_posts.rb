class UpdateDefaultGoogleNewsOnPosts < ActiveRecord::Migration

  # By default, all posts should have false, not true.
  # All old posts should be false too, so lets delete and re-add column

  def up
    remove_column :posts, :google_news
    add_column :posts, :google_news, :boolean, default: false, after: :allow_comments
  end

  def down
    remove_column :posts, :google_news
    add_column :posts, :google_news, :boolean, default: true, after: :allow_comments
    Post.update_all(google_news: false)
  end

end
