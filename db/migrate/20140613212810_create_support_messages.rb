class CreateSupportMessages < ActiveRecord::Migration

  def change
    create_table :support_messages do |t|
      t.string        :uuid
      t.integer       :user_id
      t.string        :session_id
      t.string        :name
      t.string        :email
      t.string        :phone
      t.text          :message
      t.string        :ip_address
      t.string        :browser
      t.string        :referrer
      t.timestamps
    end

    add_index :support_messages, [:uuid], unique: true
  end

end
