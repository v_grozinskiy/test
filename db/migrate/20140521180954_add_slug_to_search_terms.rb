class AddSlugToSearchTerms < ActiveRecord::Migration
  def change
    add_column :search_terms, :slug, :string
    add_index :search_terms, :slug
  end
end
