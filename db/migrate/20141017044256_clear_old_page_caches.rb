class ClearOldPageCaches < ActiveRecord::Migration

  def up
    ApplicationController.new.delete_major_caches
    Rails.cache.delete_matched('*/xml*')
    Rails.cache.delete_matched('*/static_pages/*')

    CategoryWorker.new.dropdowns
    PostWorker.new.popularity
    TrendWorker.new.trend_list
  end

  def down
  end

end
