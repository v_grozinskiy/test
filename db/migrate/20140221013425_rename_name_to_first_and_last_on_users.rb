class RenameNameToFirstAndLastOnUsers < ActiveRecord::Migration

  def up
    add_column :users, :first_name, :string, after: :name, null: false
    add_column :users, :last_name, :string, after: :first_name, null: false

    User.find_each do |user|
      n = (user.attributes['name'] || '').split(' ')
      user.first_name = n.shift
      user.last_name = n.join(' ')
      puts "ERROR: #{user.errors.messages}" unless user.save
    end
  end

  def down
    remove_column :users, :first_name
    remove_column :users, :last_name
  end

end
