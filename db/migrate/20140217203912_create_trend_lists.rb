class CreateTrendLists < ActiveRecord::Migration

  def change
    create_table :trend_lists do |t|
      t.string      :list_type
      t.integer     :total_score
      t.string      :state
      t.timestamps
    end
  end

end
