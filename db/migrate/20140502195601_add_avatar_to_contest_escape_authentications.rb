class AddAvatarToContestEscapeAuthentications < ActiveRecord::Migration

  def change
    add_column :contest_escape_authentications, :token_expire_at, :datetime, after: :secret
    add_column :contest_escape_authentications, :avatar_file_name, :string, after: :token_expire_at
    add_column :contest_escape_authentications, :avatar_content_type, :string, after: :avatar_file_name
    add_column :contest_escape_authentications, :avatar_file_size, :integer, after: :avatar_content_type
    add_column :contest_escape_authentications, :avatar_updated_at, :datetime, after: :avatar_file_size
    add_column :contest_escape_authentications, :profile_image_url, :string, after: :avatar_updated_at
  end

end
