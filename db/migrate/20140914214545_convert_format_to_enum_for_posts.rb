class ConvertFormatToEnumForPosts < ActiveRecord::Migration

  # TODO : Remove old_format, old_style later on.
  def up
    # --- Post ---
    rename_column :posts, :format, :old_format unless column_exists?(:posts, :old_format)
    add_column :posts, :format, :integer, default: 0, after: :old_format unless column_exists?(:posts, :format)
    Post.where('slug is not null').find_each{|post| post.update(format: (Post.formats[post.old_format.downcase] rescue 0) ) rescue nil }

    # --- Post Items ---
    rename_column :post_items, :style, :old_style unless column_exists?(:post_items, :old_style)
    add_column :post_items, :style, :integer, default: 0, after: :old_style unless column_exists?(:post_items, :style)
    PostItem.find_each{|item| item.update(style: (PostItem.styles["#{item.old_style}_item".downcase] rescue 0) ) }
  end

  def down
    # --- Post ---
    remove_column :posts, :format
    rename_column :posts, :old_format, :format

    # --- Post Items ---
    remove_column :post_items, :style
    rename_column :post_items, :old_style, :style
  end

end
