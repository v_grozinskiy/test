class CreateImpressionRollups < ActiveRecord::Migration

  def change
    create_table :impression_rollups do |t|
      t.date      :rollup_date,                                       null: false
      t.string    :impressionable_type
      t.integer   :impressionable_id
      t.string    :controller_name
      t.string    :action_name
      t.string    :view_name
      t.text      :message,             limit: 16777215
      t.integer   :impression_count,                      default: 0, null: false
      t.integer   :unique_impression_count,               default: 0, null: false
      t.timestamps
    end
    
    add_index :impression_rollups,  [:impressionable_type, :impressionable_id], name: :impressionable_rollup_type_index
    add_index :impression_rollups,  [:impressionable_type, :message, :impressionable_id], name: :impressionable_rollup_type_message_index, length: {impressionable_type: nil, message: 255, impressionable_id: nil}
  end

end
