class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name,                       null: false
      t.string :email
      t.string :slug,                       null: false
      t.string :login,                      null: false
      t.string :byline
      t.string :facebook_id
      t.string :facebook_handle
      t.string :twitter_id
      t.string :twitter_handle
      t.string :crypted_password
      t.string :password_salt
      t.string :persistence_token,          null: false
      t.string :perishable_token,           null: false
      t.string :single_access_token,        null: false
      t.integer :login_count,               null: false,    default: 0
      t.integer :failed_login_count,        null: false,    default: 0
      t.datetime :last_request_at
      t.datetime :current_login_at
      t.datetime :last_login_at
      t.string :current_login_ip
      t.string :last_login_ip

      t.timestamps
    end

    add_index :users, :slug
    add_index :users, :persistence_token
    add_index :users, :perishable_token
    add_index :users, :single_access_token
    add_index :users, :last_request_at
  end
end
