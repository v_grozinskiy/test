class CreatePostItems < ActiveRecord::Migration
  def change
    create_table :post_items do |t|
      t.integer :post_id,         null: false
      t.string :slug,             null: false
      t.text :style
      t.string :title,            null: false
      t.text :text
      t.attachment :image
      t.string :media
      t.string :source
      t.string :via
      t.boolean :bulleted,        default: true
      t.boolean :visible,         default: true
      t.integer :position,        default: 0
      t.boolean :nsfw,            default: false

      t.timestamps
    end
    
    add_index :post_items, :slug
    add_index :post_items, :post_id
  end
end
