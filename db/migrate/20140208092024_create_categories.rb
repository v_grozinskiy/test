class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :slug,           null: false
      t.string :name,           null: false
      t.text :description
      t.attachment :banner_image
      t.integer :posts_count,   default: 0
      t.boolean :promotional,   default: false
      t.boolean :nsfw,          default: false
      t.boolean :active,        default: true

      t.timestamps
    end

    add_index :categories, :slug
  end
end
