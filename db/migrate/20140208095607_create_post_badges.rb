class CreatePostBadges < ActiveRecord::Migration
  def change
    create_table :post_badges do |t|
      t.integer :post_id
      t.integer :badge_id
    end

    add_index :post_badges, :post_id
    add_index :post_badges, :badge_id
  end
end
