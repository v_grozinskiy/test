class ChangeSinceIdForContestEscapeSubmissions < ActiveRecord::Migration

  def up
    change_column :contest_escape_submissions, :google_plus_search_since_id, :integer, default: 0
  end

  def down
    # meh...
  end

end
