class AddPromoImageFeedFileSizeToPosts < ActiveRecord::Migration

  def up
    add_column :posts, :promo_image_feed_file_size, :integer, default: nil, after: :promo_image_updated_at
    Post.where('promo_image_file_name IS NOT NULL').find_each{|p| p.delay_get_feed_promo_image_file_size }
  end

  def down
    remove_column :posts, :promo_image_feed_file_size
  end

end
