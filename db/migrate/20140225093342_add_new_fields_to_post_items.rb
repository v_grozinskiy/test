class AddNewFieldsToPostItems < ActiveRecord::Migration
  def change
    add_column :post_items, :stat_views, :integer,        default: 0
    add_column :post_items, :stat_updated_at, :datetime
    add_column :post_items, :duration, :integer,          default: 0
    add_column :post_items, :image_width, :integer,       default: 0
    add_column :post_items, :image_height, :integer,      default: 0
  end
end
