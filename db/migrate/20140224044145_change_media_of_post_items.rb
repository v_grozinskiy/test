class ChangeMediaOfPostItems < ActiveRecord::Migration
  def self.up
    change_column :post_items, :media, :text
  end

  def self.down
    change_column :post_items, :media, :string
  end
end
