class AddThumbnailUrlToSubmissions < ActiveRecord::Migration

  def change
    add_column :contest_escape_submissions, :thumbnail_url, :string, after: :thumbnail_updated_at
  end

end
