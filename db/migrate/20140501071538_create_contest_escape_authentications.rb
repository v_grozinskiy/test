class CreateContestEscapeAuthentications < ActiveRecord::Migration
  def change
    create_table :contest_escape_authentications do |t|
      t.string :submission_id
      t.string :uid
      t.string :provider
      t.string :name
      t.string :username
      t.string :image
      t.string :token
      t.string :secret

      t.timestamps
    end

    add_index :contest_escape_authentications, :submission_id
    add_index :contest_escape_authentications, :uid
  end
end
