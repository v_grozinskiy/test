class CreatePostAuthors < ActiveRecord::Migration
  def change
    create_table :post_authors do |t|
      t.integer :post_id,     null: false
      t.integer :user_id,     null: false
      t.integer :position,    null: false,   default: 0
    end

    add_index :post_authors, :post_id
    add_index :post_authors, :user_id
  end
end
