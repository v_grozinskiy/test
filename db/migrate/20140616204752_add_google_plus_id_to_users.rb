class AddGooglePlusIdToUsers < ActiveRecord::Migration

  def change
    add_column :users, :google_plus_id, :string, after: :twitter_handle
    add_column :users, :google_plus_handle, :string, after: :google_plus_id
  end

end
