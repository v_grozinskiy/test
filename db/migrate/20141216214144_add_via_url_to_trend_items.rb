class AddViaUrlToTrendItems < ActiveRecord::Migration

  def change
    add_column :trend_items, :via_url, :string, after: :source_url
  end

end
